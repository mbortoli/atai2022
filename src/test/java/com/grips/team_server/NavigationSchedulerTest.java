package com.grips.team_server;

import com.grips.refbox.msg_handler.NavigationChallangeHandler;
import com.grips.scheduler.challanges.NavigationScheduler;
import org.junit.jupiter.api.Test;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.NavigationChallengeProtos;
import org.robocup_logistics.llsf_msgs.ZoneProtos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static org.robocup_logistics.llsf_msgs.TeamProtos.Team.*;
import static org.robocup_logistics.llsf_msgs.ZoneProtos.Zone.*;

public class NavigationSchedulerTest {
    @Test
    void doSomething() {
        int fail_counter = 0;
        //BeaconSignalProtos.BeaconSignal.
            Random rand = new Random();
            List<ZoneProtos.Zone> zones = new ArrayList<ZoneProtos.Zone>();
            while (zones.size() < 12) {
                int dec = 1 + rand.nextInt(5);
                int one = 1 + rand.nextInt(5);
                if (dec * 10 + one == 51 || dec * 10 + one == 41 || dec * 10 + one == 31)
                    continue;
                if (zones.contains(ZoneProtos.Zone.forNumber(1000 + dec * 10 + one)))
                    continue;
                zones.add(ZoneProtos.Zone.forNumber(1000 + dec * 10 + one));
            }
            Iterable<ZoneProtos.Zone> iterable = zones;
            System.out.println("Hell0");
            NavigationChallengeProtos.Route r = NavigationChallengeProtos.Route.newBuilder().addAllRoute(iterable).setId(0).build();
            NavigationScheduler nav = new NavigationScheduler();
            NavigationChallengeProtos.NavigationRoutes route = NavigationChallengeProtos.NavigationRoutes.newBuilder()
                    .setTeamColor(MAGENTA)
                    .addRoutes(r)
                    .build();


            //System.out.println(route.getRoutesList().get(0).getRouteList().size());

            nav.updateRoutes(route);
            Boolean ret = nav.clusterRoutes();
            if(!ret)
                fail_counter++;

        //System.out.println(fail_counter);
    }
}
