(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! /home/peter/team_server/visualization/src/main.ts */
      "zUnb");
      /***/
    },

    /***/
    "2Hn8":
    /*!**********************************************!*\
      !*** ./src/app/robot-controller/machines.ts ***!
      \**********************************************/

    /*! exports provided: MachineOptions */

    /***/
    function Hn8(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MachineOptions", function () {
        return MachineOptions;
      });

      var MachineOptions = [{
        value: "C-CS1",
        label: "C-CS1"
      }, {
        value: "C-CS2",
        label: "C-CS2"
      }, {
        value: "C-RS1",
        label: "C-RS1"
      }, {
        value: "C-RS2",
        label: "C-RS2"
      }, {
        value: "C-BS",
        label: "C-BS"
      }, {
        value: "C-DS",
        label: "C-DS"
      }, {
        value: "C-SS",
        label: "C-SS"
      }, {
        value: "M-CS1",
        label: "M-CS1"
      }, {
        value: "M-CS2",
        label: "M-CS2"
      }, {
        value: "M-RS1",
        label: "M-RS1"
      }, {
        value: "M-RS2",
        label: "M-RS2"
      }, {
        value: "M-BS",
        label: "M-BS"
      }, {
        value: "M-DS",
        label: "M-DS"
      }, {
        value: "M-SS",
        label: "M-SS"
      }];
      /***/
    },

    /***/
    "4QAB":
    /*!****************************************************!*\
      !*** ./src/app/statistics/statistics.component.ts ***!
      \****************************************************/

    /*! exports provided: StatisticsComponent */

    /***/
    function QAB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "StatisticsComponent", function () {
        return StatisticsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../services/data.service */
      "EnSQ");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _game_points_game_points_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./game-points/game-points.component */
      "VIXV");

      function StatisticsComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-game-points", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var data_r1 = ctx.ngIf;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", data_r1);
        }
      }

      var StatisticsComponent = /*#__PURE__*/function () {
        function StatisticsComponent(dataService) {
          _classCallCheck(this, StatisticsComponent);

          this.dataService = dataService;
          this.data$ = dataService.getPointsForStatistics();
        }

        _createClass(StatisticsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return StatisticsComponent;
      }();

      StatisticsComponent.ɵfac = function StatisticsComponent_Factory(t) {
        return new (t || StatisticsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]));
      };

      StatisticsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: StatisticsComponent,
        selectors: [["app-statistics"]],
        decls: 2,
        vars: 3,
        consts: [["class", "container", 4, "ngIf"], [1, "container"], [1, "container", 3, "data"]],
        template: function StatisticsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, StatisticsComponent_div_0_Template, 2, 1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](1, "async");
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](1, 1, ctx.data$));
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _game_points_game_points_component__WEBPACK_IMPORTED_MODULE_3__["GamePointsComponent"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["AsyncPipe"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YXRpc3RpY3Mvc3RhdGlzdGljcy5jb21wb25lbnQuc2FzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](StatisticsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-statistics',
            templateUrl: './statistics.component.html',
            styleUrls: ['./statistics.component.sass']
          }]
        }], function () {
          return [{
            type: _services_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "7BCm":
    /*!****************************************************************!*\
      !*** ./src/app/robot-controller/robot-controller.component.ts ***!
      \****************************************************************/

    /*! exports provided: RobotControllerComponent */

    /***/
    function BCm(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RobotControllerComponent", function () {
        return RobotControllerComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _move_to_waypoint_move_to_waypoint_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./move-to-waypoint/move-to-waypoint.component */
      "bYgO");
      /* harmony import */


      var _get_product_get_product_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./get-product/get-product.component */
      "azNi");
      /* harmony import */


      var _deliver_product_deliver_product_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./deliver-product/deliver-product.component */
      "7Bhy");
      /* harmony import */


      var _explore_machine_explore_machine_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./explore-machine/explore-machine.component */
      "acWC");
      /* harmony import */


      var _buffer_cap_buffer_cap_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./buffer-cap/buffer-cap.component */
      "GIv5");

      var RobotControllerComponent = /*#__PURE__*/function () {
        function RobotControllerComponent() {
          _classCallCheck(this, RobotControllerComponent);
        }

        _createClass(RobotControllerComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return RobotControllerComponent;
      }();

      RobotControllerComponent.ɵfac = function RobotControllerComponent_Factory(t) {
        return new (t || RobotControllerComponent)();
      };

      RobotControllerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: RobotControllerComponent,
        selectors: [["app-robot-controller"]],
        decls: 7,
        vars: 0,
        template: function RobotControllerComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "robot-controller works!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-move-to-waypoint");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-get-product");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-deliver-product");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-explore-machine");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "app-buffer-cap");
          }
        },
        directives: [_move_to_waypoint_move_to_waypoint_component__WEBPACK_IMPORTED_MODULE_1__["MoveToWaypointComponent"], _get_product_get_product_component__WEBPACK_IMPORTED_MODULE_2__["GetProductComponent"], _deliver_product_deliver_product_component__WEBPACK_IMPORTED_MODULE_3__["DeliverProductComponent"], _explore_machine_explore_machine_component__WEBPACK_IMPORTED_MODULE_4__["ExploreMachineComponent"], _buffer_cap_buffer_cap_component__WEBPACK_IMPORTED_MODULE_5__["BufferCapComponent"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JvYm90LWNvbnRyb2xsZXIvcm9ib3QtY29udHJvbGxlci5jb21wb25lbnQuc2FzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RobotControllerComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-robot-controller',
            templateUrl: './robot-controller.component.html',
            styleUrls: ['./robot-controller.component.sass']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "7Bhy":
    /*!*******************************************************************************!*\
      !*** ./src/app/robot-controller/deliver-product/deliver-product.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: DeliverProductComponent */

    /***/
    function Bhy(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DeliverProductComponent", function () {
        return DeliverProductComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-formly-helpers */
      "z69a");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../services/endpoint.service */
      "mnBg");
      /* harmony import */


      var _robots__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../robots */
      "ZgY0");
      /* harmony import */


      var _machines__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../machines */
      "2Hn8");
      /* harmony import */


      var _machine_points__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../machine-points */
      "Tpbn");
      /* harmony import */


      var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-formly/core */
      "0FS3");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");

      var DeliverProductComponent = /*#__PURE__*/function () {
        function DeliverProductComponent(endpoint) {
          _classCallCheck(this, DeliverProductComponent);

          this.endpoint = endpoint;
          this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({});
          this.model = {};
          this.fields = [ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("robotId", "Robot ID", _robots__WEBPACK_IMPORTED_MODULE_4__["RobotOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("machine", "Machine", _machines__WEBPACK_IMPORTED_MODULE_5__["MachineOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("machinePoint", "MachinePoint", _machine_points__WEBPACK_IMPORTED_MODULE_6__["MachinePointOptions"])];
        }

        _createClass(DeliverProductComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "submit",
          value: function submit() {
            console.log("Model is: ", this.model);
            this.endpoint.post("robot-controller/deliver-product", this.model).subscribe(function (res) {
              return console.log("Deliver Product send!");
            });
          }
        }]);

        return DeliverProductComponent;
      }();

      DeliverProductComponent.ɵfac = function DeliverProductComponent_Factory(t) {
        return new (t || DeliverProductComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__["EndpointService"]));
      };

      DeliverProductComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: DeliverProductComponent,
        selectors: [["app-deliver-product"]],
        decls: 6,
        vars: 4,
        consts: [[3, "formGroup", "ngSubmit"], [3, "model", "fields", "form"], ["type", "submit", "mat-raised-button", "", "color", "primary"]],
        template: function DeliverProductComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Deliver Product!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function DeliverProductComponent_Template_form_ngSubmit_2_listener() {
              return ctx.submit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "formly-form", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Submit");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("model", ctx.model)("fields", ctx.fields)("form", ctx.form);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_7__["FormlyForm"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButton"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JvYm90LWNvbnRyb2xsZXIvZGVsaXZlci1wcm9kdWN0L2RlbGl2ZXItcHJvZHVjdC5jb21wb25lbnQuc2FzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DeliverProductComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-deliver-product',
            templateUrl: './deliver-product.component.html',
            styleUrls: ['./deliver-product.component.sass']
          }]
        }], function () {
          return [{
            type: _services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__["EndpointService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "7g7D":
    /*!********************************************************!*\
      !*** ./src/app/file-stepper/file-stepper.component.ts ***!
      \********************************************************/

    /*! exports provided: FileStepperComponent */

    /***/
    function g7D(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FileStepperComponent", function () {
        return FileStepperComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../services/data.service */
      "EnSQ");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/material/form-field */
      "kmnG");
      /* harmony import */


      var _angular_material_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/select */
      "d3UM");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");
      /* harmony import */


      var _angular_material_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/core */
      "FKr1");
      /* harmony import */


      var _logistics_contest_pipes_minute_seconds_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../logistics/contest/pipes/minute-seconds.pipe */
      "exDf");

      function FileStepperComponent_mat_option_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var file_r2 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", file_r2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", file_r2, " ");
        }
      }

      function FileStepperComponent_div_9_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "minuteSeconds");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("GameTime: ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, ctx_r1.currentData[ctx_r1.currentIndex].gameState.gameTimeNanoSeconds / 1000000000), " ");
        }
      }

      var FileStepperComponent = /*#__PURE__*/function () {
        function FileStepperComponent(dataService) {
          _classCallCheck(this, FileStepperComponent);

          this.dataService = dataService;
          this.currentIndex = 0;
        }

        _createClass(FileStepperComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.currentData = [];
          }
        }, {
          key: "onSelectFile",
          value: function onSelectFile($event) {
            var _this = this;

            console.log($event.value);
            this.dataService.getFileData($event.value).subscribe(function (data) {
              _this.currentData = data;
              _this.currentIndex = 0;

              _this.dataService.updateData(_this.currentData[_this.currentIndex]);

              console.log(_this.currentData);
            });
          }
        }, {
          key: "back",
          value: function back() {
            this.currentIndex--;
            this.dataService.updateData(this.currentData[this.currentIndex]);
          }
        }, {
          key: "step",
          value: function step() {
            this.currentIndex++;
            this.dataService.updateData(this.currentData[this.currentIndex]);
          }
        }]);

        return FileStepperComponent;
      }();

      FileStepperComponent.ɵfac = function FileStepperComponent_Factory(t) {
        return new (t || FileStepperComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]));
      };

      FileStepperComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: FileStepperComponent,
        selectors: [["app-file-stepper"]],
        inputs: {
          files: "files"
        },
        decls: 16,
        vars: 4,
        consts: [[1, "row"], [1, "col"], [3, "selectionChange"], [3, "value", 4, "ngFor", "ngForOf"], [1, "col", "step"], ["class", "col", 4, "ngIf"], ["mat-raised-button", "", 3, "click"], ["color", "primary", "mat-raised-button", "", 3, "click"], [3, "value"]],
        template: function FileStepperComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-form-field");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Select file to load");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-select", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectionChange", function FileStepperComponent_Template_mat_select_selectionChange_5_listener($event) {
              return ctx.onSelectFile($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, FileStepperComponent_mat_option_6_Template, 2, 2, "mat-option", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, FileStepperComponent_div_9_Template, 3, 3, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileStepperComponent_Template_button_click_11_listener() {
              return ctx.back();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Step back");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileStepperComponent_Template_button_click_14_listener() {
              return ctx.step();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Step");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.files);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("Step: ", ctx.currentIndex, " of ", ctx.currentData.length, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.currentData.length > ctx.currentIndex);
          }
        },
        directives: [_angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_2__["MatLabel"], _angular_material_select__WEBPACK_IMPORTED_MODULE_3__["MatSelect"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_5__["MatButton"], _angular_material_core__WEBPACK_IMPORTED_MODULE_6__["MatOption"]],
        pipes: [_logistics_contest_pipes_minute_seconds_pipe__WEBPACK_IMPORTED_MODULE_7__["MinuteSecondsPipe"]],
        styles: [".row[_ngcontent-%COMP%] {\n  display: flex;\n}\n\n.col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.step[_ngcontent-%COMP%] {\n  white-space: nowrap;\n  margin-right: 5px;\n  margin-left: 5px;\n  padding-top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmlsZS1zdGVwcGVyL2ZpbGUtc3RlcHBlci5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7QUFDRjs7QUFBQTtFQUNFLE9BQUE7QUFHRjs7QUFGQTtFQUNFLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBS0YiLCJmaWxlIjoic3JjL2FwcC9maWxlLXN0ZXBwZXIvZmlsZS1zdGVwcGVyLmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiLnJvd1xuICBkaXNwbGF5OiBmbGV4XG4uY29sXG4gIGZsZXg6IDFcbi5zdGVwXG4gIHdoaXRlLXNwYWNlOiBub3dyYXBcbiAgbWFyZ2luLXJpZ2h0OiA1cHhcbiAgbWFyZ2luLWxlZnQ6IDVweFxuICBwYWRkaW5nLXRvcDogMTVweFxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FileStepperComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-file-stepper',
            templateUrl: './file-stepper.component.html',
            styleUrls: ['./file-stepper.component.sass']
          }]
        }], function () {
          return [{
            type: _services_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]
          }];
        }, {
          files: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "EnSQ":
    /*!******************************************!*\
      !*** ./src/app/services/data.service.ts ***!
      \******************************************/

    /*! exports provided: DataService */

    /***/
    function EnSQ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DataService", function () {
        return DataService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs/index */
      "dNeE");
      /* harmony import */


      var rxjs_index__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(rxjs_index__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var DataService = /*#__PURE__*/function () {
        function DataService(httpClient) {
          _classCallCheck(this, DataService);

          this.httpClient = httpClient;
        }

        _createClass(DataService, [{
          key: "startNetwork",
          value: function startNetwork() {
            var _this2 = this;

            this.$data = Object(rxjs_index__WEBPACK_IMPORTED_MODULE_1__["timer"])(0, 3000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (_) {
              return _this2.httpClient.get("http://localhost:8090/visualization");
            }));
          }
        }, {
          key: "getFilesNames",
          value: function getFilesNames() {
            return this.httpClient.get("http://localhost:8090/recordings_list");
          }
        }, {
          key: "getFileData",
          value: function getFileData(fileName) {
            return this.httpClient.get("http://localhost:8090/recording/" + fileName);
          }
        }, {
          key: "getPointsForStatistics",
          value: function getPointsForStatistics() {
            return this.httpClient.get("http://localhost:8090/gamepoints");
          }
        }, {
          key: "updateData",
          value: function updateData(newData) {
            this.$data = Object(rxjs_index__WEBPACK_IMPORTED_MODULE_1__["of"])(newData);
          }
        }]);

        return DataService;
      }();

      DataService.ɵfac = function DataService_Factory(t) {
        return new (t || DataService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]));
      };

      DataService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: DataService,
        factory: DataService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "GIv5":
    /*!*********************************************************************!*\
      !*** ./src/app/robot-controller/buffer-cap/buffer-cap.component.ts ***!
      \*********************************************************************/

    /*! exports provided: BufferCapComponent */

    /***/
    function GIv5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BufferCapComponent", function () {
        return BufferCapComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-formly-helpers */
      "z69a");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _robots__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../robots */
      "ZgY0");
      /* harmony import */


      var _services_endpoint_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../services/endpoint.service */
      "mnBg");
      /* harmony import */


      var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ngx-formly/core */
      "0FS3");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");

      var BufferCapComponent = /*#__PURE__*/function () {
        function BufferCapComponent(endpoint) {
          _classCallCheck(this, BufferCapComponent);

          this.endpoint = endpoint;
          this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({});
          this.model = {};
          this.fields = [ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("robotId", "Robot ID", _robots__WEBPACK_IMPORTED_MODULE_3__["RobotOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredText("machine", "Machine"), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredNumber("shelf", "Shelf")];
        }

        _createClass(BufferCapComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "submit",
          value: function submit() {
            console.log("Model is: ", this.model);
            this.endpoint.post("robot-controller/buffer-cap", this.model).subscribe(function (res) {
              return console.log("Buffer Cap send!");
            });
          }
        }]);

        return BufferCapComponent;
      }();

      BufferCapComponent.ɵfac = function BufferCapComponent_Factory(t) {
        return new (t || BufferCapComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_endpoint_service__WEBPACK_IMPORTED_MODULE_4__["EndpointService"]));
      };

      BufferCapComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: BufferCapComponent,
        selectors: [["app-buffer-cap"]],
        decls: 6,
        vars: 4,
        consts: [[3, "formGroup", "ngSubmit"], [3, "model", "fields", "form"], ["type", "submit", "mat-raised-button", "", "color", "primary"]],
        template: function BufferCapComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Buffer CapStation!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function BufferCapComponent_Template_form_ngSubmit_2_listener() {
              return ctx.submit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "formly-form", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Submit");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("model", ctx.model)("fields", ctx.fields)("form", ctx.form);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_5__["FormlyForm"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JvYm90LWNvbnRyb2xsZXIvYnVmZmVyLWNhcC9idWZmZXItY2FwLmNvbXBvbmVudC5zYXNzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BufferCapComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-buffer-cap',
            templateUrl: './buffer-cap.component.html',
            styleUrls: ['./buffer-cap.component.sass']
          }]
        }], function () {
          return [{
            type: _services_endpoint_service__WEBPACK_IMPORTED_MODULE_4__["EndpointService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "HZFr":
    /*!************************************************************************************!*\
      !*** ./src/app/logistics/contest/active-lock-parts/active-lock-parts.component.ts ***!
      \************************************************************************************/

    /*! exports provided: ActiveLockPartsComponent */

    /***/
    function HZFr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ActiveLockPartsComponent", function () {
        return ActiveLockPartsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/cdk/tree */
      "FvrZ");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/tree */
      "8yBR");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/icon */
      "NFeN");

      var _c0 = function _c0(a0, a1) {
        return {
          "success": a0,
          "inwork": a1
        };
      };

      function ActiveLockPartsComponent_mat_tree_node_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-tree-node", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "li", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var node_r2 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](2, _c0, node_r2.productId == undefined, node_r2.productId != undefined));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" O:", node_r2.productId, " ");
        }
      }

      function ActiveLockPartsComponent_mat_nested_tree_node_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-nested-tree-node");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-icon", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](7, 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var node_r3 = ctx.$implicit;

          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("aria-label", "toggle " + node_r3.name);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r1.treeControl.isExpanded(node_r3) ? "expand_more" : "chevron_right", " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", node_r3.machine, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("example-tree-invisible", !ctx_r1.treeControl.isExpanded(node_r3));
        }
      }

      var ActiveLockPartsComponent = /*#__PURE__*/function () {
        function ActiveLockPartsComponent() {
          _classCallCheck(this, ActiveLockPartsComponent);

          this.treeControl = new _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_1__["NestedTreeControl"](function (node) {
            return node.children;
          });
          this.dataSource = new _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTreeNestedDataSource"]();

          this.hasChild = function (_, node) {
            return !!node.children && node.children.length > 0;
          };
        }

        _createClass(ActiveLockPartsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "updateData",
          value: function updateData() {
            var _this3 = this;

            if (this.data == undefined) {
              return;
            }

            var tmp = this.data.reduce(function (obj, ele) {
              if (obj.filter(function (x) {
                return x.machine == ele.machine;
              })[0] == undefined) {
                obj.push({
                  machine: ele.machine,
                  children: []
                });
              }

              obj.filter(function (x) {
                return x.machine == ele.machine;
              })[0].children.push(ele);
              return obj;
            }, []);
            this.dataSource.data = tmp;
            this.treeControl.dataNodes = tmp;
            this.treeControl.dataNodes.forEach(function (node) {
              return _this3.treeControl.expand(node);
            });
          }
        }, {
          key: "startUpdates",
          value: function startUpdates() {
            var _this4 = this;

            Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["timer"])(0, 3000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (_) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(_this4.updateData());
            })).subscribe();
          }
        }]);

        return ActiveLockPartsComponent;
      }();

      ActiveLockPartsComponent.ɵfac = function ActiveLockPartsComponent_Factory(t) {
        return new (t || ActiveLockPartsComponent)();
      };

      ActiveLockPartsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ActiveLockPartsComponent,
        selectors: [["app-active-lock-parts"]],
        inputs: {
          data: "data"
        },
        decls: 5,
        vars: 3,
        consts: [[1, "example-tree", 3, "dataSource", "treeControl"], ["matTreeNodeToggle", "", 4, "matTreeNodeDef"], [4, "matTreeNodeDef", "matTreeNodeDefWhen"], ["matTreeNodeToggle", ""], [1, "mat-tree-node", 3, "ngClass"], [1, "mat-tree-node"], ["matTreeNodeToggle", "", 1, "mat-icon-rtl-mirror"], ["matTreeNodeOutlet", ""]],
        template: function ActiveLockPartsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "lock");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-tree", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ActiveLockPartsComponent_mat_tree_node_3_Template, 3, 5, "mat-tree-node", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ActiveLockPartsComponent_mat_nested_tree_node_4_Template, 8, 5, "mat-nested-tree-node", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.dataSource)("treeControl", ctx.treeControl);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matTreeNodeDefWhen", ctx.hasChild);
          }
        },
        directives: [_angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTree"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTreeNodeDef"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTreeNode"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTreeNodeToggle"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgClass"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatNestedTreeNode"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIcon"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_4__["MatTreeNodeOutlet"]],
        styles: [".example-tree-invisible[_ngcontent-%COMP%] {\n  display: none;\n}\n\n.example-tree[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%], li[_ngcontent-%COMP%] {\n  margin-top: 0;\n  margin-bottom: 0;\n  list-style-type: none;\n}\n\n.inwork[_ngcontent-%COMP%] {\n  background: greenyellow;\n}\n\n.success[_ngcontent-%COMP%] {\n  background: green;\n}\n\nmat-tree[_ngcontent-%COMP%] {\n  padding-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvYWN0aXZlLWxvY2stcGFydHMvYWN0aXZlLWxvY2stcGFydHMuY29tcG9uZW50LnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtBQUVGOztBQUFBO0VBQ0UsdUJBQUE7QUFHRjs7QUFEQTtFQUNFLGlCQUFBO0FBSUY7O0FBRkE7RUFDRSxtQkFBQTtBQUtGIiwiZmlsZSI6InNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvYWN0aXZlLWxvY2stcGFydHMvYWN0aXZlLWxvY2stcGFydHMuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS10cmVlLWludmlzaWJsZVxuICBkaXNwbGF5OiBub25lXG5cbi5leGFtcGxlLXRyZWUgdWwsIGxpXG4gIG1hcmdpbi10b3A6IDBcbiAgbWFyZ2luLWJvdHRvbTogMFxuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmVcblxuLmlud29ya1xuICBiYWNrZ3JvdW5kOiBncmVlbnllbGxvd1xuXG4uc3VjY2Vzc1xuICBiYWNrZ3JvdW5kOiBncmVlblxuXG5tYXQtdHJlZVxuICBwYWRkaW5nLXJpZ2h0OiAxMHB4XG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ActiveLockPartsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-active-lock-parts',
            templateUrl: './active-lock-parts.component.html',
            styleUrls: ['./active-lock-parts.component.sass']
          }]
        }], function () {
          return [];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "N9jR":
    /*!****************************************************************************************!*\
      !*** ./src/app/logistics/contest/robot-beacon-signal/robot-beacon-signal.component.ts ***!
      \****************************************************************************************/

    /*! exports provided: RobotBeaconSignalComponent */

    /***/
    function N9jR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RobotBeaconSignalComponent", function () {
        return RobotBeaconSignalComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _angular_material_card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/card */
      "Wp6s");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var _c0 = function _c0(a0) {
        return {
          "old": a0
        };
      };

      var RobotBeaconSignalComponent = /*#__PURE__*/function () {
        function RobotBeaconSignalComponent() {
          _classCallCheck(this, RobotBeaconSignalComponent);
        }

        _createClass(RobotBeaconSignalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this5 = this;

            Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["timer"])(0, 1000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["mergeMap"])(function (_) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])((_this5.seconds = Date.now() - _this5.data.localTimestamp) / 1000);
            })).subscribe();
          }
        }]);

        return RobotBeaconSignalComponent;
      }();

      RobotBeaconSignalComponent.ɵfac = function RobotBeaconSignalComponent_Factory(t) {
        return new (t || RobotBeaconSignalComponent)();
      };

      RobotBeaconSignalComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: RobotBeaconSignalComponent,
        selectors: [["app-robot-beacon-signal"]],
        inputs: {
          data: "data"
        },
        decls: 5,
        vars: 6,
        consts: [[3, "ngClass"]],
        template: function RobotBeaconSignalComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](4, _c0, ctx.seconds > 3000));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", ctx.data.robotName, " ", ctx.data.teamColor, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.seconds / 1000, "");
          }
        },
        directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCard"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgClass"]],
        styles: ["mat-card[_ngcontent-%COMP%] {\n  max-width: 250px;\n}\n\n.old[_ngcontent-%COMP%] {\n  background-color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3Qvcm9ib3QtYmVhY29uLXNpZ25hbC9yb2JvdC1iZWFjb24tc2lnbmFsLmNvbXBvbmVudC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQUE7QUFDRjs7QUFDQTtFQUNFLHFCQUFBO0FBRUYiLCJmaWxlIjoic3JjL2FwcC9sb2dpc3RpY3MvY29udGVzdC9yb2JvdC1iZWFjb24tc2lnbmFsL3JvYm90LWJlYWNvbi1zaWduYWwuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtY2FyZFxuICBtYXgtd2lkdGg6IDI1MHB4XG5cbi5vbGRcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RobotBeaconSignalComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-robot-beacon-signal',
            templateUrl: './robot-beacon-signal.component.html',
            styleUrls: ['./robot-beacon-signal.component.sass']
          }]
        }], function () {
          return [];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _header_header_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./header/header.component */
      "fECr");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var AppComponent = function AppComponent() {
        _classCallCheck(this, AppComponent);

        this.title = 'visualization';
      };

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)();
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["app-root"]],
        decls: 3,
        vars: 0,
        consts: [[1, "header"], [1, "container"]],
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-header", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_header_header_component__WEBPACK_IMPORTED_MODULE_1__["HeaderComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]],
        styles: ["[_nghost-%COMP%] {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQUNGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICBoZWlnaHQ6IDEwMCU7XG59XG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "T7Fr":
    /*!****************************************************************************************!*\
      !*** ./src/app/logistics/contest/active-part-demands/active-part-demands.component.ts ***!
      \****************************************************************************************/

    /*! exports provided: ActivePartDemandsComponent */

    /***/
    function T7Fr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ActivePartDemandsComponent", function () {
        return ActivePartDemandsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/cdk/tree */
      "FvrZ");
      /* harmony import */


      var _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/material/tree */
      "8yBR");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/icon */
      "NFeN");

      var _c0 = function _c0(a0, a1) {
        return {
          "tbd": a0,
          "inwork": a1
        };
      };

      function ActivePartDemandsComponent_mat_tree_node_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-tree-node", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "li", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var node_r2 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](3, _c0, node_r2.subProductionTaskId == undefined, node_r2.subProductionTaskId != undefined));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" O:", node_r2.productTaskId, "/T:", node_r2.subProductionTaskId, " ");
        }
      }

      function ActivePartDemandsComponent_mat_nested_tree_node_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-nested-tree-node");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-icon", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](7, 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var node_r3 = ctx.$implicit;

          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("aria-label", "toggle " + node_r3.name);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r1.treeControl.isExpanded(node_r3) ? "expand_more" : "chevron_right", " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", node_r3.machine, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("example-tree-invisible", !ctx_r1.treeControl.isExpanded(node_r3));
        }
      }

      var ActivePartDemandsComponent = /*#__PURE__*/function () {
        function ActivePartDemandsComponent() {
          _classCallCheck(this, ActivePartDemandsComponent);

          this.treeControl = new _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_1__["NestedTreeControl"](function (node) {
            return node.children;
          });
          this.dataSource = new _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatTreeNestedDataSource"]();

          this.hasChild = function (_, node) {
            return !!node.children && node.children.length > 0;
          };
        }

        _createClass(ActivePartDemandsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "updateData",
          value: function updateData() {
            var _this6 = this;

            if (this.data == undefined) {
              return;
            }

            var tmp = this.data.reduce(function (obj, ele) {
              if (obj.filter(function (x) {
                return x.machine == ele.machine;
              })[0] == undefined) {
                obj.push({
                  machine: ele.machine,
                  children: []
                });
              }

              obj.filter(function (x) {
                return x.machine == ele.machine;
              })[0].children.push(ele);
              return obj;
            }, []);
            this.dataSource.data = tmp;
            this.treeControl.dataNodes = tmp;
            this.treeControl.dataNodes.forEach(function (node) {
              return _this6.treeControl.expand(node);
            });
          }
        }]);

        return ActivePartDemandsComponent;
      }();

      ActivePartDemandsComponent.ɵfac = function ActivePartDemandsComponent_Factory(t) {
        return new (t || ActivePartDemandsComponent)();
      };

      ActivePartDemandsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ActivePartDemandsComponent,
        selectors: [["app-active-part-demands"]],
        inputs: {
          data: "data"
        },
        decls: 5,
        vars: 3,
        consts: [[1, "example-tree", 3, "dataSource", "treeControl"], ["matTreeNodeToggle", "", 4, "matTreeNodeDef"], [4, "matTreeNodeDef", "matTreeNodeDefWhen"], ["matTreeNodeToggle", ""], [1, "mat-tree-node", 3, "ngClass"], [1, "mat-tree-node"], ["matTreeNodeToggle", "", 1, "mat-icon-rtl-mirror"], ["matTreeNodeOutlet", ""]],
        template: function ActivePartDemandsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "demands");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-tree", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ActivePartDemandsComponent_mat_tree_node_3_Template, 3, 6, "mat-tree-node", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ActivePartDemandsComponent_mat_nested_tree_node_4_Template, 8, 5, "mat-nested-tree-node", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.dataSource)("treeControl", ctx.treeControl);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matTreeNodeDefWhen", ctx.hasChild);
          }
        },
        directives: [_angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatTree"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatTreeNodeDef"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatTreeNode"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatTreeNodeToggle"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgClass"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatNestedTreeNode"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_4__["MatIcon"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatTreeNodeOutlet"]],
        styles: [".example-tree-invisible[_ngcontent-%COMP%] {\n  display: none;\n}\n\n.example-tree[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%], li[_ngcontent-%COMP%] {\n  margin-top: 0;\n  margin-bottom: 0;\n  list-style-type: none;\n}\n\n.tbd[_ngcontent-%COMP%] {\n  background: gray;\n}\n\n.inwork[_ngcontent-%COMP%] {\n  background: greenyellow;\n}\n\nmat-tree[_ngcontent-%COMP%] {\n  padding-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvYWN0aXZlLXBhcnQtZGVtYW5kcy9hY3RpdmUtcGFydC1kZW1hbmRzLmNvbXBvbmVudC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtBQUNGOztBQUNBO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7QUFFRjs7QUFBQTtFQUNFLGdCQUFBO0FBR0Y7O0FBREE7RUFDRSx1QkFBQTtBQUlGOztBQUZBO0VBQ0UsbUJBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL2xvZ2lzdGljcy9jb250ZXN0L2FjdGl2ZS1wYXJ0LWRlbWFuZHMvYWN0aXZlLXBhcnQtZGVtYW5kcy5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLXRyZWUtaW52aXNpYmxlXG4gIGRpc3BsYXk6IG5vbmVcblxuLmV4YW1wbGUtdHJlZSB1bCwgbGlcbiAgbWFyZ2luLXRvcDogMFxuICBtYXJnaW4tYm90dG9tOiAwXG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZVxuXG4udGJkXG4gIGJhY2tncm91bmQ6IGdyYXlcblxuLmlud29ya1xuICBiYWNrZ3JvdW5kOiBncmVlbnllbGxvd1xuXG5tYXQtdHJlZVxuICBwYWRkaW5nLXJpZ2h0OiAxMHB4XG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ActivePartDemandsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-active-part-demands',
            templateUrl: './active-part-demands.component.html',
            styleUrls: ['./active-part-demands.component.sass']
          }]
        }], function () {
          return [];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "Tpbn":
    /*!****************************************************!*\
      !*** ./src/app/robot-controller/machine-points.ts ***!
      \****************************************************/

    /*! exports provided: MachinePointOptions */

    /***/
    function Tpbn(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MachinePointOptions", function () {
        return MachinePointOptions;
      });

      var MachinePointOptions = [{
        value: "input",
        label: "input"
      }, {
        value: "output",
        label: "output"
      }];
      /***/
    },

    /***/
    "VIXV":
    /*!*****************************************************************!*\
      !*** ./src/app/statistics/game-points/game-points.component.ts ***!
      \*****************************************************************/

    /*! exports provided: GamePointsComponent */

    /***/
    function VIXV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GamePointsComponent", function () {
        return GamePointsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @swimlane/ngx-charts */
      "zQsl");

      var GamePointsComponent = /*#__PURE__*/function () {
        function GamePointsComponent() {
          _classCallCheck(this, GamePointsComponent);

          this.view = [1920, 400];
        }

        _createClass(GamePointsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.preparedData = this.data.map(function (x) {
              console.log(x);
              return {
                name: x.gameName,
                value: x.pointsCyan
              };
            });
          }
        }]);

        return GamePointsComponent;
      }();

      GamePointsComponent.ɵfac = function GamePointsComponent_Factory(t) {
        return new (t || GamePointsComponent)();
      };

      GamePointsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: GamePointsComponent,
        selectors: [["app-game-points"]],
        inputs: {
          data: "data"
        },
        decls: 2,
        vars: 8,
        consts: [[1, "chart-parent"], ["scheme", "picnic", "legendTitle", "Games", "xAxisLabel", "Game", "yAxisLabel", "Points", 3, "results", "showGridLines", "legend", "xAxis", "yAxis", "showXAxisLabel", "showYAxisLabel", "noBarWhenZero"]],
        template: function GamePointsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "ngx-charts-bar-vertical", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("results", ctx.preparedData)("showGridLines", true)("legend", true)("xAxis", true)("yAxis", true)("showXAxisLabel", true)("showYAxisLabel", true)("noBarWhenZero", false);
          }
        },
        directives: [_swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_1__["BarVerticalComponent"]],
        styles: [".chart-parent[_ngcontent-%COMP%] {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3RhdGlzdGljcy9nYW1lLXBvaW50cy9nYW1lLXBvaW50cy5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3N0YXRpc3RpY3MvZ2FtZS1wb2ludHMvZ2FtZS1wb2ludHMuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2hhcnQtcGFyZW50XG4gIGhlaWdodDogMTAwJVxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GamePointsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-game-points',
            templateUrl: './game-points.component.html',
            styleUrls: ['./game-points.component.sass']
          }]
        }], function () {
          return [];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app-routing.module */
      "vY5A");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _logistics_contest_contest_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./logistics/contest/contest.component */
      "f5/C");
      /* harmony import */


      var _logistics_contest_active_product_task_active_product_task_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./logistics/contest/active-product-task/active-product-task.component */
      "uwb6");
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      "R1ws");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _logistics_contest_active_part_demands_active_part_demands_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./logistics/contest/active-part-demands/active-part-demands.component */
      "T7Fr");
      /* harmony import */


      var _logistics_contest_active_lock_parts_active_lock_parts_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./logistics/contest/active-lock-parts/active-lock-parts.component */
      "HZFr");
      /* harmony import */


      var _logistics_contest_robot_beacon_signal_robot_beacon_signal_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./logistics/contest/robot-beacon-signal/robot-beacon-signal.component */
      "N9jR");
      /* harmony import */


      var _logistics_contest_map_map_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./logistics/contest/map/map.component */
      "irMj");
      /* harmony import */


      var _logistics_contest_map_cell_cell_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./logistics/contest/map/cell/cell.component */
      "boUs");
      /* harmony import */


      var _logistics_contest_orders_orders_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./logistics/contest/orders/orders.component */
      "zKIh");
      /* harmony import */


      var _logistics_contest_pipes_minute_seconds_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ./logistics/contest/pipes/minute-seconds.pipe */
      "exDf");
      /* harmony import */


      var _header_header_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./header/header.component */
      "fECr");
      /* harmony import */


      var _file_stepper_file_stepper_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./file-stepper/file-stepper.component */
      "7g7D");
      /* harmony import */


      var _logistics_contest_robots_robots_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./logistics/contest/robots/robots.component */
      "mjzl");
      /* harmony import */


      var _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./statistics/statistics.component */
      "4QAB");
      /* harmony import */


      var _statistics_game_points_game_points_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! ./statistics/game-points/game-points.component */
      "VIXV");
      /* harmony import */


      var _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! @swimlane/ngx-charts */
      "zQsl");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! @angular/material/icon */
      "NFeN");
      /* harmony import */


      var _angular_material_select__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
      /*! @angular/material/select */
      "d3UM");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
      /*! @angular/material/form-field */
      "kmnG");
      /* harmony import */


      var _angular_material_tree__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
      /*! @angular/material/tree */
      "8yBR");
      /* harmony import */


      var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
      /*! @angular/material/tabs */
      "wZkO");
      /* harmony import */


      var _angular_material_card__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
      /*! @angular/material/card */
      "Wp6s");
      /* harmony import */


      var _angular_material_menu__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
      /*! @angular/material/menu */
      "STbY");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
      /*! @angular/material/input */
      "qFsG");
      /* harmony import */


      var _angular_material_table__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
      /*! @angular/material/table */
      "+0xr");
      /* harmony import */


      var _robot_controller_robot_controller_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(
      /*! ./robot-controller/robot-controller.component */
      "7BCm");
      /* harmony import */


      var _robot_controller_move_to_waypoint_move_to_waypoint_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(
      /*! ./robot-controller/move-to-waypoint/move-to-waypoint.component */
      "bYgO");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(
      /*! @ngx-formly/core */
      "0FS3");
      /* harmony import */


      var _ngx_formly_material__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(
      /*! @ngx-formly/material */
      "Gm3Y");
      /* harmony import */


      var _robot_controller_get_product_get_product_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(
      /*! ./robot-controller/get-product/get-product.component */
      "azNi");
      /* harmony import */


      var _robot_controller_deliver_product_deliver_product_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(
      /*! ./robot-controller/deliver-product/deliver-product.component */
      "7Bhy");
      /* harmony import */


      var _robot_controller_explore_machine_explore_machine_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(
      /*! ./robot-controller/explore-machine/explore-machine.component */
      "acWC");
      /* harmony import */


      var _robot_controller_buffer_cap_buffer_cap_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(
      /*! ./robot-controller/buffer-cap/buffer-cap.component */
      "GIv5");

      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function AppModule_Factory(t) {
          return new (t || AppModule)();
        },
        providers: [],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__["MatIconModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_28__["MatButtonModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_22__["MatSelectModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_23__["MatFormFieldModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_27__["MatMenuModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_29__["MatInputModule"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_24__["MatTreeModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_25__["MatTabsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["NoopAnimationsModule"], _angular_material_table__WEBPACK_IMPORTED_MODULE_30__["MatTableModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_26__["MatCardModule"], _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_20__["NgxChartsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_33__["ReactiveFormsModule"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_34__["FormlyModule"].forRoot(), _ngx_formly_material__WEBPACK_IMPORTED_MODULE_35__["FormlyMaterialModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _logistics_contest_contest_component__WEBPACK_IMPORTED_MODULE_4__["ContestComponent"], _logistics_contest_active_product_task_active_product_task_component__WEBPACK_IMPORTED_MODULE_5__["ActiveProductTaskComponent"], _logistics_contest_active_part_demands_active_part_demands_component__WEBPACK_IMPORTED_MODULE_8__["ActivePartDemandsComponent"], _logistics_contest_active_lock_parts_active_lock_parts_component__WEBPACK_IMPORTED_MODULE_9__["ActiveLockPartsComponent"], _logistics_contest_robot_beacon_signal_robot_beacon_signal_component__WEBPACK_IMPORTED_MODULE_10__["RobotBeaconSignalComponent"], _logistics_contest_map_map_component__WEBPACK_IMPORTED_MODULE_11__["MapComponent"], _logistics_contest_map_cell_cell_component__WEBPACK_IMPORTED_MODULE_12__["CellComponent"], _logistics_contest_orders_orders_component__WEBPACK_IMPORTED_MODULE_13__["OrdersComponent"], _logistics_contest_pipes_minute_seconds_pipe__WEBPACK_IMPORTED_MODULE_14__["MinuteSecondsPipe"], _header_header_component__WEBPACK_IMPORTED_MODULE_15__["HeaderComponent"], _file_stepper_file_stepper_component__WEBPACK_IMPORTED_MODULE_16__["FileStepperComponent"], _logistics_contest_robots_robots_component__WEBPACK_IMPORTED_MODULE_17__["RobotsComponent"], _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_18__["StatisticsComponent"], _statistics_game_points_game_points_component__WEBPACK_IMPORTED_MODULE_19__["GamePointsComponent"], _robot_controller_robot_controller_component__WEBPACK_IMPORTED_MODULE_31__["RobotControllerComponent"], _robot_controller_move_to_waypoint_move_to_waypoint_component__WEBPACK_IMPORTED_MODULE_32__["MoveToWaypointComponent"], _robot_controller_get_product_get_product_component__WEBPACK_IMPORTED_MODULE_36__["GetProductComponent"], _robot_controller_deliver_product_deliver_product_component__WEBPACK_IMPORTED_MODULE_37__["DeliverProductComponent"], _robot_controller_explore_machine_explore_machine_component__WEBPACK_IMPORTED_MODULE_38__["ExploreMachineComponent"], _robot_controller_buffer_cap_buffer_cap_component__WEBPACK_IMPORTED_MODULE_39__["BufferCapComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__["MatIconModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_28__["MatButtonModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_22__["MatSelectModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_23__["MatFormFieldModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_27__["MatMenuModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_29__["MatInputModule"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_24__["MatTreeModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_25__["MatTabsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["NoopAnimationsModule"], _angular_material_table__WEBPACK_IMPORTED_MODULE_30__["MatTableModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_26__["MatCardModule"], _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_20__["NgxChartsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_33__["ReactiveFormsModule"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_34__["FormlyModule"], _ngx_formly_material__WEBPACK_IMPORTED_MODULE_35__["FormlyMaterialModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _logistics_contest_contest_component__WEBPACK_IMPORTED_MODULE_4__["ContestComponent"], _logistics_contest_active_product_task_active_product_task_component__WEBPACK_IMPORTED_MODULE_5__["ActiveProductTaskComponent"], _logistics_contest_active_part_demands_active_part_demands_component__WEBPACK_IMPORTED_MODULE_8__["ActivePartDemandsComponent"], _logistics_contest_active_lock_parts_active_lock_parts_component__WEBPACK_IMPORTED_MODULE_9__["ActiveLockPartsComponent"], _logistics_contest_robot_beacon_signal_robot_beacon_signal_component__WEBPACK_IMPORTED_MODULE_10__["RobotBeaconSignalComponent"], _logistics_contest_map_map_component__WEBPACK_IMPORTED_MODULE_11__["MapComponent"], _logistics_contest_map_cell_cell_component__WEBPACK_IMPORTED_MODULE_12__["CellComponent"], _logistics_contest_orders_orders_component__WEBPACK_IMPORTED_MODULE_13__["OrdersComponent"], _logistics_contest_pipes_minute_seconds_pipe__WEBPACK_IMPORTED_MODULE_14__["MinuteSecondsPipe"], _header_header_component__WEBPACK_IMPORTED_MODULE_15__["HeaderComponent"], _file_stepper_file_stepper_component__WEBPACK_IMPORTED_MODULE_16__["FileStepperComponent"], _logistics_contest_robots_robots_component__WEBPACK_IMPORTED_MODULE_17__["RobotsComponent"], _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_18__["StatisticsComponent"], _statistics_game_points_game_points_component__WEBPACK_IMPORTED_MODULE_19__["GamePointsComponent"], _robot_controller_robot_controller_component__WEBPACK_IMPORTED_MODULE_31__["RobotControllerComponent"], _robot_controller_move_to_waypoint_move_to_waypoint_component__WEBPACK_IMPORTED_MODULE_32__["MoveToWaypointComponent"], _robot_controller_get_product_get_product_component__WEBPACK_IMPORTED_MODULE_36__["GetProductComponent"], _robot_controller_deliver_product_deliver_product_component__WEBPACK_IMPORTED_MODULE_37__["DeliverProductComponent"], _robot_controller_explore_machine_explore_machine_component__WEBPACK_IMPORTED_MODULE_38__["ExploreMachineComponent"], _robot_controller_buffer_cap_buffer_cap_component__WEBPACK_IMPORTED_MODULE_39__["BufferCapComponent"]],
            imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_21__["MatIconModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_28__["MatButtonModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_22__["MatSelectModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_23__["MatFormFieldModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_27__["MatMenuModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_29__["MatInputModule"], _angular_material_tree__WEBPACK_IMPORTED_MODULE_24__["MatTreeModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_25__["MatTabsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["NoopAnimationsModule"], _angular_material_table__WEBPACK_IMPORTED_MODULE_30__["MatTableModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_26__["MatCardModule"], _swimlane_ngx_charts__WEBPACK_IMPORTED_MODULE_20__["NgxChartsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_33__["ReactiveFormsModule"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_34__["FormlyModule"].forRoot(), _ngx_formly_material__WEBPACK_IMPORTED_MODULE_35__["FormlyMaterialModule"]],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "ZgY0":
    /*!********************************************!*\
      !*** ./src/app/robot-controller/robots.ts ***!
      \********************************************/

    /*! exports provided: RobotOptions */

    /***/
    function ZgY0(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RobotOptions", function () {
        return RobotOptions;
      });

      var RobotOptions = [{
        value: "1",
        label: "Robot 1"
      }, {
        value: "2",
        label: "Robot 2"
      }, {
        value: "3",
        label: "Robot 3"
      }];
      /***/
    },

    /***/
    "acWC":
    /*!*******************************************************************************!*\
      !*** ./src/app/robot-controller/explore-machine/explore-machine.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: ExploreMachineComponent */

    /***/
    function acWC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExploreMachineComponent", function () {
        return ExploreMachineComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-formly-helpers */
      "z69a");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _robots__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../robots */
      "ZgY0");
      /* harmony import */


      var _machines__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../machines */
      "2Hn8");
      /* harmony import */


      var _machine_points__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../machine-points */
      "Tpbn");
      /* harmony import */


      var _services_endpoint_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../services/endpoint.service */
      "mnBg");
      /* harmony import */


      var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-formly/core */
      "0FS3");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");

      var ExploreMachineComponent = /*#__PURE__*/function () {
        function ExploreMachineComponent(endpoint) {
          _classCallCheck(this, ExploreMachineComponent);

          this.endpoint = endpoint;
          this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({});
          this.model = {};
          this.fields = [ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("robotId", "Robot ID", _robots__WEBPACK_IMPORTED_MODULE_3__["RobotOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("machine", "Machine", _machines__WEBPACK_IMPORTED_MODULE_4__["MachineOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredText("zone", "Zone"), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("side", "Side", _machine_points__WEBPACK_IMPORTED_MODULE_5__["MachinePointOptions"])];
        }

        _createClass(ExploreMachineComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "submit",
          value: function submit() {
            console.log("Model is: ", this.model);
            this.endpoint.post("robot-controller/explore-machine", this.model).subscribe(function (res) {
              return console.log("Explore Machine send!");
            });
          }
        }]);

        return ExploreMachineComponent;
      }();

      ExploreMachineComponent.ɵfac = function ExploreMachineComponent_Factory(t) {
        return new (t || ExploreMachineComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_endpoint_service__WEBPACK_IMPORTED_MODULE_6__["EndpointService"]));
      };

      ExploreMachineComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ExploreMachineComponent,
        selectors: [["app-explore-machine"]],
        decls: 6,
        vars: 4,
        consts: [[3, "formGroup", "ngSubmit"], [3, "model", "fields", "form"], ["type", "submit", "mat-raised-button", "", "color", "primary"]],
        template: function ExploreMachineComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Explore Machine!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ExploreMachineComponent_Template_form_ngSubmit_2_listener() {
              return ctx.submit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "formly-form", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Submit");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("model", ctx.model)("fields", ctx.fields)("form", ctx.form);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_7__["FormlyForm"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButton"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JvYm90LWNvbnRyb2xsZXIvZXhwbG9yZS1tYWNoaW5lL2V4cGxvcmUtbWFjaGluZS5jb21wb25lbnQuc2FzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ExploreMachineComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-explore-machine',
            templateUrl: './explore-machine.component.html',
            styleUrls: ['./explore-machine.component.sass']
          }]
        }], function () {
          return [{
            type: _services_endpoint_service__WEBPACK_IMPORTED_MODULE_6__["EndpointService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "azNi":
    /*!***********************************************************************!*\
      !*** ./src/app/robot-controller/get-product/get-product.component.ts ***!
      \***********************************************************************/

    /*! exports provided: GetProductComponent */

    /***/
    function azNi(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GetProductComponent", function () {
        return GetProductComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-formly-helpers */
      "z69a");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../services/endpoint.service */
      "mnBg");
      /* harmony import */


      var _robots__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../robots */
      "ZgY0");
      /* harmony import */


      var _machines__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../machines */
      "2Hn8");
      /* harmony import */


      var _machine_points__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../machine-points */
      "Tpbn");
      /* harmony import */


      var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngx-formly/core */
      "0FS3");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");

      var GetProductComponent = /*#__PURE__*/function () {
        function GetProductComponent(endpoint) {
          _classCallCheck(this, GetProductComponent);

          this.endpoint = endpoint;
          this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({});
          this.model = {};
          this.fields = [ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("robotId", "Robot ID", _robots__WEBPACK_IMPORTED_MODULE_4__["RobotOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("machine", "Machine", _machines__WEBPACK_IMPORTED_MODULE_5__["MachineOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("machinePoint", "MachinePoint", _machine_points__WEBPACK_IMPORTED_MODULE_6__["MachinePointOptions"])];
        }

        _createClass(GetProductComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "submit",
          value: function submit() {
            console.log("Model is: ", this.model);
            this.endpoint.post("robot-controller/get-product", this.model).subscribe(function (res) {
              return console.log("Get Product send!");
            });
          }
        }]);

        return GetProductComponent;
      }();

      GetProductComponent.ɵfac = function GetProductComponent_Factory(t) {
        return new (t || GetProductComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__["EndpointService"]));
      };

      GetProductComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: GetProductComponent,
        selectors: [["app-get-product"]],
        decls: 6,
        vars: 4,
        consts: [[3, "formGroup", "ngSubmit"], [3, "model", "fields", "form"], ["type", "submit", "mat-raised-button", "", "color", "primary"]],
        template: function GetProductComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Get Product!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function GetProductComponent_Template_form_ngSubmit_2_listener() {
              return ctx.submit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "formly-form", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Submit");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("model", ctx.model)("fields", ctx.fields)("form", ctx.form);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_7__["FormlyForm"], _angular_material_button__WEBPACK_IMPORTED_MODULE_8__["MatButton"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JvYm90LWNvbnRyb2xsZXIvZ2V0LXByb2R1Y3QvZ2V0LXByb2R1Y3QuY29tcG9uZW50LnNhc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GetProductComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-get-product',
            templateUrl: './get-product.component.html',
            styleUrls: ['./get-product.component.sass']
          }]
        }], function () {
          return [{
            type: _services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__["EndpointService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "bYgO":
    /*!*********************************************************************************!*\
      !*** ./src/app/robot-controller/move-to-waypoint/move-to-waypoint.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: MoveToWaypointComponent */

    /***/
    function bYgO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MoveToWaypointComponent", function () {
        return MoveToWaypointComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ngx-formly-helpers */
      "z69a");
      /* harmony import */


      var ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../services/endpoint.service */
      "mnBg");
      /* harmony import */


      var _robots__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../robots */
      "ZgY0");
      /* harmony import */


      var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ngx-formly/core */
      "0FS3");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");

      var MoveToWaypointComponent = /*#__PURE__*/function () {
        function MoveToWaypointComponent(endpoint) {
          _classCallCheck(this, MoveToWaypointComponent);

          this.endpoint = endpoint;
          this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({});
          this.model = {};
          this.fields = [ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredSelect("robotId", "Robot ID", _robots__WEBPACK_IMPORTED_MODULE_4__["RobotOptions"]), ngx_formly_helpers__WEBPACK_IMPORTED_MODULE_2__["formly"].requiredText("zone", "Zone")];
        }

        _createClass(MoveToWaypointComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "submit",
          value: function submit() {
            console.log("Model is: ", this.model);
            this.endpoint.post("robot-controller/move-to", this.model).subscribe(function (res) {
              return console.log("Go To send!");
            });
          }
        }]);

        return MoveToWaypointComponent;
      }();

      MoveToWaypointComponent.ɵfac = function MoveToWaypointComponent_Factory(t) {
        return new (t || MoveToWaypointComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__["EndpointService"]));
      };

      MoveToWaypointComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: MoveToWaypointComponent,
        selectors: [["app-move-to-waypoint"]],
        decls: 6,
        vars: 4,
        consts: [[3, "formGroup", "ngSubmit"], [3, "model", "fields", "form"], ["type", "submit", "mat-raised-button", "", "color", "primary"]],
        template: function MoveToWaypointComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Move to waypoint!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function MoveToWaypointComponent_Template_form_ngSubmit_2_listener() {
              return ctx.submit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "formly-form", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Submit");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("model", ctx.model)("fields", ctx.fields)("form", ctx.form);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _ngx_formly_core__WEBPACK_IMPORTED_MODULE_5__["FormlyForm"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JvYm90LWNvbnRyb2xsZXIvbW92ZS10by13YXlwb2ludC9tb3ZlLXRvLXdheXBvaW50LmNvbXBvbmVudC5zYXNzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MoveToWaypointComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-move-to-waypoint',
            templateUrl: './move-to-waypoint.component.html',
            styleUrls: ['./move-to-waypoint.component.sass']
          }]
        }], function () {
          return [{
            type: _services_endpoint_service__WEBPACK_IMPORTED_MODULE_3__["EndpointService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "boUs":
    /*!**************************************************************!*\
      !*** ./src/app/logistics/contest/map/cell/cell.component.ts ***!
      \**************************************************************/

    /*! exports provided: CellComponent */

    /***/
    function boUs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CellComponent", function () {
        return CellComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function CellComponent_div_0_span_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("(", ctx_r1.data.machine, ")");
        }
      }

      function CellComponent_div_0_p_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " NULL ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0, a1, a2) {
        return {
          "free": a0,
          "machine": a1,
          "blocked": a2
        };
      };

      function CellComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, CellComponent_div_0_span_3_Template, 2, 1, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, CellComponent_div_0_p_4_Template, 2, 0, "p", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](4, _c0, ctx_r0.data.zoneOccupiedState == "FREE", ctx_r0.data.zoneOccupiedState == "MACHINE", ctx_r0.data.zoneOccupiedState == "BLOCKED"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.data.zoneName, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.data.machine != null);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.data == null);
        }
      }

      var CellComponent = /*#__PURE__*/function () {
        function CellComponent() {
          _classCallCheck(this, CellComponent);
        }

        _createClass(CellComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CellComponent;
      }();

      CellComponent.ɵfac = function CellComponent_Factory(t) {
        return new (t || CellComponent)();
      };

      CellComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: CellComponent,
        selectors: [["app-cell"]],
        inputs: {
          data: "data"
        },
        decls: 1,
        vars: 1,
        consts: [["class", "container", 3, "ngClass", 4, "ngIf"], [1, "container", 3, "ngClass"], [4, "ngIf"]],
        template: function CellComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, CellComponent_div_0_Template, 5, 8, "div", 0);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.data != null);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"]],
        styles: [".free[_ngcontent-%COMP%] {\n  background-color: white;\n}\n\n.machine[_ngcontent-%COMP%] {\n  background-color: grey;\n}\n\n.blocked[_ngcontent-%COMP%] {\n  background-color: black;\n  color: white;\n}\n\n.container[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  padding: 22px 0px;\n}\n\n.container[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvbWFwL2NlbGwvY2VsbC5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxzQkFBQTtBQUVGOztBQUFBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FBR0Y7O0FBRkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBS0Y7O0FBSkU7RUFDRSxrQkFBQTtFQUNBLFdBQUE7QUFNSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2lzdGljcy9jb250ZXN0L21hcC9jZWxsL2NlbGwuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnJlZVxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZVxuXG4ubWFjaGluZVxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5XG5cbi5ibG9ja2VkXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrXG4gIGNvbG9yOiB3aGl0ZVxuLmNvbnRhaW5lclxuICB3aWR0aDogMTAwJVxuICBoZWlnaHQ6IDEwMCVcbiAgcGFkZGluZzogMjJweCAwcHhcbiAgcFxuICAgIHRleHQtYWxpZ246IGNlbnRlclxuICAgIG1hcmdpbjogMHB4XG5cbiJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CellComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-cell',
            templateUrl: './cell.component.html',
            styleUrls: ['./cell.component.sass']
          }]
        }], function () {
          return [];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "crnd":
    /*!**********************************************************!*\
      !*** ./src/$$_lazy_route_resource lazy namespace object ***!
      \**********************************************************/

    /*! no static exports found */

    /***/
    function crnd(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "crnd";
      /***/
    },

    /***/
    "exDf":
    /*!****************************************************************!*\
      !*** ./src/app/logistics/contest/pipes/minute-seconds.pipe.ts ***!
      \****************************************************************/

    /*! exports provided: MinuteSecondsPipe */

    /***/
    function exDf(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MinuteSecondsPipe", function () {
        return MinuteSecondsPipe;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var MinuteSecondsPipe = /*#__PURE__*/function () {
        function MinuteSecondsPipe() {
          _classCallCheck(this, MinuteSecondsPipe);
        }

        _createClass(MinuteSecondsPipe, [{
          key: "transform",
          value: function transform(value, args) {
            value = Math.floor(value);
            var minutes = Math.floor(value / 60);
            var seconds = value - minutes * 60;

            if (seconds < 10) {
              seconds = "0" + seconds;
            }

            if (minutes < 10) {
              minutes = "0" + minutes;
            }

            return minutes + ':' + seconds;
          }
        }]);

        return MinuteSecondsPipe;
      }();

      MinuteSecondsPipe.ɵfac = function MinuteSecondsPipe_Factory(t) {
        return new (t || MinuteSecondsPipe)();
      };

      MinuteSecondsPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
        name: "minuteSeconds",
        type: MinuteSecondsPipe,
        pure: true
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MinuteSecondsPipe, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
          args: [{
            name: 'minuteSeconds'
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "f5/C":
    /*!********************************************************!*\
      !*** ./src/app/logistics/contest/contest.component.ts ***!
      \********************************************************/

    /*! exports provided: ContestComponent */

    /***/
    function f5C(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContestComponent", function () {
        return ContestComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _active_part_demands_active_part_demands_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./active-part-demands/active-part-demands.component */
      "T7Fr");
      /* harmony import */


      var _active_lock_parts_active_lock_parts_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./active-lock-parts/active-lock-parts.component */
      "HZFr");
      /* harmony import */


      var _services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../services/data.service */
      "EnSQ");
      /* harmony import */


      var _map_map_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./map/map.component */
      "irMj");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/tabs */
      "wZkO");
      /* harmony import */


      var _orders_orders_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./orders/orders.component */
      "zKIh");
      /* harmony import */


      var _robots_robots_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./robots/robots.component */
      "mjzl");
      /* harmony import */


      var _robot_beacon_signal_robot_beacon_signal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./robot-beacon-signal/robot-beacon-signal.component */
      "N9jR");
      /* harmony import */


      var _active_product_task_active_product_task_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./active-product-task/active-product-task.component */
      "uwb6");

      function ContestComponent_mat_tab_group_0_div_7_app_robot_beacon_signal_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-robot-beacon-signal", 5);
        }

        if (rf & 2) {
          var task_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", task_r5);
        }
      }

      function ContestComponent_mat_tab_group_0_div_7_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContestComponent_mat_tab_group_0_div_7_app_robot_beacon_signal_1_Template, 1, 1, "app-robot-beacon-signal", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var task_r5 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", task_r5 != null);
        }
      }

      function ContestComponent_mat_tab_group_0_div_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-active-product-task", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var task_r8 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", task_r8)("shrink", false);
        }
      }

      function ContestComponent_mat_tab_group_0_app_map_12_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-map", 17);
        }

        if (rf & 2) {
          var data_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().ngIf;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", data_r1.explorationZones)("robot", data_r1.robotBeaconSignals);
        }
      }

      function ContestComponent_mat_tab_group_0_Template(rf, ctx) {
        if (rf & 1) {
          var _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-tab-group", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectedTabChange", function ContestComponent_mat_tab_group_0_Template_mat_tab_group_selectedTabChange_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11);

            var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r10.onTabChange($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-tab", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-active-part-demands", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Robots");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ContestComponent_mat_tab_group_0_div_7_Template, 2, 1, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "app-active-lock-parts", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, ContestComponent_mat_tab_group_0_div_10_Template, 2, 2, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-tab", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, ContestComponent_mat_tab_group_0_app_map_12_Template, 1, 2, "app-map", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "mat-tab", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "app-orders", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-tab", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "app-robots", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var data_r1 = ctx.ngIf;

          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", data_r1.activePartDemands);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", data_r1.robotBeaconSignals);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", data_r1.activeLockParts);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", data_r1.activeProductTasks);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.tabIndex == 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("orders", data_r1.productOrders)("rings", data_r1.rings);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("robots", data_r1.robotBeaconSignals);
        }
      }

      var ContestComponent = /*#__PURE__*/function () {
        function ContestComponent(dataService) {
          _classCallCheck(this, ContestComponent);

          this.dataService = dataService;
        }

        _createClass(ContestComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            /*
            this.tabIndex = 0;
            //this.loadStatic();
            timer(0, 3000).pipe(
              mergeMap(_ => {
                  if (this.partDemands != undefined) {
                    return of(this.partDemands.updateData());
                  } else {
                    //console.log('partsDemands null!');
                    return of({});
                  }
                }
              )).subscribe();
                 timer(0, 3000).pipe(
              mergeMap(_ => {
                  if (this.partDemands != undefined) {
                    return of(this.lockParts.updateData());
                  } else {
                    //console.log('lockParts null!');
                    return of({});
                  }
                }
              )).subscribe();
                 timer(0, 1000).pipe(
              mergeMap(_ => {
                  if (this.map != undefined) {
                    return of(this.map.update());
                  } else {
                    //console.log('map is null!');
                    return of({});
                  }
                }
              )).subscribe();
                  */
          }
        }, {
          key: "test",
          value: function test() {
            console.log("In Test!!");
          }
        }, {
          key: "onTabChange",
          value: function onTabChange($event) {
            console.log("Here: $event");
            console.log($event);
            this.tabIndex = $event.index;
          }
        }]);

        return ContestComponent;
      }();

      ContestComponent.ɵfac = function ContestComponent_Factory(t) {
        return new (t || ContestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]));
      };

      ContestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ContestComponent,
        selectors: [["app-contest"]],
        viewQuery: function ContestComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_active_part_demands_active_part_demands_component__WEBPACK_IMPORTED_MODULE_1__["ActivePartDemandsComponent"], true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_active_lock_parts_active_lock_parts_component__WEBPACK_IMPORTED_MODULE_2__["ActiveLockPartsComponent"], true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_map_map_component__WEBPACK_IMPORTED_MODULE_4__["MapComponent"], true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.partDemands = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.lockParts = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.map = _t.first);
          }
        },
        decls: 2,
        vars: 3,
        consts: [[3, "selectedTabChange", 4, "ngIf"], [3, "selectedTabChange"], ["label", "Overview"], [1, "flex-container"], [1, "flex-shrink"], [3, "data"], [4, "ngFor", "ngForOf"], ["class", "flex-element", 4, "ngFor", "ngForOf"], ["label", "Map"], [3, "data", "robot", 4, "ngIf"], ["label", "Orders"], [3, "orders", "rings"], ["label", "Robots"], [3, "robots"], [3, "data", 4, "ngIf"], [1, "flex-element"], [3, "data", "shrink"], [3, "data", "robot"]],
        template: function ContestComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ContestComponent_mat_tab_group_0_Template, 17, 8, "mat-tab-group", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](1, "async");
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](1, 1, ctx.dataService.$data));
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_6__["MatTabGroup"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_6__["MatTab"], _active_part_demands_active_part_demands_component__WEBPACK_IMPORTED_MODULE_1__["ActivePartDemandsComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _active_lock_parts_active_lock_parts_component__WEBPACK_IMPORTED_MODULE_2__["ActiveLockPartsComponent"], _orders_orders_component__WEBPACK_IMPORTED_MODULE_7__["OrdersComponent"], _robots_robots_component__WEBPACK_IMPORTED_MODULE_8__["RobotsComponent"], _robot_beacon_signal_robot_beacon_signal_component__WEBPACK_IMPORTED_MODULE_9__["RobotBeaconSignalComponent"], _active_product_task_active_product_task_component__WEBPACK_IMPORTED_MODULE_10__["ActiveProductTaskComponent"], _map_map_component__WEBPACK_IMPORTED_MODULE_4__["MapComponent"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["AsyncPipe"]],
        styles: [".flex-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n}\n\n.flex-element[_ngcontent-%COMP%] {\n  flex-grow: 1;\n}\n\n.flex-shrink[_ngcontent-%COMP%] {\n  flex-shrink: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvY29udGVzdC5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUNBO0VBQ0UsWUFBQTtBQUVGOztBQURBO0VBQ0UsY0FBQTtBQUlGIiwiZmlsZSI6InNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvY29udGVzdC5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mbGV4LWNvbnRhaW5lclxuICBkaXNwbGF5OiBmbGV4XG4gIGZsZXgtZGlyZWN0aW9uOiByb3dcblxuLmZsZXgtZWxlbWVudFxuICBmbGV4LWdyb3c6IDFcbi5mbGV4LXNocmlua1xuICBmbGV4LXNocmluazogMVxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContestComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-contest',
            templateUrl: './contest.component.html',
            styleUrls: ['./contest.component.sass']
          }]
        }], function () {
          return [{
            type: _services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]
          }];
        }, {
          partDemands: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: [_active_part_demands_active_part_demands_component__WEBPACK_IMPORTED_MODULE_1__["ActivePartDemandsComponent"]]
          }],
          lockParts: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: [_active_lock_parts_active_lock_parts_component__WEBPACK_IMPORTED_MODULE_2__["ActiveLockPartsComponent"]]
          }],
          map: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: [_map_map_component__WEBPACK_IMPORTED_MODULE_4__["MapComponent"]]
          }]
        });
      })();
      /***/

    },

    /***/
    "fECr":
    /*!********************************************!*\
      !*** ./src/app/header/header.component.ts ***!
      \********************************************/

    /*! exports provided: HeaderComponent */

    /***/
    function fECr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderComponent", function () {
        return HeaderComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../services/data.service */
      "EnSQ");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/material/button */
      "bTqV");
      /* harmony import */


      var _angular_material_menu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/menu */
      "STbY");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _file_stepper_file_stepper_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../file-stepper/file-stepper.component */
      "7g7D");

      function HeaderComponent_app_file_stepper_18_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-file-stepper", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](1, "async");
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("files", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](1, 1, ctx_r1.fileList));
        }
      }

      var HeaderComponent = /*#__PURE__*/function () {
        function HeaderComponent(dataService) {
          _classCallCheck(this, HeaderComponent);

          this.dataService = dataService;
          this.networkActive = true;
          this.fileActive = false;
          this.fileColor = '';
          this.networkColor = 'primary';
        }

        _createClass(HeaderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.dataService.startNetwork();
          }
        }, {
          key: "networkClicked",
          value: function networkClicked() {
            this.fileList = undefined;
            this.networkActive = true;
            this.fileActive = false;
            this.fileColor = '';
            this.networkColor = 'primary';
          }
        }, {
          key: "fileClicked",
          value: function fileClicked() {
            this.fileList = this.dataService.getFilesNames();
            this.networkActive = false;
            this.fileActive = true;
            this.fileColor = 'primary';
            this.networkColor = '';
          }
        }]);

        return HeaderComponent;
      }();

      HeaderComponent.ɵfac = function HeaderComponent_Factory(t) {
        return new (t || HeaderComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]));
      };

      HeaderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: HeaderComponent,
        selectors: [["app-header"]],
        decls: 20,
        vars: 4,
        consts: [[1, "row"], ["mat-button", "", 3, "matMenuTriggerFor"], ["menu", "matMenu"], ["mat-menu-item", "", "routerLink", "contest"], ["mat-menu-item", "", "routerLink", "statistics"], ["mat-menu-item", "", "routerLink", "robot-controller"], [1, "col"], ["mat-raised-button", "", 3, "color", "click"], [3, "files", 4, "ngIf"], [1, "grow"], [3, "files"]],
        template: function HeaderComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Sites");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-menu", null, 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Contest");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Statistics");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Robot Controller");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "button", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_12_listener() {
              return ctx.networkClicked();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Network");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_15_listener() {
              return ctx.fileClicked();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Load File");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, HeaderComponent_app_file_stepper_18_Template, 2, 3, "app-file-stepper", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matMenuTriggerFor", _r0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("color", ctx.networkColor);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("color", ctx.fileColor);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.fileActive);
          }
        },
        directives: [_angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButton"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_3__["MatMenuTrigger"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_3__["_MatMenu"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_3__["MatMenuItem"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _file_stepper_file_stepper_component__WEBPACK_IMPORTED_MODULE_6__["FileStepperComponent"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["AsyncPipe"]],
        styles: [".row[_ngcontent-%COMP%] {\n  display: flex;\n}\n\n.col[_ngcontent-%COMP%] {\n  flex-shrink: 1;\n}\n\n.grow[_ngcontent-%COMP%] {\n  flex-grow: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7QUFDRjs7QUFBQTtFQUNFLGNBQUE7QUFHRjs7QUFGQTtFQUNFLFlBQUE7QUFLRiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93XG4gIGRpc3BsYXk6IGZsZXhcbi5jb2xcbiAgZmxleC1zaHJpbms6IDFcbi5ncm93XG4gIGZsZXgtZ3JvdzogMVxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HeaderComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.sass']
          }]
        }], function () {
          return [{
            type: _services_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "irMj":
    /*!********************************************************!*\
      !*** ./src/app/logistics/contest/map/map.component.ts ***!
      \********************************************************/

    /*! exports provided: MapComponent */

    /***/
    function irMj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MapComponent", function () {
        return MapComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var konva_lib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! konva/lib */
      "Ni3L");
      /* harmony import */


      var konva_lib__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(konva_lib__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var MapComponent = /*#__PURE__*/function () {
        function MapComponent() {
          _classCallCheck(this, MapComponent);

          this.offsetYForHeader = 300;
          this.robotLayerOffsetY = 800;
          this.robotsKonvaNodes = [];
          this.robotsKonvaNodes.push(this.robotToCircle(0, 1, 5));
          this.robotsKonvaNodes.push(this.robotToCircle(1, 2, 5));
          this.robotsKonvaNodes.push(this.robotToCircle(2, 3, 5));
        }

        _createClass(MapComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this7 = this;

            console.log(this.robot);
            this.machineLayer = new konva_lib__WEBPACK_IMPORTED_MODULE_1__["Layer"]({
              offsetX: -800,
              offsetY: window.innerHeight - this.offsetYForHeader,
              scaleY: -1
            });
            this.robotLayer = new konva_lib__WEBPACK_IMPORTED_MODULE_1__["Layer"]({
              offsetX: 0,
              offsetY: -1 * this.robotLayerOffsetY,
              scaleY: 1,
              scaleX: 1
            });
            this.robotsKonvaNodes.forEach(function (node) {
              return _this7.robotLayer.add(node.circle);
            });
            this.robotsKonvaNodes.forEach(function (node) {
              return _this7.robotLayer.add(node.label);
            });
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            var _this8 = this;

            this.stage = new konva_lib__WEBPACK_IMPORTED_MODULE_1__["Stage"]({
              scrollable: true,
              container: 'container',
              width: window.innerWidth - 50,
              height: window.innerHeight - this.offsetYForHeader
            });
            this.stage.add(this.machineLayer);
            this.stage.add(this.robotLayer);
            var filteredData = this.data.filter(function (x) {
              return x.machine != null;
            });
            console.log(this.data);
            console.log("filtered", filteredData); //data is based in Centimeters!

            filteredData.forEach(function (machine) {
              console.log("Machine placing at x", machine.zoneCenter.x, "y", machine.zoneCenter.y);
              console.log(machine);
              var team = machine.machine.split("-")[0];
              var name = machine.machine.split("-")[1];
              console.log("team is, ", team);
              var color = 'cyan';

              if (team == 'M') {
                color = 'magenta';
              }

              var square = {
                circle: new konva_lib__WEBPACK_IMPORTED_MODULE_1__["Rect"]({
                  x: machine.zoneCenter.x * 100,
                  y: machine.zoneCenter.y * 100,
                  width: 100,
                  height: 50,
                  fill: color,
                  stroke: 'black',
                  strokeWidth: 4
                }),
                label: new konva_lib__WEBPACK_IMPORTED_MODULE_1__["Text"]({
                  text: name,
                  x: machine.zoneCenter.x * 100 + 10,
                  y: machine.zoneCenter.y * 100 + 45,
                  fontFamily: 'Calibri',
                  scaleY: -1,
                  fontSize: 30,
                  padding: 5,
                  fill: 'black'
                })
              };

              _this8.machineLayer.add(square.circle);

              _this8.machineLayer.add(square.label);
            });
            this.machineLayer.draw();
            this.robotLayer.draw();
            this.update();
            this.stage.on('click', function () {
              return _this8.onLeftClick();
            });
            console.log(this.robot);
          }
        }, {
          key: "onLeftClick",
          value: function onLeftClick() {
            console.log("Left clicked!!");
            console.log("robots: ", this.robot);
            this.update();
          }
        }, {
          key: "update",
          value: function update() {
            var _this9 = this;

            console.log("rob: ", this.robot);
            this.robot.forEach(function (rob) {
              if (rob != null) {
                console.log(rob.robotId);

                _this9.robotsKonvaNodes[rob.robotId - 1].circle.absolutePosition({
                  x: rob.poseX * 100 + 850,
                  y: -1 * rob.poseY * 100 + _this9.robotLayerOffsetY
                });

                _this9.robotsKonvaNodes[rob.robotId - 1].label.absolutePosition({
                  x: rob.poseX * 100 + 850 - 20,
                  y: -1 * rob.poseY * 100 + _this9.robotLayerOffsetY + 25
                });

                console.log(_this9.robotsKonvaNodes[rob.robotId - 1]);
              }
            });
            console.log("redrawing robots", this.robotsKonvaNodes);
            this.robotLayer.draw();
          }
        }, {
          key: "robotToCircle",
          value: function robotToCircle(index, x, y) {
            console.log("creating circle for text");
            var color = "brown";

            if (index == 1) {
              color = "blue";
            } else if (index == 2) {
              color = "green";
            }

            return {
              circle: new konva_lib__WEBPACK_IMPORTED_MODULE_1__["Circle"]({
                x: x * 100,
                y: y * 100,
                radius: 25,
                fill: color,
                stroke: 'black',
                strokeWidth: 4
              }),
              label: new konva_lib__WEBPACK_IMPORTED_MODULE_1__["Text"]({
                text: "" + index,
                x: x * 100 - 20,
                y: y * 100 + 25,
                scaleY: -1,
                fontFamily: 'Calibri',
                fontSize: 50,
                padding: 5,
                fill: 'black'
              })
            };
          }
        }]);

        return MapComponent;
      }();

      MapComponent.ɵfac = function MapComponent_Factory(t) {
        return new (t || MapComponent)();
      };

      MapComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: MapComponent,
        selectors: [["app-map"]],
        inputs: {
          data: "data",
          robot: "robot"
        },
        decls: 4,
        vars: 3,
        consts: [["id", "container", 1, "container"]],
        template: function MapComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "json");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 0);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Map: ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, ctx.robot[0]), "");
          }
        },
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["JsonPipe"]],
        styles: [".row[_ngcontent-%COMP%] {\n  display: flex;\n}\n\n.col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvbWFwL21hcC5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7QUFDRjs7QUFBQTtFQUNFLE9BQUE7QUFHRiIsImZpbGUiOiJzcmMvYXBwL2xvZ2lzdGljcy9jb250ZXN0L21hcC9tYXAuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93XG4gIGRpc3BsYXk6IGZsZXhcbi5jb2xcbiAgZmxleDogMVxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MapComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-map',
            templateUrl: './map.component.html',
            styleUrls: ['./map.component.sass']
          }]
        }], function () {
          return [];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          robot: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "mjzl":
    /*!**************************************************************!*\
      !*** ./src/app/logistics/contest/robots/robots.component.ts ***!
      \**************************************************************/

    /*! exports provided: RobotsComponent */

    /***/
    function mjzl(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RobotsComponent", function () {
        return RobotsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/form-field */
      "kmnG");
      /* harmony import */


      var _angular_material_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/select */
      "d3UM");
      /* harmony import */


      var _angular_material_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/core */
      "FKr1");

      function RobotsComponent_div_1_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var robot_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("ID: ", robot_r5.robotId, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Name: ", robot_r5.robotName, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("OprsStack: ", robot_r5.oprsStack, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Color: ", robot_r5.teamColor, "");
        }
      }

      function RobotsComponent_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RobotsComponent_div_1_div_1_Template, 9, 4, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var robot_r5 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", robot_r5 != null);
        }
      }

      function RobotsComponent_div_6_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-option", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var rob_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", rob_r8.robotId);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](rob_r8.robotId);
        }
      }

      function RobotsComponent_div_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RobotsComponent_div_6_div_1_Template, 3, 2, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var rob_r8 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", rob_r8 != null);
        }
      }

      function RobotsComponent_div_11_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-option", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var t_r11 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", t_r11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](t_r11);
        }
      }

      function RobotsComponent_mat_form_field_12_div_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-option", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "CS1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-option", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "CS2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RobotsComponent_mat_form_field_12_div_5_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-option", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "BS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RobotsComponent_mat_form_field_12_div_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-option", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "DS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RobotsComponent_mat_form_field_12_div_7_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-option", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "DS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RobotsComponent_mat_form_field_12_Template(rf, ctx) {
        if (rf & 1) {
          var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Machine ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-select", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectionChange", function RobotsComponent_mat_form_field_12_Template_mat_select_selectionChange_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r17);

            var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r16.setMachine($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, RobotsComponent_mat_form_field_12_div_4_Template, 5, 0, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, RobotsComponent_mat_form_field_12_div_5_Template, 3, 0, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, RobotsComponent_mat_form_field_12_div_6_Template, 3, 0, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, RobotsComponent_mat_form_field_12_div_7_Template, 3, 0, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.curTask === "GetBaseFromShelf" || ctx_r3.curTask === "GetBaseFromCS" || ctx_r3.curTask === "DeliverBaseToCS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.curTask === "GetBaseFromMachine" || ctx_r3.curTask === "DeliverBaseToMachine");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.curTask === "DeliverBaseToDS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.curTask === "MoveToZone");
        }
      }

      function RobotsComponent_mat_form_field_13_Template(rf, ctx) {
        if (rf & 1) {
          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Zone just testing now... TODO add field where we can click? Made with a for loop.... Just an idea ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-select", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectionChange", function RobotsComponent_mat_form_field_13_Template_mat_select_selectionChange_3_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r18.setZone($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-option", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "M_Z22");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-option", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "C_Z22");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var RobotsComponent = /*#__PURE__*/function () {
        function RobotsComponent(httpClient) {
          _classCallCheck(this, RobotsComponent);

          this.httpClient = httpClient;
          this.allTasks = ['GetBaseFromShelf', 'GetBaseFromMachine', 'GetBaseFromCS', 'DeliverBaseToCS', 'DeliverBaseToDS', 'DeliverBaseToMachine', 'MoveToZone'];
        }

        _createClass(RobotsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.curTask = "Task...";
            console.log("Robots value: ", this.robots);
          }
        }, {
          key: "setTask",
          value: function setTask($event) {
            console.log("Task to teamserver ", $event);
            this.curTask = $event.value;
          }
        }, {
          key: "setMachine",
          value: function setMachine($event) {
            this.machine = $event.value;
          }
        }, {
          key: "setRobotID",
          value: function setRobotID($event) {
            this.robotID = $event.value;
          }
        }, {
          key: "setZone",
          value: function setZone($event) {
            this.zone = $event.value;
          }
        }, {
          key: "sendTask",
          value: function sendTask() {
            //Base checks of the parameters
            if (this.robotID == null) {
              alert("Robot Id must be set!");
              return;
            }

            if (this.curTask == null) {
              alert("Please set a task for the Robot!");
              return;
            } //Checking tasks


            if (this.curTask != "MoveToZone" && this.machine == null) {
              alert("For tasks involving a machine please set a machine!);");
              return;
            }

            if (this.curTask == "MoveToZone" && this.zone == null) {
              alert("For tasks involving a zone please set a zone! (M/C_Zxy);");
              return;
            }

            this.httpClient.post("http://localhost:8090/send_robot_task", {
              task: this.curTask,
              robotID: this.robotID,
              machineForTask: this.machine,
              zone: this.zone,
              robotColor: this.robots[0].teamColor
            }).subscribe(function (res) {
              console.log(res);
            });
            console.log("send task");
          }
        }]);

        return RobotsComponent;
      }();

      RobotsComponent.ɵfac = function RobotsComponent_Factory(t) {
        return new (t || RobotsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
      };

      RobotsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: RobotsComponent,
        selectors: [["app-robots"]],
        inputs: {
          robots: "robots"
        },
        decls: 16,
        vars: 5,
        consts: [[1, "container", "flex-container"], ["class", "flex-element", 4, "ngFor", "ngForOf"], [3, "selectionChange"], [4, "ngFor", "ngForOf"], [4, "ngIf"], [3, "click"], [1, "flex-element"], ["class", "robot", 4, "ngIf"], [1, "robot"], [3, "value"], ["value", "CS1"], ["value", "CS2"], ["value", "BS"], ["value", "DS"], ["value", "M_Z22"], ["value", "C_Z22"]],
        template: function RobotsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RobotsComponent_div_1_Template, 2, 1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-form-field");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Robot");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-select", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectionChange", function RobotsComponent_Template_mat_select_selectionChange_5_listener($event) {
              return ctx.setRobotID($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, RobotsComponent_div_6_Template, 2, 1, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Task...");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-select", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectionChange", function RobotsComponent_Template_mat_select_selectionChange_10_listener($event) {
              return ctx.setTask($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, RobotsComponent_div_11_Template, 3, 2, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, RobotsComponent_mat_form_field_12_Template, 8, 4, "mat-form-field", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, RobotsComponent_mat_form_field_13_Template, 9, 0, "mat-form-field", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RobotsComponent_Template_button_click_14_listener() {
              return ctx.sendTask();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Send Task");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.robots);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.robots);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.allTasks);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.curTask != "MoveToZone");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.curTask === "MoveToZone");
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_3__["MatLabel"], _angular_material_select__WEBPACK_IMPORTED_MODULE_4__["MatSelect"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _angular_material_core__WEBPACK_IMPORTED_MODULE_5__["MatOption"]],
        styles: [".flex-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n}\n\n.flex-element[_ngcontent-%COMP%] {\n  flex-grow: 1;\n}\n\n.flex-shrink[_ngcontent-%COMP%] {\n  flex-shrink: 1;\n}\n\n.robot[_ngcontent-%COMP%] {\n  border: 1px solid gray;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3Qvcm9ib3RzL3JvYm90cy5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUFBO0VBQ0UsWUFBQTtBQUdGOztBQUZBO0VBQ0UsY0FBQTtBQUtGOztBQUpBO0VBQ0Usc0JBQUE7QUFPRiIsImZpbGUiOiJzcmMvYXBwL2xvZ2lzdGljcy9jb250ZXN0L3JvYm90cy9yb2JvdHMuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZmxleC1jb250YWluZXJcbiAgZGlzcGxheTogZmxleFxuICBmbGV4LWRpcmVjdGlvbjogcm93XG4uZmxleC1lbGVtZW50XG4gIGZsZXgtZ3JvdzogMVxuLmZsZXgtc2hyaW5rXG4gIGZsZXgtc2hyaW5rOiAxXG4ucm9ib3RcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JheVxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RobotsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-robots',
            templateUrl: './robots.component.html',
            styleUrls: ['./robots.component.sass']
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
          }];
        }, {
          robots: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "mnBg":
    /*!**********************************************!*\
      !*** ./src/app/services/endpoint.service.ts ***!
      \**********************************************/

    /*! exports provided: EndpointService */

    /***/
    function mnBg(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EndpointService", function () {
        return EndpointService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var EndpointService = /*#__PURE__*/function () {
        function EndpointService(httpClient) {
          _classCallCheck(this, EndpointService);

          this.httpClient = httpClient;
        }

        _createClass(EndpointService, [{
          key: "post",
          value: function post(url, payload) {
            return this.httpClient.post("http://localhost:8090/" + url, payload);
          }
        }]);

        return EndpointService;
      }();

      EndpointService.ɵfac = function EndpointService_Factory(t) {
        return new (t || EndpointService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
      };

      EndpointService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: EndpointService,
        factory: EndpointService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EndpointService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "uwb6":
    /*!****************************************************************************************!*\
      !*** ./src/app/logistics/contest/active-product-task/active-product-task.component.ts ***!
      \****************************************************************************************/

    /*! exports provided: ActiveProductTaskComponent */

    /***/
    function uwb6(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ActiveProductTaskComponent", function () {
        return ActiveProductTaskComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_material_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/material/table */
      "+0xr");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function ActiveProductTaskComponent_th_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Id");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0, a1, a2, a3, a4) {
        return {
          "tbd": a0,
          "inwork": a1,
          "assigned": a2,
          "success": a3,
          "failed": a4
        };
      };

      function ActiveProductTaskComponent_td_5_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var element_r10 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction5"](2, _c0, element_r10.state === "TBD", element_r10.state === "INWORK", element_r10.state === "ASSIGNED", element_r10.state === "SUCCESS", element_r10.state === "FAILED"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element_r10.id, " ");
        }
      }

      function ActiveProductTaskComponent_th_7_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ActiveProductTaskComponent_td_8_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var element_r11 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction5"](2, _c0, element_r11.state === "TBD", element_r11.state === "INWORK", element_r11.state === "ASSIGNED", element_r11.state === "SUCCESS", element_r11.state === "FAILED"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element_r11.name, " ");
        }
      }

      function ActiveProductTaskComponent_th_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Machine");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ActiveProductTaskComponent_td_11_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var element_r12 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction5"](2, _c0, element_r12.state === "TBD", element_r12.state === "INWORK", element_r12.state === "ASSIGNED", element_r12.state === "SUCCESS", element_r12.state === "FAILED"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element_r12.machine, " ");
        }
      }

      function ActiveProductTaskComponent_th_13_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " RobotId");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ActiveProductTaskComponent_td_14_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var element_r13 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction5"](2, _c0, element_r13.state === "TBD", element_r13.state === "INWORK", element_r13.state === "ASSIGNED", element_r13.state === "SUCCESS", element_r13.state === "FAILED"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r13.robotId);
        }
      }

      function ActiveProductTaskComponent_tr_15_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "tr", 12);
        }
      }

      function ActiveProductTaskComponent_tr_16_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "tr", 13);
        }
      }

      var ActiveProductTaskComponent = /*#__PURE__*/function () {
        function ActiveProductTaskComponent() {
          _classCallCheck(this, ActiveProductTaskComponent);

          this.displayedColumns = ['id', 'name', 'machine', 'robotId'];
        }

        _createClass(ActiveProductTaskComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"]();
            this.dataSource.data = this.data.subProductionTasks;
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {}
        }]);

        return ActiveProductTaskComponent;
      }();

      ActiveProductTaskComponent.ɵfac = function ActiveProductTaskComponent_Factory(t) {
        return new (t || ActiveProductTaskComponent)();
      };

      ActiveProductTaskComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ActiveProductTaskComponent,
        selectors: [["app-active-product-task"]],
        inputs: {
          data: "data",
          shrink: "shrink"
        },
        decls: 17,
        vars: 5,
        consts: [["mat-table", "", 1, "mat-elevation-z8", 3, "dataSource"], ["matColumnDef", "id"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 3, "ngClass", 4, "matCellDef"], ["matColumnDef", "name"], ["matColumnDef", "machine"], ["matColumnDef", "robotId"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", 4, "matRowDef", "matRowDefColumns"], ["mat-header-cell", ""], ["mat-cell", "", 3, "ngClass"], [1, "robotId"], ["mat-header-row", ""], ["mat-row", ""]],
        template: function ActiveProductTaskComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "table", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](3, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ActiveProductTaskComponent_th_4_Template, 2, 0, "th", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, ActiveProductTaskComponent_td_5_Template, 2, 8, "td", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](6, 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ActiveProductTaskComponent_th_7_Template, 2, 0, "th", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, ActiveProductTaskComponent_td_8_Template, 2, 8, "td", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](9, 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, ActiveProductTaskComponent_th_10_Template, 2, 0, "th", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, ActiveProductTaskComponent_td_11_Template, 2, 8, "td", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](12, 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ActiveProductTaskComponent_th_13_Template, 2, 0, "th", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, ActiveProductTaskComponent_td_14_Template, 3, 8, "td", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, ActiveProductTaskComponent_tr_15_Template, 1, 0, "tr", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, ActiveProductTaskComponent_tr_16_Template, 1, 0, "tr", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("Id: ", ctx.data.productOrder.id, " Complexity: ", ctx.data.productOrder.complexity, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dataSource", ctx.dataSource);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matHeaderRowDef", ctx.displayedColumns);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("matRowDefColumns", ctx.displayedColumns);
          }
        },
        directives: [_angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatTable"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatColumnDef"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatHeaderCellDef"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatCellDef"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatHeaderRowDef"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatRowDef"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatHeaderCell"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatCell"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatHeaderRow"], _angular_material_table__WEBPACK_IMPORTED_MODULE_1__["MatRow"]],
        styles: ["table[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.tbd[_ngcontent-%COMP%] {\n  background: gray;\n}\n\n.inwork[_ngcontent-%COMP%] {\n  background: greenyellow;\n}\n\n.assigned[_ngcontent-%COMP%] {\n  background: yellow;\n}\n\n.success[_ngcontent-%COMP%] {\n  background: green;\n}\n\n.failed[_ngcontent-%COMP%] {\n  background: red;\n}\n\n.robotId[_ngcontent-%COMP%] {\n  text-align: right;\n}\n\n.mat-header-row[_ngcontent-%COMP%] {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3QvYWN0aXZlLXByb2R1Y3QtdGFzay9hY3RpdmUtcHJvZHVjdC10YXNrLmNvbXBvbmVudC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtBQUNGOztBQUNBO0VBQ0UsZ0JBQUE7QUFFRjs7QUFBQTtFQUNFLHVCQUFBO0FBR0Y7O0FBREE7RUFDRSxrQkFBQTtBQUlGOztBQUZBO0VBQ0UsaUJBQUE7QUFLRjs7QUFIQTtFQUNFLGVBQUE7QUFNRjs7QUFKQTtFQUNFLGlCQUFBO0FBT0Y7O0FBTEE7RUFDRSxhQUFBO0FBUUYiLCJmaWxlIjoic3JjL2FwcC9sb2dpc3RpY3MvY29udGVzdC9hY3RpdmUtcHJvZHVjdC10YXNrL2FjdGl2ZS1wcm9kdWN0LXRhc2suY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZVxuICB3aWR0aDogMTAwJVxuXG4udGJkXG4gIGJhY2tncm91bmQ6IGdyYXlcblxuLmlud29ya1xuICBiYWNrZ3JvdW5kOiBncmVlbnllbGxvd1xuXG4uYXNzaWduZWRcbiAgYmFja2dyb3VuZDogeWVsbG93XG5cbi5zdWNjZXNzXG4gIGJhY2tncm91bmQ6IGdyZWVuXG5cbi5mYWlsZWRcbiAgYmFja2dyb3VuZDogcmVkXG5cbi5yb2JvdElkXG4gIHRleHQtYWxpZ246IHJpZ2h0XG5cbi5tYXQtaGVhZGVyLXJvd1xuICBkaXNwbGF5OiBub25lXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ActiveProductTaskComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-active-product-task',
            templateUrl: './active-product-task.component.html',
            styleUrls: ['./active-product-task.component.sass']
          }]
        }], function () {
          return [];
        }, {
          data: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          shrink: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "vY5A":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function vY5A(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _logistics_contest_contest_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./logistics/contest/contest.component */
      "f5/C");
      /* harmony import */


      var _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./statistics/statistics.component */
      "4QAB");
      /* harmony import */


      var _robot_controller_robot_controller_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./robot-controller/robot-controller.component */
      "7BCm");

      var routes = [{
        path: '',
        redirectTo: 'contest',
        pathMatch: 'full'
      }, {
        path: 'contest',
        component: _logistics_contest_contest_component__WEBPACK_IMPORTED_MODULE_2__["ContestComponent"]
      }, {
        path: 'statistics',
        component: _statistics_statistics_component__WEBPACK_IMPORTED_MODULE_3__["StatisticsComponent"]
      }, {
        path: 'robot-controller',
        component: _robot_controller_robot_controller_component__WEBPACK_IMPORTED_MODULE_4__["RobotControllerComponent"]
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AppRoutingModule
      });
      AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AppRoutingModule_Factory(t) {
          return new (t || AppRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "zKIh":
    /*!**************************************************************!*\
      !*** ./src/app/logistics/contest/orders/orders.component.ts ***!
      \**************************************************************/

    /*! exports provided: OrdersComponent */

    /***/
    function zKIh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrdersComponent", function () {
        return OrdersComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _pipes_minute_seconds_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../pipes/minute-seconds.pipe */
      "exDf");

      function OrdersComponent_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ring_r2 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" ", ring_r2.ringColor, ": ", ring_r2.rawMaterial, "\n");
        }
      }

      var _c0 = function _c0(a0) {
        return {
          "competitive": a0
        };
      };

      var _c1 = function _c1(a0, a1) {
        return {
          "black": a0,
          "grey": a1
        };
      };

      var _c2 = function _c2(a0, a1, a2, a3) {
        return {
          "yellow": a0,
          "blue": a1,
          "orange": a2,
          "green": a3
        };
      };

      var _c3 = function _c3(a0, a1, a2) {
        return {
          "silver": a0,
          "black": a1,
          "red": a2
        };
      };

      function OrdersComponent_div_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "minuteSeconds");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Deliver Period:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "minuteSeconds");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](13, "minuteSeconds");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var order_r3 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("Order ", order_r3.id, " - ", order_r3.complexity, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("CreationTime: ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](5, 15, order_r3.creationGameTime / 1000000000), "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](21, _c0, order_r3.competitive));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" Competitive: ", order_r3.competitive, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](12, 17, order_r3.deliveryPeriodBegin), " - ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](13, 19, order_r3.deliveryPeriodEnd), "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](23, _c1, order_r3.capColor == "CAP_BLACK", order_r3.capColor == "CAP_GREY"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction4"](26, _c2, order_r3.ring3 == "RING_YELLOW", order_r3.ring3 == "RING_BLUE", order_r3.ring3 == "RING_ORANGE", order_r3.ring3 == "RING_GREEN"))("hidden", order_r3.ring3 == null);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction4"](31, _c2, order_r3.ring2 == "RING_YELLOW", order_r3.ring2 == "RING_BLUE", order_r3.ring2 == "RING_ORANGE", order_r3.ring2 == "RING_GREEN"))("hidden", order_r3.ring2 == null);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction4"](36, _c2, order_r3.ring1 == "RING_YELLOW", order_r3.ring1 == "RING_BLUE", order_r3.ring1 == "RING_ORANGE", order_r3.ring1 == "RING_GREEN"))("hidden", order_r3.ring1 == null);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](41, _c3, order_r3.baseColor == "BASE_SILVER", order_r3.baseColor == "BASE_BLACK", order_r3.baseColor == "BASE_RED"));
        }
      }

      var OrdersComponent = /*#__PURE__*/function () {
        function OrdersComponent() {
          _classCallCheck(this, OrdersComponent);
        }

        _createClass(OrdersComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            console.log(this.orders);
          }
        }]);

        return OrdersComponent;
      }();

      OrdersComponent.ɵfac = function OrdersComponent_Factory(t) {
        return new (t || OrdersComponent)();
      };

      OrdersComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: OrdersComponent,
        selectors: [["app-orders"]],
        inputs: {
          orders: "orders",
          rings: "rings"
        },
        decls: 5,
        vars: 2,
        consts: [[4, "ngFor", "ngForOf"], [1, "container", "flex-container"], ["class", "flex-element", 4, "ngFor", "ngForOf"], [1, "flex-element"], [3, "ngClass"], [1, "cap", 3, "ngClass"], [1, "ring", 3, "ngClass", "hidden"], [1, "base", 3, "ngClass"]],
        template: function OrdersComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Orders:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, OrdersComponent_div_2_Template, 2, 2, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, OrdersComponent_div_4_Template, 19, 45, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.rings);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.orders);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"]],
        pipes: [_pipes_minute_seconds_pipe__WEBPACK_IMPORTED_MODULE_2__["MinuteSecondsPipe"]],
        styles: [".cap[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 20px;\n}\n\n.ring[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 30px;\n}\n\n.base[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 80px;\n}\n\n.black[_ngcontent-%COMP%] {\n  background-color: black;\n}\n\n.grey[_ngcontent-%COMP%] {\n  background-color: grey;\n}\n\n.yellow[_ngcontent-%COMP%] {\n  background-color: yellow;\n}\n\n.green[_ngcontent-%COMP%] {\n  background-color: green;\n}\n\n.blue[_ngcontent-%COMP%] {\n  background-color: blue;\n}\n\n.orange[_ngcontent-%COMP%] {\n  background-color: orange;\n}\n\n.red[_ngcontent-%COMP%] {\n  background-color: red;\n}\n\n.silver[_ngcontent-%COMP%] {\n  background-color: silver;\n}\n\n.competitive[_ngcontent-%COMP%] {\n  color: red;\n}\n\n.flex-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n}\n\n.flex-element[_ngcontent-%COMP%] {\n  flex-grow: 1;\n}\n\n.flex-shrink[_ngcontent-%COMP%] {\n  flex-shrink: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3Qvb3JkZXJzL29yZGVycy5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0FBQ0Y7O0FBQUE7RUFDRSxZQUFBO0VBQ0EsWUFBQTtBQUdGOztBQUZBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7QUFLRjs7QUFKQTtFQUNFLHVCQUFBO0FBT0Y7O0FBTkE7RUFDRSxzQkFBQTtBQVNGOztBQVJBO0VBQ0Usd0JBQUE7QUFXRjs7QUFWQTtFQUNFLHVCQUFBO0FBYUY7O0FBWkE7RUFDRSxzQkFBQTtBQWVGOztBQWRBO0VBQ0Usd0JBQUE7QUFpQkY7O0FBaEJBO0VBQ0UscUJBQUE7QUFtQkY7O0FBbEJBO0VBQ0Usd0JBQUE7QUFxQkY7O0FBcEJBO0VBQ0UsVUFBQTtBQXVCRjs7QUFyQkE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUF3QkY7O0FBdkJBO0VBQ0UsWUFBQTtBQTBCRjs7QUF6QkE7RUFDRSxjQUFBO0FBNEJGIiwiZmlsZSI6InNyYy9hcHAvbG9naXN0aWNzL2NvbnRlc3Qvb3JkZXJzL29yZGVycy5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXBcbiAgd2lkdGg6IDEwMHB4XG4gIGhlaWdodDogMjBweFxuLnJpbmdcbiAgd2lkdGg6IDEwMHB4XG4gIGhlaWdodDogMzBweFxuLmJhc2VcbiAgd2lkdGg6IDEwMHB4XG4gIGhlaWdodDogODBweFxuLmJsYWNrXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrXG4uZ3JleVxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5XG4ueWVsbG93XG4gIGJhY2tncm91bmQtY29sb3I6IHllbGxvd1xuLmdyZWVuXG4gIGJhY2tncm91bmQtY29sb3I6IGdyZWVuXG4uYmx1ZVxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlXG4ub3JhbmdlXG4gIGJhY2tncm91bmQtY29sb3I6IG9yYW5nZVxuLnJlZFxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWRcbi5zaWx2ZXJcbiAgYmFja2dyb3VuZC1jb2xvcjogc2lsdmVyXG4uY29tcGV0aXRpdmVcbiAgY29sb3I6IHJlZFxuXG4uZmxleC1jb250YWluZXJcbiAgZGlzcGxheTogZmxleFxuICBmbGV4LWRpcmVjdGlvbjogcm93XG4uZmxleC1lbGVtZW50XG4gIGZsZXgtZ3JvdzogMVxuLmZsZXgtc2hyaW5rXG4gIGZsZXgtc2hyaW5rOiAxXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](OrdersComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-orders',
            templateUrl: './orders.component.html',
            styleUrls: ['./orders.component.sass']
          }]
        }], function () {
          return [];
        }, {
          orders: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          rings: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /***/

    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.error(err);
      });
      /***/

    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map