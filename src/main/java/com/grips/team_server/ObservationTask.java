/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.team_server;
import com.grips.config.Config;
import com.shared.domain.MachineName;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.persistence.domain.MachineReport;
import com.grips.persistence.domain.RobotObservation;
import com.grips.protobuf_lib.RobotConnections;
import com.grips.protobuf_lib.RobotMessageRegister;
import com.google.common.collect.Lists;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.model.teamserver.*;
import com.robot_communication.services.RobotClient;
import com.grips.scheduler.GameField;
import com.robot_communication.services.PrsTaskCreator;
import com.shared.domain.GamePhase;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_comm.ProtobufMessage;
import org.robocup_logistics.llsf_msgs.*;
import org.robocup_logistics.llsf_utils.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import com.grips.refbox.RefBoxConnectionManager;

@Service
@CommonsLog
public class ObservationTask {
    @Autowired
    private GameStateDao gameStateDao;
    @Autowired
    private MachineReportDao machineReportDao;
    @Autowired
    private RobotConnections robotConnections;
    @Autowired
    private RefBoxConnectionManager rbcm;
    @Autowired
    private MachineInfoRefBoxDao machineInfoRefBoxDao;
    @Autowired
    private RobotObservationDao robotObservationDao;
    @Autowired
    private GameField _gameField;
    @Autowired
    private SubProductionTaskDao subProductionTaskDao;
    @Autowired
    private PrsTaskCreator prsTaskCreator;

    @Autowired
    private RobotClient robotClient;

    private GamePhase oldPhase = null;
    private com.shared.domain.GameState oldState = null;

    private int numExplorationDistributions = 0;

    @Value("${exploration.time_offset.robot1}") private int OFFSET_EXPLORATION_ROBOT1;
    @Value("${exploration.time_offset.robot2}") private int OFFSET_EXPLORATION_ROBOT2;
    @Value("${exploration.time_offset.robot3}") private int OFFSET_EXPLORATION_ROBOT3;

    @Scheduled(cron = "${task.observation}")
    @Transactional
    public void run() {

        GamePhase gamePhase = gameStateDao.getGamePhase();
        com.shared.domain.GameState gameState = gameStateDao.getGameState();

        if (oldPhase != gamePhase) {
            log.info("Switching to game phase: " + gamePhase);

            if(oldPhase == GamePhase.EXPLORATION && gamePhase == GamePhase.PRODUCTION) {
                cancelAllExplorationTasks();
            }

            oldPhase = gamePhase;
        }
        if (oldState != gameState || oldState == null) {
            if (oldState != null) {
                log.info("Switching to game state: " + gameState);
            }
            oldState = gameState;
            if(gameState != null && gameState != com.shared.domain.GameState.RUNNING) {
                robotClient.stopAllRobots();
            }
            if(gameState == com.shared.domain.GameState.RUNNING) {
                robotClient.startAllRobots();
            }
        }

        if (null == gamePhase) {
            log.error("gamePhase is null");
            return;
        }
        if (null == gameState) {
            log.error("gameState is null");
            return;
        }

        // TODO: handle invalid gamephase changes correct, e.g. production -> exploration!
        switch (gamePhase) {
            case PRE_GAME:
                handlePreGamePhase();
                return;
            case SETUP:
                handleSetupPhase();
                return;
            case EXPLORATION:
                handleExplorationPhase();
                return;
            case PRODUCTION:
                handleProductionPhase();
                return;
            case POST_GAME:
                handlePostGamePhase();
                return;
            default:
                log.error("Invalid Game state in observation task detected!");
        }
    }

    private void cancelAllExplorationTasks() {
        log.info("Cancelling all active exploration tasks!");
        synchronized (robotConnections.getRobots()) {
            for (Peer robot : robotConnections.getRobots()) {
                List<SubProductionTask> inwork = subProductionTaskDao.findByRobotIdAndState((int) robot.getId(), SubProductionTask.TaskState.INWORK);
                if (inwork != null && inwork.size() > 0) {
                    // Robot is doing a production task already in exploration, do not cancel this task!
                    continue;
                } else {
                    robotClient.cancelTask((int) robot.getId());
                }
            }
        }
    }

    // --------------------   Game lifecycle:   ------------------------------------------------------------------------

    private void handlePreGamePhase() {
    }

    private void handleSetupPhase() {
    }

    private void handleExplorationPhase() {
        //advancedExplorationScheduler.updateGameField();


        if(gameStateDao.getGameState() != com.shared.domain.GameState.RUNNING) {
            // If not running -> only visualize, no actions, no reports...
            return;
        }

        //distributeExplorationTask();

        BigInteger gameTime = gameStateDao.getLatestGameTimeExplorationPhase();
        // -1, 0, +1 <-> lower, equal, higher
        if (gameTime.compareTo(Config.NUM_NSECS_EXPLORATION_START_REPORTING) == -1) {
            return;
        }

        sendExplorationDataToRefBox();

        /*
        if (gameTime.compareTo(Config.NUM_NSECS_REPORT_CAM) == -1) {
            sendCamObservationsToRefBox();
        }
        */

        //sendLightInfoToRefbox();
        //sendPosInfoToRefbox();

        // if we are at gametime 2:57 in simulation, send robotobservations that seem to be correct
        //TODO: gametime
        if (gameTime.compareTo(Config.NUM_NSEC_REPORT_AR_ONLY) == 1) {
            log.info("sending AR only observations");
            sendArOnlyExplorationResults();
        }
    }

    private void sendArOnlyExplorationResults() {
        ArrayList<RobotObservation> allObservations = Lists.newArrayList(robotObservationDao.findAll());

        //we only consider robotobservations for machines for which we do not have machinereports
        final List<MachineName> reportedMachines = Lists.newArrayList(machineReportDao.findAll()).stream()
                .map(MachineReport::getName)
                .collect(Collectors.toList());

        List<RobotObservation> remove = allObservations.stream()
                .filter(o -> reportedMachines.contains(o.getMachineName()))
                .collect(Collectors.toList());

        allObservations.removeAll(remove);

        List<MachineReportProtos.MachineReportEntry> machineReports = new ArrayList<>();

        // now we see if there are "common" observations
        Map<String, Long> commonObservations = allObservations.stream()
                .collect(Collectors.groupingBy(RobotObservation::getMachineZone, Collectors.counting()));
        for (String zoneToLookat: commonObservations.keySet()) {
            if (commonObservations.get(zoneToLookat) == 3) {
                List<MachineName> observedMachines = allObservations.stream()
                        .filter(o -> o.getMachineZone().equalsIgnoreCase(zoneToLookat))
                        .map(RobotObservation::getMachineName)
                        .distinct()
                        .collect(Collectors.toList());
                List<Integer> observedOrientations = allObservations.stream()
                        .filter(o -> o.getMachineZone().equalsIgnoreCase(zoneToLookat)).map(RobotObservation::getOrientation)
                        .distinct()
                        .collect(Collectors.toList());

                if (observedMachines.size() == 1) {
                    machineReports.add(buildMachineReportEntry(observedMachines, zoneToLookat, observedOrientations));
                }

            } else if (commonObservations.get(zoneToLookat) == 2) {
                List<MachineName> observedMachines = allObservations.stream()
                        .filter(o -> o.getMachineZone().equalsIgnoreCase(zoneToLookat))
                        .map(RobotObservation::getMachineName)
                        .distinct()
                        .collect(Collectors.toList());
                List<Integer> observedOrientations = allObservations.stream().filter(o -> o.getMachineZone().equalsIgnoreCase(zoneToLookat)).map(o -> o.getOrientation()).distinct().collect(Collectors.toList());

                MachineName machine = observedMachines.get(0);
                boolean onlyObservations = allObservations.stream()
                        .noneMatch(o -> o.getMachineName().equals(machine) && !o.getMachineZone().equalsIgnoreCase(zoneToLookat));
                if (onlyObservations && observedMachines.size() == 1) {
                    machineReports.add(buildMachineReportEntry(observedMachines, zoneToLookat, observedOrientations));
                }
            }
        }

        TeamProtos.Team teamColor = TeamProtos.Team.valueOf(gameStateDao.getTeamColor());
        TeamColor mTeamColor = TeamColor.fromString(gameStateDao.getTeamColor());
        if (machineReports.size() > 0) {
            MachineReportProtos.MachineReport msg = MachineReportProtos.MachineReport.newBuilder()
                    .addAllMachines(machineReports)
                    .setTeamColor(teamColor).build();

            Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(MachineReportProtos.MachineReport.class);

            rbcm.sendPrivateMsg(new ProtobufMessage(key.cmp_id, key.msg_id, msg));
        }
    }

    private MachineReportProtos.MachineReportEntry buildMachineReportEntry(List<MachineName> observedMachines, String machineZone, List<Integer> observedOrientations) {
        MachineReportProtos.MachineReportEntry entry;
        Integer orientation = null;
        String machineName = null;

        if (observedMachines.size() == 1 && observedOrientations.size() == 1) {
            // all 3 robots observed the same machine and orientation, so we report it
            orientation = observedOrientations.get(0);
            machineName = observedMachines.get(0).getRawMachineName();
        } else if (observedMachines.size() == 1 && observedOrientations.size() != 1) {
            // report without orientation
            machineName = observedMachines.get(0).getRawMachineName();
        }

        if (orientation != null) {
            entry = MachineReportProtos.MachineReportEntry.newBuilder()
                    .setName(machineName)
                    .setZone(ZoneProtos.Zone.valueOf(machineZone))
                    //.setRotation(orientation)
                    .build();
        } else {
            entry = MachineReportProtos.MachineReportEntry.newBuilder()
                    .setName(machineName)
                    .setZone(ZoneProtos.Zone.valueOf(machineZone))
                    .build();
        }

        return entry;
    }

    private void sendCamObservationsToRefBox() {
        TeamProtos.Team teamColor = TeamProtos.Team.valueOf(gameStateDao.getTeamColor());
        TeamColor mTeamColor = TeamColor.valueOf(gameStateDao.getTeamColor());

        List<MachineInfoRefBox> machines = machineInfoRefBoxDao.findByTeamColor(mTeamColor);

        List<MachineReportProtos.MachineReportEntry> machineReports = new ArrayList<>();

        for (MachineInfoRefBox mInfo : machines) {
            //find most probable zone for each machine
            //FUFUFU

            List<RobotObservation> observations = robotObservationDao.findByMachineNameLike(mInfo.getName());
            //TODO: find most probable zone and second most zone and compre
            // if most probable zone >= second zone * 2, we report this zone and orientation

            Map<String, Integer> zoneCounts = new LinkedHashMap<>();
            for (RobotObservation o : observations) {
                String zone = o.getMachineZone();
                if (zoneCounts.containsKey(zone)) {
                    zoneCounts.put(zone, zoneCounts.get(zone) + 1);
                } else {
                    zoneCounts.put(zone, 1);
                }
            }

            String mostZone = null;
            int countMost, countSecond;
            LinkedHashMap<String, Integer> sortedBySeen = zoneCounts.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
            if (sortedBySeen.size() == 1) {
                // only reported for one zone
                mostZone = sortedBySeen.keySet().iterator().next();
            } else if (sortedBySeen.size() > 1) {
                Iterator<Map.Entry<String, Integer>> iterator = sortedBySeen.entrySet().iterator();
                Map.Entry<String, Integer> most = iterator.next();
                Map.Entry<String, Integer> second = iterator.next();

                countMost = most.getValue();
                countSecond = second.getValue();

                if (countMost >= countSecond*2) {
                    mostZone = most.getKey();
                }
            }

            if (mostZone != null) {
                // report machine in zone, orientation in this zone still needs to be determined
                final String finalZone = mostZone;
                List<RobotObservation> oInMostZone = observations.stream().filter(o -> o.getMachineZone().equalsIgnoreCase(finalZone)).collect(Collectors.toList());
                Map<Integer, Integer> orientationCounts = new HashMap<>();
                for (RobotObservation o : oInMostZone) {
                    if (orientationCounts.containsKey(o.getOrientation())) {
                        orientationCounts.put(o.getOrientation(), orientationCounts.get(o.getOrientation()) + 1);
                    } else {
                        orientationCounts.put(o.getOrientation(), 1);
                    }
                }

                int mostOrientation = -1;
                int mostOc, secondOs;
                LinkedHashMap<Integer, Integer> sortedByCount = orientationCounts.entrySet().stream().sorted(Map.Entry.<Integer, Integer>comparingByValue().reversed()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
                if (sortedByCount.size() == 1) {
                    // only reported for one zone
                    mostOrientation = sortedByCount.keySet().iterator().next();
                } else if (sortedByCount.size() > 1) {
                    Iterator<Map.Entry<Integer, Integer>> iterator = sortedByCount.entrySet().iterator();
                    Map.Entry<Integer, Integer> most = iterator.next();
                    Map.Entry<Integer, Integer> second = iterator.next();

                    mostOc = most.getValue();
                    secondOs = second.getValue();

                    if (mostOc >= secondOs*2) {
                        mostOrientation = most.getKey();
                    }
                }

                // if mostOrientation == -1 only send zone, otherwise zone+orientation

                if (mostOrientation != -1) {
                    MachineReportProtos.MachineReportEntry entry = MachineReportProtos.MachineReportEntry.newBuilder()
                            .setName(mInfo.getName())
                            .setZone(ZoneProtos.Zone.valueOf(mostZone))
                            .setRotation(mostOrientation).build();

                    machineReports.add(entry);
                } else {
                    MachineReportProtos.MachineReportEntry entry = MachineReportProtos.MachineReportEntry.newBuilder()
                            .setName(mInfo.getName())
                            .setZone(ZoneProtos.Zone.valueOf(mostZone)).build();

                    machineReports.add(entry);
                }
            }
        }

        if (machineReports.size() > 0) {
            MachineReportProtos.MachineReport msg = MachineReportProtos.MachineReport.newBuilder()
                    .addAllMachines(machineReports)
                    .setTeamColor(teamColor).build();

            Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(MachineReportProtos.MachineReport.class);

            rbcm.sendPrivateMsg(new ProtobufMessage(key.cmp_id, key.msg_id, msg));
        }

        log.info("report count: " + machineReports.size());
        for (MachineReportProtos.MachineReportEntry me : machineReports) {
            log.info("reporting " + me.getName() + " with orientation: " + me.getRotation());
        }
    }

    private void handleProductionPhase() {
        //for (Integer activeRobot : rcm.getRobotIds()) {
        //    SubProductionTask task = productionScheduler.scheduleTask(activeRobot);
        //    rcm.sendProductionTaskToRobot(task);
        //}
    }

    private void handlePostGamePhase() {
    }


    // ------------------------ Helper methods: ------------------------------------------------------------------------


    private void sendExplorationDataToRefBox() {

        //List<Object[]> machineReports = machineReportDao.getMachineReports();
        List<MachineReport> machineReports = Lists.newArrayList(machineReportDao.findAll());

        TeamProtos.Team teamColor = TeamProtos.Team.valueOf(gameStateDao.getTeamColor());

        List<MachineReportProtos.MachineReportEntry> entries = new ArrayList<>();
        for(MachineReport machineReport : machineReports) {
            MachineName machine = machineReport.getName();
            int rotation = machineReport.getRotation();
            ZoneProtos.Zone zone =  machineReport.getZone();

            // TODO: test if swapping is correct. (export to method)
            String firstLetterZone = String.valueOf(zone.toString().charAt(0));

            int xPos = zone.getNumber()/10;
            int yPos = zone.getNumber()%10;

            switch (teamColor) {
                case CYAN:
                    if(machine.isMagenta()) {
                        machine = machine.mirror();
                        if(firstLetterZone.compareToIgnoreCase("C") == 0) {
                            zone = ZoneProtos.Zone.valueOf("M" + zone.toString().substring(1));
                        }
                        else {
                            zone = ZoneProtos.Zone.valueOf("C" + zone.toString().substring(1));
                        }
                        rotation = _gameField.rotateMachineIfNecessary(xPos, yPos, machine, rotation);
                    }
                    break;
                case MAGENTA:
                    if(machine.isCyan()) {
                        machine = machine.mirror();
                        if(firstLetterZone.compareToIgnoreCase("C") == 0) {
                            zone = ZoneProtos.Zone.valueOf("M" + zone.toString().substring(1));
                        }
                        else {
                            zone = ZoneProtos.Zone.valueOf("C" + zone.toString().substring(1));
                        }
                        rotation = _gameField.rotateMachineIfNecessary(xPos, yPos, machine, rotation);
                    }
                    break;
                default:
                    log.error("Invalid Team found!");
                    continue;
            }

            // Swapping end...


            MachineReportProtos.MachineReportEntry entry = MachineReportProtos.MachineReportEntry.newBuilder()
                    .setName(machine.getRawMachineName())
                    .setZone(zone)
                    .setRotation(rotation).build();

            if (entry.getZone() == null) {
                // do not report a machine without a zone
                continue;
            }

            entries.add(entry);
        }

        StringBuilder logMsg = new StringBuilder();
        logMsg.append("[REPORT]: ");
        entries.forEach(x -> logMsg.append(x.getName()).append(" zone: ").append(x.getZone()).append(" rotation: ").append(x.getRotation()));
        log.info(logMsg.toString());

        MachineReportProtos.MachineReport msg = MachineReportProtos.MachineReport.newBuilder()
                .addAllMachines(entries)
                .setTeamColor(teamColor).build();

        Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(MachineReportProtos.MachineReport.class);

        rbcm.sendPrivateMsg(new ProtobufMessage(key.cmp_id, key.msg_id, msg));
    }

}
