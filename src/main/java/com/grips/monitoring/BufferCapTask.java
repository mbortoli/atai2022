package com.grips.monitoring;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BufferCapTask {
    private long robotId;
    private String machine;
    private int shelf;
}
