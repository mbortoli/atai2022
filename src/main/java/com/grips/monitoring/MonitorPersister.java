package com.grips.monitoring;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.grips.persistence.dao.GameStateDao;
import com.shared.domain.GamePhase;
import com.visualization.TeamServerVisualization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class MonitorPersister {

    @Autowired
    private VisualizationController visualizationController;

    @Autowired
    private GameStateDao gameStateDao;

    List<TeamServerVisualization> exploration;
    List<TeamServerVisualization> production;

    ObjectMapper objectMapper;

    LocalDateTime localDateTime;
    DateTimeFormatter formatter;

    public MonitorPersister() {
        this.exploration = new ArrayList<>();
        this.production = new ArrayList<>();
        this.objectMapper = new ObjectMapper();
        this.localDateTime = LocalDateTime.now();
        formatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
    }

    @Scheduled(fixedDelay = 30000)
    public void writeToFile() {
        GamePhase gamePhase = gameStateDao.getGamePhase();
        if (gamePhase != GamePhase.EXPLORATION && gamePhase != GamePhase.PRODUCTION) {
            return;
        }
        ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
        String fileName = "pro_" + localDateTime.format(formatter) + ".json";
        List<TeamServerVisualization> toAdd = this.production;
        if (GamePhase.EXPLORATION.equals(gamePhase)) {
            fileName = "exp_" + localDateTime.format(formatter) + ".json";
            toAdd = this.exploration;
        }
        toAdd.add(visualizationController.getVisualizationData());
        if (toAdd.size() > 1) {
            toAdd.get(toAdd.size() - 1).setExplorationZones(null);
        }
        try {
            String directoryName = "/recordings";
            File directory = new File(directoryName);
            if (! directory.exists()){
                directory.mkdir();
            }
            writer.writeValue(new File(directoryName + "/" + fileName), toAdd);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
