package com.grips.monitoring;

import com.grips.persistence.domain.ExplorationZone;
import com.robot_communication.services.RobotClient;
import com.robot_communication.services.PrsTaskCreator;
import com.shared.domain.MachineName;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.Data;

import java.util.Random;

@RestController
@RequestMapping("/robot-controller")
@Data
@CommonsLog
public class RobotController {

    @Autowired
    private RobotClient robotClient;

    @Autowired
    private PrsTaskCreator prsTaskCreator;

    @CrossOrigin
    @PostMapping("move-to")
    public void moveToTask(@RequestBody MoveToTask moveToTask) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks task = new PrsTaskCreator().createMoveToWaypointTask(moveToTask.getRobotId(), new Random().nextInt(), moveToTask.getZone());
        robotClient.sendPrsTaskToRobot(task);
    }

    @CrossOrigin
    @PostMapping("buffer-cap")
    public void moveToTask(@RequestBody BufferCapTask bufferCapTask) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks task = new PrsTaskCreator().createBufferCapTask(bufferCapTask.getRobotId(),
                new Random().nextInt(), new MachineName(bufferCapTask.getMachine()), bufferCapTask.getShelf());
        robotClient.sendPrsTaskToRobot(task);
    }

    @CrossOrigin
    @PostMapping("get-product")
    public void getProduct(@RequestBody GetProductTask getTask) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks task = new PrsTaskCreator().createGetWorkPieceTaskNew(getTask.getRobotId(),
                1L, getTask.getMachine(), getTask.getMachinePoint());
        robotClient.sendPrsTaskToRobot(task);
    }

    @CrossOrigin
    @PostMapping("deliver-product")
    public void deliverProduct(@RequestBody DeliverProductTask deliverTask) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks task = new PrsTaskCreator().createDeliverWorkPieceTaskNew(deliverTask.getRobotId(), 1L,
                deliverTask.getMachine(), deliverTask.getMachinePoint());
        robotClient.sendPrsTaskToRobot(task);
    }

    @CrossOrigin
    @PostMapping("explore-machine")
    public void exploreMachine(@RequestBody ExploreMachineTask exploreMachineTask) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks task = new PrsTaskCreator().createExplorationTask(
                exploreMachineTask.getRobotId(), exploreMachineTask.getMachine(), exploreMachineTask.getZone(),
                exploreMachineTask.getSide());
        robotClient.sendPrsTaskToRobot(task);
    }


    @CrossOrigin
    @PostMapping("send_robot_task")
    public ResponseEntity acceptRobotTask(@RequestBody RobotTask curRobotTask) {
        System.out.println("Received robot task: " + curRobotTask.task);
        System.out.println("Received robot team color: " + curRobotTask.robotColor);
        System.out.println("Received robot ID: " + curRobotTask.robotID);
        System.out.println("Received machine ID: " + curRobotTask.machineForTask);
        System.out.println("Received zone: " + curRobotTask.zone);
        System.out.println(curRobotTask.task.contentEquals("MoveToZone"));

        boolean succses = false;
        if (!curRobotTask.task.contentEquals("MoveToZone")) {
            succses = acceptRobotTaskProduction(curRobotTask);
        } else {
            acceptRobotTaskExploration(curRobotTask);
        }
        if (succses == true)
            return ResponseEntity.ok(HttpStatus.OK);
        else
            return null;
    }

    public boolean acceptRobotTaskProduction(RobotTask curRobotTask) {


        String color = "M";
        if (curRobotTask.robotColor.equals("CYAN")) {
            color = "C";
        }
        String machine = color + "-" + curRobotTask.machineForTask;
        Long robotId = Long.parseLong(curRobotTask.robotID);

        if (curRobotTask.task.contains("Get")) {
            robotClient.sendGetTaskToRobot(robotId, 1L, machine, resolveMachineSide(curRobotTask.task), 1);
        } else if (curRobotTask.task.contains("Deliver")) {
            robotClient.sendDeliverTaskToRobot(robotId, 1L, machine, resolveMachineSide(curRobotTask.task), 1);
        } else {
            System.out.println("No specified task type");
            return false;
        }
        return true;
    }

    /**
     * Either robot goes on a quest to find the machine that should be near the zone given or when no machine is given
     * he goes to the zone and waits for a new task.
     */
    public boolean acceptRobotTaskExploration(RobotTask curRobotTask) {

        //Just for extra measure...
        if (!curRobotTask.task.equals("MoveToZone")) {
            System.out.println("This should not happen... Received wrong task!");
            return false;
        }

        //check zone format
        char teamColor = curRobotTask.zone.charAt(0);
        String filler = curRobotTask.zone.substring(1, 3);
        System.out.println(curRobotTask.zone.substring(3, 3));
        System.out.println(curRobotTask.zone.substring(3, 4));
        System.out.println(curRobotTask.zone.substring(4, 5));
        int x = Integer.parseInt(curRobotTask.zone.substring(3, 4));
        int y = Integer.parseInt(curRobotTask.zone.substring(4, 5));

        if (teamColor != 'M' && teamColor != 'C') {
            System.out.println("Teamcolor for the field position not set correctly");
            return false;
        }
        if (!filler.equals("_Z")) {
            System.out.println("We need the zone position in the format Color + \"_Z\" + x + y. Missing _Z! Got:" + filler);
            return false;
        }
        if (x > ExplorationZone.NUM_ZONES_WIDTH / 2) {
            System.out.println("Zone width out of bounds! Got:" + x + " should be:" + ExplorationZone.NUM_ZONES_WIDTH / 2);
            return false;
        }
        if (y > ExplorationZone.NUM_ZONES_HEIGHT) {
            System.out.println("Zone height out of bounds! Got:" + y + " should be:" + ExplorationZone.NUM_ZONES_HEIGHT);
            return false;
        }

        int robotId = Integer.parseInt(curRobotTask.robotID);

        GripsMidlevelTasksProtos.GripsMidlevelTasks task = prsTaskCreator.createWaitingTask((long) robotId, curRobotTask.zone);

        robotClient.sendPrsTaskToRobot(task);
        return true;
    }

    private MachineSide resolveMachineSide(String taskName) {
        switch (taskName) {
            case "GetBaseFromShelf":
                return MachineSide.SHELF;
            case "GetBaseFromCS":
                return MachineSide.OUTPUT;
            case "GetBaseFromMachine":
                return MachineSide.OUTPUT;
            case "DeliverBaseToCS":
                return MachineSide.INPUT;
            case "DeliverBaseToMachine":
                return MachineSide.INPUT;
            case "DeliverBaseToDS":
                return MachineSide.INPUT;
            default:
                throw new RuntimeException("Error: taskName not mappep! " + taskName);
        }
    }
}
