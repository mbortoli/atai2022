package com.grips.monitoring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.grips.persistence.domain.LockPart;
import com.grips.persistence.domain.ProductTask;
import com.grips.persistence.domain.BeaconSignalFromRobot;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.Ring;
import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.LockPartDao;
import com.grips.persistence.dao.PartDemandDao;
import com.grips.persistence.dao.ProductOrderDao;
import com.grips.persistence.dao.ProductTaskDao;
import com.grips.persistence.dao.RingDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.scheduler.GameField;
import com.visualization.TeamServerVisualization;
import com.visualization.TeamServerVisualizationImpl;
import com.visualization.domain.GamePointsData;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CommonsLog
public class VisualizationController {

    private final String recordingsPath = "/home/peter/team_server/recordings";

    @Autowired
    private ProductTaskDao productTaskDao;

    @Autowired
    private BeaconSignalFromRobotDao beaconSignalFromRobotDao;

    @Autowired
    private GameStateDao gameStateDao;

    @Autowired
    private PartDemandDao partDemandDao;

    @Autowired
    private LockPartDao lockPartDao;

    @Autowired
    private GameField gameField;

    @Autowired
    private RingDao ringDao;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductOrderDao productOrderDao;

    @CrossOrigin
    @RequestMapping("/visualization")
    public TeamServerVisualization getVisualizationData() {
        TeamServerVisualization vis = new TeamServerVisualization();

        // set gamephase and state for visualization, as well as teamcolor
        vis.setGameState(gameStateDao.findTop1ByOrderByGameTimeNanoSecondsDesc());

        // get all materialcounts
        //vis.setMaterialCounts(Lists.newArrayList(materialCountDao.findAll()));

        // get currently active producttasks


        vis.setActiveProductTasks(Lists.newArrayList(productTaskDao.findByState(ProductTask.ProductState.INWORK)));


        vis.setTimeStamp(System.currentTimeMillis());
        //vis.setRobot1Running(1);

        ArrayList<BeaconSignalFromRobot> beacons = new ArrayList<>();
        beacons.add(beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("1"));
        beacons.add(beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("2"));
        beacons.add(beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("3"));
        vis.setRobotBeaconSignals(beacons);

        vis.setActivePartDemands(Lists.newArrayList(partDemandDao.findAll()));

        Iterable<LockPart> allLockParts = lockPartDao.findAll();
        List<LockPart> filteredLockParts = Lists.newArrayList(allLockParts);
        filteredLockParts = filteredLockParts.stream().filter(l -> !l.getMachine().contains("SHELF")).collect(Collectors.toList());
        vis.setActiveLockParts(filteredLockParts);

        vis.setExplorationZones(gameField.getAllZones());
        List<ProductOrder> orders = new ArrayList<>();
        productOrderDao.findAll().forEach(orders::add);
        vis.setProductOrders(orders);

        List<Ring> rings = new ArrayList<>();
        ringDao.findAll().forEach(rings::add);
        vis.setRings(rings);

        return vis;
    }


    @CrossOrigin
    @RequestMapping("/recordings_list")
    public List<String> recordingsList() throws IOException {
        List<String> returner = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(recordingsPath))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(x -> x.getFileName().toString().endsWith(".json"))
                    .map(x -> x.getFileName().toString())
                    .forEach(returner::add);
        }
        return returner;
    }

    @CrossOrigin
    @RequestMapping("/recording/{name}")
    public String recordingsList(@PathVariable String name) throws IOException {
        return new String(Files.readAllBytes(Paths.get(recordingsPath + "/" + name)));
    }

    @SneakyThrows
    @CrossOrigin
    @RequestMapping("/gamepoints")
    public List<GamePointsData> gamePoints() {
        return recordingsList().stream()
                .filter(name -> name.startsWith("pro"))
                .map(name -> {
                    try {
                        TeamServerVisualizationImpl[] viz;
                        viz = objectMapper.readValue(new File(recordingsPath + "/" + name), TeamServerVisualizationImpl[].class);
                        GamePointsData data = new GamePointsData();
                        data.setGameName(name);
                        TeamServerVisualizationImpl last = viz[viz.length - 1];
                        data.setPointMagenta(last.getGameState().getPointsMagenta());
                        data.setPointsCyan(last.getGameState().getPointsCyan());
                        return data;
                    } catch (IOException e) {
                        log.error("Error reading: ", e);
                    }
                    //get points from last entry only!
                    return null;
                })
                .collect(Collectors.toList());
    }
}
