package com.grips.robot;

import com.google.common.collect.Lists;
import com.grips.persistence.domain.ExplorationZone;
import com.grips.persistence.domain.MachineReport;
import com.grips.persistence.dao.MachineReportDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.scheduler.GameField;
import com.robot_communication.services.RobotClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.ZoneProtos;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@CommonsLog
public class SendSuccessfullMachineReports {
    private final MachineReportDao machineReportDao;
    private final GameStateDao gameStateDao;
    private final GameField gameField;
    private final RobotClient robotClient;
    public SendSuccessfullMachineReports(MachineReportDao machineReportDao,
                                         GameStateDao gameStateDao,
                                         GameField gameField,
                                         RobotClient robotClient) {
        this.machineReportDao = machineReportDao;
        this.gameStateDao = gameStateDao;
        this.gameField = gameField;
        this.robotClient = robotClient;
    }

    public void sendSuccessfullMachineReports(int robotId) {
        int numberMachineReports = Lists.newArrayList(machineReportDao.findAll()).size();

        String teamColor = gameStateDao.getTeamColor();
        int currentPoints = 0;
        if (teamColor.startsWith("C")) {
            currentPoints = gameStateDao.getCurrentPointsCyan();
        } else if (teamColor.startsWith("M")) {
            currentPoints = gameStateDao.getCurrentPointsMagenta();
        }

        if ((numberMachineReports * 2) == currentPoints) {
            // up to now, all reports seem to be correct, so broadcast this information to current robot
            List<MachineInfoProtos.Machine> machines = new ArrayList<>();
            for (MachineReport mr : machineReportDao.findAll()) {
                MachineInfoProtos.Machine m = MachineInfoProtos.Machine.newBuilder()
                        .setName(mr.getName().getRawMachineName())
                        .setZone(ZoneProtos.Zone.valueOf(mr.getZone().toString()))
                        .setRotation(mr.getRotation()).build();
                machines.add(m);
                //System.out.println("sending successfully reported machine " + mr.getName() + " to robot");

                ExplorationZone zone = gameField.getZoneByName(mr.getZone().toString());
                if (zone != null) {
                    ExplorationZone mirroredZone = zone.getMirroredZone();

                    int rotation = gameField.rotateMachineIfNecessary((int) zone.getZoneNumber() / 10,
                            (int) zone.getZoneNumber() % 10, mr.getName(), mr.getRotation());

                    MachineInfoProtos.Machine m_mirrored = MachineInfoProtos.Machine.newBuilder()
                            .setName(mr.getName().mirror().getRawMachineName())
                            .setZone(ZoneProtos.Zone.valueOf(mirroredZone.getZoneName()))
                            .setRotation(rotation).build();
                    machines.add(m_mirrored);


                    //System.out.println("sending successfully reported machine " + m_name + " to robot");
                }
            }

            MachineInfoProtos.MachineInfo mi = MachineInfoProtos.MachineInfo.newBuilder().addAllMachines(machines).build();
            try {
                this.robotClient.sendToRobot(robotId, mi);
            } catch (NumberFormatException nfe) {
                log.error("Beaconsignal does not contain parseable robot id");
            }
        }
    }
}
