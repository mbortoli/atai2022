package com.grips.robot;

import com.google.protobuf.GeneratedMessageV3;
import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.protobuf_lib.RobotConnections;
import com.grips.refbox.RefBoxConnectionManager;
import com.grips.robot.msg_handler.*;
import com.grips.scheduled_tasks.SendAllKnownMachinesTask;
import com.grips.scheduler.api.DbService;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.robot_communication.services.HandleRobotMessageThread;
import com.robot_communication.services.IRobotMessageThreadFactory;
import com.robot_communication.services.RobotClient;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.net.Socket;
import java.util.AbstractMap;

@Service
@CommonsLog
public class HandleRobotMessageThreadFactory implements IRobotMessageThreadFactory {

    private final PrepareMachineMsgHandler prepareMachineMsgHandler;
    private final ReportAllMachinesMsgHandler reportAllMachinesMsgHandler;
    private final PrsTaskMsgHandler prsTaskMsgHandler;
    private final MachineOrientationMsgHandler machineOrientationMsgHandler;
    private final Mapper mapper;
    private final SendAllKnownMachinesTask sendAllKnownMachines;
    private final RobotConnections robotConnections;
    private final GameStateDao gameStateDao;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RefBoxConnectionManager rbcm;
    private final RobotClient robotClient;
    private final DbService dbService;
    private final IScheduler productionScheduler;
    private final ExplorationScheduler explorationScheduler;
    private final SendSuccessfullMachineReports sendSuccessfullMachineReports;


    public HandleRobotMessageThreadFactory(PrepareMachineMsgHandler prepareMachineMsgHandler,
                                           ReportAllMachinesMsgHandler reportAllMachinesMsgHandler,
                                           PrsTaskMsgHandler prsTaskMsgHandler,
                                           MachineOrientationMsgHandler machineOrientationMsgHandler,
                                           Mapper mapper,
                                           SendAllKnownMachinesTask sendAllKnownMachines,
                                           RobotConnections robotConnections,
                                           GameStateDao gameStateDao,
                                           BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                                           RefBoxConnectionManager rbcm,
                                           RobotClient robotClient,
                                           DbService dbService,
                                           @Qualifier("production-scheduler") IScheduler productionScheduler,
                                           ExplorationScheduler explorationScheduler,
                                           SendSuccessfullMachineReports sendSuccessfullMachineReports) {
        this.prepareMachineMsgHandler = prepareMachineMsgHandler;
        this.reportAllMachinesMsgHandler = reportAllMachinesMsgHandler;
        this.prsTaskMsgHandler = prsTaskMsgHandler;
        this.machineOrientationMsgHandler = machineOrientationMsgHandler;
        this.mapper = mapper;
        this.sendAllKnownMachines = sendAllKnownMachines;
        this.robotConnections = robotConnections;
        this.gameStateDao = gameStateDao;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.rbcm = rbcm;
        this.robotClient = robotClient;
        this.dbService = dbService;
        this.productionScheduler = productionScheduler;
        this.explorationScheduler = explorationScheduler;
        this.sendSuccessfullMachineReports = sendSuccessfullMachineReports;
    }


    public HandleRobotMessageThread create(Socket _socket, AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        return new HandleRobotMessageThread(createBeaconMsgHandler(_socket), prepareMachineMsgHandler, reportAllMachinesMsgHandler,
                prsTaskMsgHandler, machineOrientationMsgHandler, msg);
    }

    private BeaconMsgHandler createBeaconMsgHandler(Socket socket) {
        return new BeaconMsgHandler(socket, robotConnections, gameStateDao, mapper, beaconSignalFromRobotDao,
                rbcm, robotClient, dbService, productionScheduler, explorationScheduler,
                sendSuccessfullMachineReports, sendAllKnownMachines);
    }
}
