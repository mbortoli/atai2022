package com.grips.robot.msg_handler;

import com.grips.persistence.domain.BeaconSignalFromRobot;
import com.grips.model.teamserver.Peer;
import com.grips.model.teamserver.PeerState;
import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.protobuf_lib.RobotConnections;
import com.grips.protobuf_lib.RobotMessageRegister;
import com.grips.refbox.RefBoxConnectionManager;
import com.robot_communication.services.RobotClient;
import com.grips.robot.SendSuccessfullMachineReports;
import com.grips.scheduled_tasks.SendAllKnownMachinesTask;
import com.grips.scheduler.api.DbService;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.grips.config.Config;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_comm.ProtobufMessage;
import org.robocup_logistics.llsf_msgs.*;
import org.robocup_logistics.llsf_utils.Key;
import org.robocup_logistics.llsf_utils.NanoSecondsTimestampProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.net.Socket;
import java.util.function.Consumer;

@CommonsLog
public class BeaconMsgHandler implements Consumer<GripsBeaconSignalProtos.GripsBeaconSignal> {

    @Value("${gameconfig.scheduler.task_delay}")
    private int SLEEP_TASK_ASSIGNMENT;

    private final Socket socket;
    private final RobotConnections robotConnections;
    private final GameStateDao gameStateDao;
    private final Mapper mapper;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RefBoxConnectionManager rbcm;
    private final RobotClient robotClient;
    private final DbService dbService;
    private final IScheduler productionScheduler;
    private final ExplorationScheduler explorationScheduler;
    private final SendAllKnownMachinesTask sendAllKnownMachines;

    private boolean currentlyInAssignTask = false;

    public BeaconMsgHandler(Socket socket,
                            RobotConnections robotConnections,
                            GameStateDao gameStateDao,
                            Mapper mapper,
                            BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                            RefBoxConnectionManager rbcm,
                            RobotClient robotClient,
                            DbService dbService,
                            @Qualifier("production-scheduler") IScheduler productionScheduler,
                            ExplorationScheduler explorationScheduler,
                            SendSuccessfullMachineReports sendSuccessfullMachineReports,
                            SendAllKnownMachinesTask sendAllKnownMachines) {
        this.socket = socket;
        this.robotConnections = robotConnections;
        this.gameStateDao = gameStateDao;
        this.mapper = mapper;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.rbcm = rbcm;
        this.robotClient = robotClient;
        this.dbService = dbService;
        this.productionScheduler = productionScheduler;
        this.explorationScheduler = explorationScheduler;
        this.sendAllKnownMachines = sendAllKnownMachines;
    }

    @Override
    public void accept(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal) {

        int robotId = beacon_signal.getBeaconSignal().getNumber();

        if (!robotConnections.isRobotConnected(beacon_signal.getBeaconSignal().getNumber())) {
            Peer robot = mapper.map(beacon_signal.getBeaconSignal(), Peer.class);
            robot.setLastActive(System.currentTimeMillis());
            //robot.setId(beacon_signal.getBeaconSignal().getNumber());
            robot.setConnection(socket);
            robot.setWaiting(false);
            robotConnections.addRobot(robot);

            sendAllKnownMachines.sendAllKnownMachines((long) robotId);
        } else {
            robotConnections.getRobot(beacon_signal.getBeaconSignal().getNumber()).setLastActive(System.currentTimeMillis());
        }


        acceptPrivate(beacon_signal);
    }

    private void acceptPrivate(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal) {
        BeaconSignalFromRobot bSigRobot = mapper.map(beacon_signal.getBeaconSignal(), BeaconSignalFromRobot.class);
        bSigRobot.setTaskId(beacon_signal.getTaskId());
        Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(BeaconSignalProtos.BeaconSignal.class);

        int robotId = 0;
        try {
            robotId = Integer.parseInt(bSigRobot.getRobotId());
        } catch (Exception e) {
            System.err.println("Robot ID cannot be parsed!");
            return;
        }


        NanoSecondsTimestampProvider nstp = new NanoSecondsTimestampProvider();
        long ms = System.currentTimeMillis();
        long ns = nstp.currentNanoSecondsTimestamp();
        int sec = (int) (ms / 1000);
        long nsec = ns - (ms * 1000000L);
        TimeProtos.Time t = TimeProtos.Time.newBuilder().setSec(sec).setNsec(nsec).build();

        String teamColor = gameStateDao.getTeamColor();
        bSigRobot.setLocalTimestamp(System.currentTimeMillis());
        beaconSignalFromRobotDao.save(bSigRobot);

        Peer robot = robotConnections.getRobot(robotId);
        if (robot == null) {
            log.error("Robot with ID " + robotId + " not found in connection manager!!!");
            return;
        }


        if (null == teamColor) {
            log.error("TeamColor is NULL!");
            return;
        }

        BeaconSignalProtos.BeaconSignal bs = BeaconSignalProtos.BeaconSignal.newBuilder().setTime(t).setSeq(beacon_signal.getBeaconSignal().getSeq())
                .setNumber(beacon_signal.getBeaconSignal().getNumber()).setPeerName(beacon_signal.getBeaconSignal().getPeerName()).setTeamName(Config.TEAM_NAME)
                .setTeamColor(TeamProtos.Team.valueOf(teamColor)).setPose(beacon_signal.getBeaconSignal().getPose()).build();

        rbcm.sendPublicMsg(new ProtobufMessage(key.cmp_id, key.msg_id, bs));

        if (!currentlyInAssignTask && PeerState.ACTIVE.equals(robot.getRobotState())) {
            currentlyInAssignTask = true;
            // if a robot sends a beacon signal in exploration, we assign him a task
            // if the robot has no task
            if (com.shared.domain.GamePhase.EXPLORATION.equals(gameStateDao.getGamePhase())
                    && gameStateDao.getGameState().equals(com.shared.domain.GameState.RUNNING)
                    && !robotClient.isRobotsStopped()
                    && (System.currentTimeMillis() - robot.getTimeLastTaskAssignment()) > SLEEP_TASK_ASSIGNMENT) {
                handleExplorationPhase(beacon_signal, robotId, robot);
            }

            // if a robot sends a beacon signal in production, we assign him a task
            // if the robot has no task
            if (com.shared.domain.GamePhase.PRODUCTION.equals(gameStateDao.getGamePhase())
                    && gameStateDao.getGameState().equals(com.shared.domain.GameState.RUNNING)
                    && !robotClient.isRobotsStopped()
                    && (System.currentTimeMillis() - robot.getTimeLastTaskAssignment()) > SLEEP_TASK_ASSIGNMENT) { //todo this does not use the sim time!
                handleProductionPhase(beacon_signal, robotId, robot);
            }

            currentlyInAssignTask = false;
        }
    }

    private void handleProductionPhase(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, Peer robot) {
        // check if required information was already received from refbox
        if (dbService.checkProductionReady() && dbService.hasOrders()) {
            productionScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
        } else {
            log.warn("Not yet ready for production!");
        }
    }

    private void handleExplorationPhase(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, Peer robot) {
        explorationScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
    }
}
