package com.grips.robot.msg_handler;

import com.grips.persistence.domain.RobotObservation;
import com.grips.persistence.dao.BeaconSignalFromRefBoxDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.grips.tools.DiscretizeAngles;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.GripsMachineHandlingReportAllSeenMachinesProtos;
import org.robocup_logistics.llsf_msgs.RobotMachineReportProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public class ReportAllMachinesMsgHandler implements Consumer<GripsMachineHandlingReportAllSeenMachinesProtos.GripsMachineHandlingReportAllSeenMachines> {

    private final Mapper mapper;
    private final GameStateDao gameStateDao;
    private final BeaconSignalFromRefBoxDao beaconSignalFromRefBoxDao;
    private final ExplorationScheduler explorationScheduler;

    public ReportAllMachinesMsgHandler(Mapper mapper,
                                       GameStateDao gameStateDao,
                                       BeaconSignalFromRefBoxDao beaconSignalFromRefBoxDao,
                                       ExplorationScheduler explorationScheduler) {
        this.mapper = mapper;
        this.gameStateDao = gameStateDao;
        this.beaconSignalFromRefBoxDao = beaconSignalFromRefBoxDao;
        this.explorationScheduler = explorationScheduler;
    }

    @Override
    public void accept(GripsMachineHandlingReportAllSeenMachinesProtos.GripsMachineHandlingReportAllSeenMachines reportAllMachines) {
        long robot_id = reportAllMachines.getRobotId();
        for (RobotMachineReportProtos.RobotMachineReportEntry tmp_mre : reportAllMachines.getMachinesList()) {
            //int rotation = (int)(tmp_mre.getRotation()*180.0/Math.PI) + 180;
            //int rotation = (int)(tmp_mre.getRotation()*180.0/Math.PI);
            int rotation = tmp_mre.getRotation();
            rotation = (rotation % 360 + 360) % 360;
            rotation = DiscretizeAngles.discretizeAngles(rotation);

            //          System.out.println("REPORT machine: " + tmp_mre.getName() + " with orientation: " + tmp_mre.getRotation() + " and side: " + tmp_mre.getSide() + " from lidar: " + tmp_mre.getFromLidar());

            RobotObservation observation = mapper.map(tmp_mre, RobotObservation.class);
            observation.setRobotId(robot_id);
            observation.setObservationTimeGame(gameStateDao.findTop1ByOrderByGameTimeNanoSecondsDesc().getGameTimeNanoSeconds());
            observation.setObservationTimeAbsolut(beaconSignalFromRefBoxDao.findTop1ByOrderByTimeNanoSecondsDesc().getTimeNanoSeconds());
            observation.setMachineZoneConfidence(1.0);
            observation.setOrientation(rotation);
            observation.setSide(tmp_mre.getSide());
            observation.setFromLidar(tmp_mre.getFromLidar());
            explorationScheduler.addRobotObservation(observation);
            // is saved in explorationscheduler
            //robotObservationDao.save(observation);
        }
    }
}
