package com.grips.robot.msg_handler;

import com.grips.model.teamserver.MachineClient;
import com.grips.model.teamserver.MachineClientUtils;
import com.grips.persistence.dao.ProductOrderDao;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.protobuf_lib.RobotMessageRegister;
import com.grips.refbox.RefBoxConnectionManager;
import com.robot_communication.services.PrsTaskCreator;
import com.robot_communication.services.RobotClient;
import com.shared.domain.MachineName;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_comm.ProtobufMessage;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.GripsPrepareMachineProtos;
import org.robocup_logistics.llsf_msgs.MachineInstructionProtos;
import org.robocup_logistics.llsf_utils.Key;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Consumer;

@Service
@CommonsLog
public class PrepareMachineMsgHandler implements Consumer<GripsPrepareMachineProtos.GripsPrepareMachine> {

    private final RefBoxConnectionManager rbcm;
    private final MachineClient machineClient;
    private final SubProductionTaskDao subProductionTaskDao;
    private final ProductOrderDao productOrderDao;
    private final PrsTaskCreator prsTaskCreator;
    private final RobotClient robotClient;

    public PrepareMachineMsgHandler(RefBoxConnectionManager rbcm,
                                    MachineClient machineClient,
                                    SubProductionTaskDao subProductionTaskDao,
                                    ProductOrderDao productOrderDao,
                                    PrsTaskCreator prsTaskCreator,
                                    RobotClient robotClient) {
        this.rbcm = rbcm;
        this.machineClient = machineClient;
        this.subProductionTaskDao = subProductionTaskDao;
        this.productOrderDao = productOrderDao;
        this.prsTaskCreator = prsTaskCreator;
        this.robotClient = robotClient;
    }

    @Override
    public void accept(GripsPrepareMachineProtos.GripsPrepareMachine prepareMachine) {
        log.info("Received PrepareMachine: " + prepareMachine.toString());
        Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(MachineInstructionProtos.PrepareMachine.class);
        MachineName machineName = new MachineName(prepareMachine.getMachineId());
        List<SubProductionTask> tasks = this.subProductionTaskDao.findByRobotIdAndState(prepareMachine.getRobotId(), SubProductionTask.TaskState.INWORK);
        if (tasks.size() != 1) {
            throw new RuntimeException(tasks.size() + " INWORK tasks found for robot: " + prepareMachine.getRobotId());
        }
        if (machineName.isCapStation()) {
            prepareCapStation(machineName, tasks.get(0));
        } else if (machineName.isBaseStation()) {
            prepareBaseStation(tasks.get(0));
        } else if (machineName.isRingStation()) {
            prepareRingStation(machineName, tasks.get(0));
        } else if (machineName.isDeliveryStation()) {
            prepareDeliveryStation(tasks.get(0));
        } else {
            throw new RuntimeException("Unsupported Machine: " + machineName.toString());
        }
        rbcm.sendPrivateMsg(new ProtobufMessage(key.cmp_id, key.msg_id, prepareMachine));
        log.info("Received PrepareMachine for machine " + prepareMachine.getMachineId() + "from robot: " + prepareMachine.getRobotId() + ".");    // TODO: From robot?
    }

    private void prepareDeliveryStation(SubProductionTask task) {
        ProductOrder pOrder = productOrderDao.findById(task.getOrderInfoId()).get();
        this.machineClient.sendPrepareDS(new Long(pOrder.getDeliveryGate()).intValue(), new Long(pOrder.getId()).intValue());
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(task.getRobotId().longValue(), task.getMachine(), "input");
        robotClient.sendToRobot(task.getRobotId(), success);
    }

    private void prepareRingStation(MachineName machineName, SubProductionTask task) {
        this.machineClient.sendPrepareRS(machineName.asMachineEnum(), toRefboxRingColor(task.getRequiredColor()));
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(task.getRobotId().longValue(), task.getMachine(), "input");
        robotClient.sendToRobot(task.getRobotId(), success);
    }

    private void prepareBaseStation(SubProductionTask task) {
        this.machineClient.sendPrepareBS(task.getSide() == MachineSide.INPUT ? MachineClientUtils.MachineSide.Input : MachineClientUtils.MachineSide.Output, MachineClientUtils.BaseColor.valueOf(task.getRequiredColor()));
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(task.getRobotId().longValue(), task.getMachine(), task.getSide().toString());
        robotClient.sendToRobot(task.getRobotId(), success);
    }

    private void prepareCapStation(MachineName machineName, SubProductionTask task) {
        this.machineClient.sendPrepareCS(machineName.asMachineEnum(), MachineClientUtils.CSOp.valueOf(task.getOptCode()));
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(task.getRobotId().longValue(), task.getMachine(), "input");
        robotClient.sendToRobot(task.getRobotId(), success);
    }

    private MachineClientUtils.RingColor toRefboxRingColor(String requiredColor) {
        switch (requiredColor) {
            case "RING_YELLOW":
                return MachineClientUtils.RingColor.RING_YELLOW;
            case "RING_BLUE":
                return MachineClientUtils.RingColor.RING_BLUE;
            case "RING_GREEN":
                return MachineClientUtils.RingColor.RING_GREEN;
            case "RING_ORANGE":
                return MachineClientUtils.RingColor.RING_ORANGE;
        }
        throw new IllegalArgumentException("Unkown ring Color: " + requiredColor);
    }
}
