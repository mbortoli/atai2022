package com.grips.tools;

import java.awt.geom.Point2D;
import java.util.*;

import static java.lang.Math.abs;

public class AStarFinder {

    private boolean _allowDiagonal = true;
    private boolean _dontCrossCorners = false;

    // Implements an AStar Path Finder with Octile Heuristic
    public AStarFinder(boolean allowDiagonal, boolean dontCrossCorners) {
        _allowDiagonal = allowDiagonal;
        _dontCrossCorners = dontCrossCorners;
    }

    public LinkedList<Node> findPath(int startX, int startY, int endX, int endY, Grid grid) {
        LinkedList<Node> openList = new LinkedList<>();

        Node startNode = grid.getNodeAt(startX, startY);
        Node endNode = grid.getNodeAt(endX, endY);

        startNode.setG(0);
        startNode.setF(0);

        openList.push(startNode);
        startNode.setOpen(true);

        while (!openList.isEmpty()) {
            Node currentNode = openList.pop();
            currentNode.setClose(true);

            if (currentNode.getX() == endX && currentNode.getY() == endY) {
                return backtrace(endNode);
            }

            List<Node> neighbors = grid.getNeighbors(currentNode, _allowDiagonal);
            for (Node neighbor : neighbors) {
                if (neighbor.isClose()) {
                    continue;
                }

                //calculate distance between current node and neighbor
                double ng = currentNode.getG() + ((neighbor.getX() - currentNode.getX() == 0 || neighbor.getY() - currentNode.getY() == 0) ? 1.0 : Math.sqrt(2.0));

                //check if neighbor has not been inspected yet
                // or can be reached with a smaller cost from the current node
                if (!neighbor.isOpen() || ng < neighbor.getG()) {
                    neighbor.setG(ng);
                    if (neighbor.getH() == Double.MAX_VALUE) {
                        neighbor.setH(octile(abs(neighbor.getX() - endX), abs(neighbor.getY() - endY)));
                    }
                    neighbor.setF(neighbor.getG() + neighbor.getH());
                    neighbor.setParent(currentNode);

                    if (!neighbor.isOpen()) {
                        openList.push(neighbor);
                        neighbor.setOpen(true);
                    }

                    reSortList(openList);
                }
            }
        }

        // we could not find a path
        return new LinkedList<>();
    }

    private void reSortList(LinkedList<Node> toSort) {
        Collections.sort(toSort, new Comparator<Node>() {
            @Override
            public int compare(Node nodeA, Node nodeB) {
                double diff = nodeA.getF() - nodeB.getF();

                if (diff < 0.0) return -1;
                else if (diff > 0.0) return 1;
                else return 0;
            }
        });
    }

    public double pathLength(LinkedList<Node> path) {
        double sum = 0.0;
        for (int i = 1; i < path.size(); ++i) {
            Node a = path.get(i-1);
            Node b = path.get(i);
            int dx = a.getX() - b.getX();
            int dy = a.getY() - b.getY();
            sum += Math.sqrt(dx*dx + dy*dy);
        }

        return sum;
    }

    private LinkedList<Node> backtrace(Node node) {
        LinkedList<Node> path = new LinkedList<>();
        path.push(node);

        Node tempNode = node;
        while (tempNode.getParent() != null) {
            tempNode = tempNode.getParent();
            path.push(tempNode);
        }

        Collections.reverse(path);
        return path;
    }

    private double octile(double dx, double dy) {
        double F = Math.sqrt(2) - 1.0;
        return (dx < dy) ? F * dx + dy : F * dy + dx;
    }
}
