package com.grips.tools;

import com.grips.persistence.dao.GameStateDao;
import com.grips.persistence.domain.ExplorationZone;
import com.grips.config.Config;
import com.shared.domain.MachineSide;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PathEstimator {

    private Grid _grid;
    private AStarFinder _astarFinder;
    private Map<String, GridMPS> _machines;

    @Autowired private GameStateDao gameStateDao;
    private boolean shortestDisposalCalculated = false;

    private String disposalCS1 = null;
    private String disposalCS2 = null;

    private HashMap<String, Double> rotStartMatrix;
    private HashMap<String, Double> rotEndMatrix;

    public PathEstimator() {
       // create grid representing our gamefield, one node is 10cm x 10cm
       _grid = new Grid(ExplorationZone.NUM_ZONES_WIDTH*10, ExplorationZone.NUM_ZONES_HEIGHT*10);
       _astarFinder = new AStarFinder(true, true);
       _machines = new HashMap<>();
        rotStartMatrix = new HashMap<String, Double>();
        rotEndMatrix = new HashMap<String, Double>();
    }

    public void addMachine(String name, double centerX, double centerY, int rotation) {

        if (!_machines.containsKey(name)) {
            // account for negative X values
            centerX += ExplorationZone.NUM_ZONES_WIDTH/2;

            // also account vor inverted Y
            centerY = ExplorationZone.NUM_ZONES_HEIGHT - centerY;

            int intCenterX = (int)Math.round(centerX*10);
            int intCenterY = (int)Math.round(centerY*10);
            int startX = (int)((int)intCenterX/(int)10)*10;
            int startY = (int)((int)intCenterY/(int)10)*10; //our grid starts top-left, LLSF starts bottom middle(left)

            List<Node> occupiedNodes = new ArrayList<>();
            for (int i = 1; i <= 10; ++i) {
                for (int j = 1; j <= 10; ++j) {
                    _grid.setOccupied(startX + i, startY + j, true);
                    occupiedNodes.add(_grid.getNodeAt(startX + i, startY + j));
                }
            }

            int inputX, inputY, outputX, outputY;

            switch (rotation) {
                case 0: inputX = startX + 10; inputY = startY + 5; outputX = startX - 1; outputY = startY + 5; break;
                case 45: inputX = startX + 10; inputY = startY - 1; outputX = startX - 1; outputY = startY + 10; break;
                case 90: inputX = startX + 5; inputY = startY - 1; outputX = startX + 5; outputY = startY + 10; break;
                case 135: inputX = startX - 1; inputY = startY - 1; outputX = startX + 10; outputY = startY + 10; break;
                case 180: inputX = startX - 1; inputY = startY + 5; outputX = startX + 10; outputY = startY + 5; break;
                case 225: inputX = startX - 1; inputY = startY + 10; outputX = startX + 10; outputY = startY - 1; break;
                case 270: inputX = startX + 5; inputY = startY + 10; outputX = startX + 5; outputY = startY - 1; break;
                case 315: inputX = startX + 10; inputY = startY + 10; outputX = startX - 1; outputY = startY - 1; break;
                default: inputX = startX; inputY = startY; outputX = startX; outputY = startY;
                    System.err.println("Rotation of " + rotation + " not known to pathestimator!");
            }

            Node inputNode = _grid.getNodeAt(inputX+1, inputY+1); // we need to add 1, since we start at (1,1) instead of (0,0)
            Node outputNode = _grid.getNodeAt(outputX+1, outputY+1);

            GridMPS gmps = new GridMPS(inputNode, outputNode, occupiedNodes);
            _machines.put(name, gmps);
        }
    }

    public double pathLength(double startX, double startY, double endX, double endY) {
        _grid.resetPathCosts();

        //account for negative X values
        startX += ExplorationZone.NUM_ZONES_WIDTH/2.0;
        endX += ExplorationZone.NUM_ZONES_WIDTH/2.0;

        //also account for inverted Y
        startY = ExplorationZone.NUM_ZONES_HEIGHT - startY;
        endY = ExplorationZone.NUM_ZONES_HEIGHT - endY;

        // round positions to nearest integer to get start and end
        LinkedList<Node> path = _astarFinder.findPath((int) Math.round(startX), (int) Math.round(startY), (int) Math.round(endX), (int) Math.round(endY), _grid);
        if (path == null || path.size() == 0) {
            System.err.println("No path could be found from " + startX + "/" + startY + " to " + endX + "/" + endY);
            return Double.MAX_VALUE;
        } else {
            return _astarFinder.pathLength(path);
        }
    }

    public double pathLength(double startX, double startY, String endMachine, MachineSide endSide) {
        _grid.resetPathCosts();

        //account for negative X values
        startX += ExplorationZone.NUM_ZONES_WIDTH/2.0;

        //also account for inverted Y
        startY = ExplorationZone.NUM_ZONES_HEIGHT - startY;

        // round positions to nearest integer to get start and end
        Node endMachineNode = getMachineNode(endMachine, endSide);
        if (endMachineNode == null) {
            return Double.MAX_VALUE;
        }

        LinkedList<Node> path = _astarFinder.findPath((int) Math.round(startX), (int)Math.round(startY), endMachineNode.getX(), endMachineNode.getY(), _grid);
        if (path == null || path.size() == 0) {
            System.err.println("No path could be found from " + startX + "/" + startY + " to " + endMachine + " " + endSide);
            //return Double.MAX_VALUE;
            return 30;
        } else {
            return _astarFinder.pathLength(path);
        }
    }

    public double pathLength(String startMachine, MachineSide startSide, String endMachine, MachineSide endSide) {
        _grid.resetPathCosts();

        Node endMachineNode = getMachineNode(endMachine, endSide);
        Node startMachineNode = getMachineNode(startMachine, startSide);
        if (startMachineNode == null || endMachineNode == null) {
            return Double.MAX_VALUE;
        }

        LinkedList<Node> path = _astarFinder.findPath(startMachineNode.getX(), startMachineNode.getY(), endMachineNode.getX(), endMachineNode.getY(), _grid);
        if (path == null || path.size() == 0) {
            System.err.println("No path could be found from " + startMachine + " " + startSide + " to " + endMachine + " " + endSide);
            return Double.MAX_VALUE;
        } else {
            //_grid.printGridWithPath(path);
            Node start_oppositeSide;
            if (startSide == MachineSide.INPUT)
                start_oppositeSide = getMachineNode(startMachine, MachineSide.OUTPUT);
            else
                start_oppositeSide = getMachineNode(startMachine, MachineSide.INPUT);
            Node end_oppositeSide;
            if (endSide == MachineSide.INPUT)
                end_oppositeSide = getMachineNode(endMachine, MachineSide.OUTPUT);
            else
                end_oppositeSide = getMachineNode(endMachine, MachineSide.INPUT);
            if (path.size() > 1){
                double start_rotation_cost = rotationCost(path.get(path.size()-1), path.get(path.size()-2), start_oppositeSide, true);
                double end_rotation_cost = rotationCost(path.get(1), path.get(0) , end_oppositeSide, false);

                double start_rotation_degree = rotationDegree(path.get(path.size()-1), path.get(path.size()-2), start_oppositeSide);
                double end_rotation_degree = rotationDegree(path.get(1), path.get(0) , end_oppositeSide);


                //double start_rot_degree = getRotationDegree(path.get(path.size()-1), path.get(path.size()-2), start_oppositeSide);

                /*
                int counter = 0;
                while (counter < path.size()) {
                    System.out.println("++ (" + path.get(counter).getX() + "," + path.get(counter).getY() + ")");
                    counter++;
                }*/
                //System.out.println("+++++++++++ENDING ROTATION BETWEEN " + startMachine + startSide + " AND " + endMachine + endSide + " IS " + end_rotation_degree + " WITH COST " + end_rotation_cost);
                //System.out.println("FIRST NODE AND MACHINE SIDE (" + path.get(0).getX() + "," + path.get(0).getY() + ") (" + endMachineNode.getX() + "," + endMachineNode.getY() + ")"  );
                //System.out.println("++++++++++COORDINATE (" + path.get(path.size()-1).getX() + "," + path.get(path.size()-1).getY() + ") ("  + path.get(path.size()-2).getX() + "," + path.get(path.size()-2).getY() + ") ("  + oppositeSide.getX() + "," + oppositeSide.getY() + ")" + "("  + startMachineNode.getX() + "," + startMachineNode.getY() + ")" );
                rotStartMatrix.put(startMachine+"_"+startSide.toString().toLowerCase(Locale.ROOT)+endMachine+"_"+endSide.toString().toLowerCase(Locale.ROOT),start_rotation_degree);
                rotEndMatrix.put(startMachine+"_"+startSide.toString().toLowerCase(Locale.ROOT)+endMachine+"_"+endSide.toString().toLowerCase(Locale.ROOT),end_rotation_degree);

                return _astarFinder.pathLength(path) + start_rotation_cost + end_rotation_cost  ;


            }
            return _astarFinder.pathLength(path);  //+ rotation_cost;
        }
    }


    public double getRotationCost (String key, boolean isStarting) {
        if (isStarting)
            return rotStartMatrix.get(key);
        else
            return rotEndMatrix.get(key);
    }

    //to determine the initial orientation of the robot, I need robot position and the position of the opposite side of the machine. Then I calculate the vertex wrt the next point of the A* path
    public double rotationDegree (Node robot, Node direction, Node machine) {
        double angleDegree;
        try {
            double angleRad = Math.atan2(machine.getY() - robot.getY(), machine.getX() - robot.getX()) - Math.atan2(direction.getY() - robot.getY(), direction.getX() - robot.getX());
            angleDegree = Math.toDegrees(angleRad);
        } catch (Exception e) {
            angleDegree = 90;
            System.out.println("atan2 error, setting standard 90 value to angleRad: " + e);
        }
        if (angleDegree > 180)
            angleDegree = 360 - angleDegree;
        if (angleDegree < -180)
            angleDegree = -360 - angleDegree;
        return angleDegree;
    }

    public double rotationCost (Node robot, Node direction, Node machine, boolean isStart) {
        double rot_degree = Math.abs(rotationDegree(robot, direction, machine));
        if (isStart) {  //the robot starts always with its back facing the machine
            if (rot_degree < 180)
                rot_degree += 180;
            else
                rot_degree -= 180;
        }
        double rotation_cost;
        if (rot_degree < 10)
            rotation_cost = 0;
        else if (rot_degree < 50)
            rotation_cost = 7;
        else if (rot_degree <= 90)
            rotation_cost = 14;
        else if (rot_degree <= 135)
            rotation_cost = 21;
        else rotation_cost = 28;
        return rotation_cost;
    }

    public Node getMachineNode(String machine, MachineSide side) {
        if (!_machines.containsKey(machine)) {
            System.err.println("Machine " + machine + " not known to pathestimator!");
            return null;
        }

        Node machineNode;
        if (side == MachineSide.INPUT || side == MachineSide.SHELF) {
            machineNode = _machines.get(machine).getInput();
        } else if (side == MachineSide.OUTPUT) {
            machineNode = _machines.get(machine).getOutput();
        } else {
            System.err.println("Unknown machine side specified for pathestimator on machine " + machine);
            return null;
        }
        return machineNode;
    }

    public void printCurrentGrid() {
        _grid.printGridWithPath(new LinkedList<>());
    }

    public boolean allMachinesAvailable() {
        if (_machines.size() == 14) {
            return true;
        }

        return false;
    }

    public void calculateShortestDisposalMachines() {
        if (!allMachinesAvailable() || shortestDisposalCalculated) return;

        String colorprefix;
        try {
            colorprefix = gameStateDao.getTeamColor().equals(Config.TEAM_CYAN_COLOR) ? "C-" : "M-";
        } catch (Exception e) {
            System.out.println("Team color undefined! Setting default to CYAN!");
            colorprefix = "C-";
        }
        String cs1 = colorprefix + "CS1";
        String cs2 = colorprefix + "CS2";
        String rs1 = colorprefix + "RS1";
        String rs2 = colorprefix + "RS2";

        double c1rs1 = pathLength(cs1, MachineSide.INPUT, rs1, MachineSide.INPUT);
        double c1rs2 = pathLength(cs1, MachineSide.INPUT, rs2, MachineSide.INPUT);
        double c2rs1 = pathLength(cs2, MachineSide.INPUT, rs1, MachineSide.INPUT);
        double c2rs2 = pathLength(cs2, MachineSide.INPUT, rs2, MachineSide.INPUT);

        // no find the shortest combined path
        if ((c1rs1 + c2rs2) < (c1rs2 + c2rs1)) {
            disposalCS1 = rs1;
            disposalCS2 = rs2;
        } else {
            disposalCS1 = rs2;
            disposalCS2 = rs1;
        }

        System.out.println("Defined preferred disposal for CS1 as " + disposalCS1);
        System.out.println("Defined preferred disposal for CS2 as " + disposalCS2);
        shortestDisposalCalculated = true;
    }

    public String getDisposalCS1() {
        return disposalCS1;
    }

    public String getDisposalCS2() {
        return disposalCS2;
    }
}
