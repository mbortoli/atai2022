package com.grips.tools;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class DiscretizeAngles {
    public static int discretizeAngles(int angle) {
        Integer[] arr = {0, 45, 90, 135, 180, 225, 270, 315, 360};
        Vector<Integer> steps = new Vector<>(Arrays.asList(arr));

        Collections.sort(steps, Comparator.comparing(p -> Math.abs(p - angle)));
        return steps.get(0) % 360;
    }
}
