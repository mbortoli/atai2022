package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TimeOffset {
    private Integer robot1;
    private Integer robot2;
    private Integer robot3;
}
