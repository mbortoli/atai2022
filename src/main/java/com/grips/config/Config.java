/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.config;

import com.google.common.primitives.Ints;
import com.grips.model.teamserver.MachineClient;
import com.grips.model.teamserver.TeamColor;
import com.grips.persistence.dao.*;
import com.grips.protobuf_lib.ProtobufServer;
import com.grips.protobuf_lib.RobotConnections;
import com.grips.refbox.*;
import com.grips.robot.HandleRobotMessageThreadFactory;
import com.grips.team_server.SubTaskGenerator;
import com.grips.tools.PathEstimator;
import com.robot_communication.services.RobotClient;
import com.grips.robot.RobotClientWrapper;
import com.grips.scheduler.GameField;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.asp.AspScheduler;
import com.grips.scheduler.challanges.GraspingScheduler;
import com.grips.scheduler.production.*;
import com.grips.scheduler.production.resouce_managment.ResourceDemandService;
import com.grips.scheduler.production.resouce_managment.ResourceLockService;
import com.grips.team_server.MPSHandler;
import com.robot_communication.services.PrsTaskCreator;
import com.robot_communication.services.RobotHandler;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.math.BigInteger;
import java.util.*;

@Configuration
@CommonsLog
public class Config {
    public static final long NUM_MILIS_ROBOT_LOST = 5000;


    public static final BigInteger NUM_NSEC_REPORT_AR_ONLY = new BigInteger("176000000000"); // 2min 57s

    public static final BigInteger NUM_NSECS_EXPLORATION_START_REPORTING = new BigInteger("0");
    public static final int NUM_RING_COLORS = 4;

    // values are only default values and will be overwritten
    // by values defined in springs application.properties file!!!
    public static int ROBOT_LISTEN_PORT = 2016;
    public static String TEAM_NAME = "GRIPS";
    public static String TEAM_CYAN_COLOR = "CYAN";

    @Value("${gameconfig.robot.recvport}")
    public void setRobotListenPort(int port) {
        ROBOT_LISTEN_PORT = port;
    }

    public static int PRIORITY_FIRST_RING;
    public static int PRIORITY_SECOND_RING;
    public static int PRIORITY_THIRD_RING;
    public static int PRIORITY_MOUNT_CAP_C0;
    public static int PRIORITY_MOUNT_CAP_C1;
    public static int PRIORITY_MOUNT_CAP_C2;
    public static int PRIORITY_MOUNT_CAP_C3;
    public static int PRIORITY_DELIVER_C0;
    public static int PRIORITY_DELIVER_C1;
    public static int PRIORITY_DELIVER_C2;
    public static int PRIORITY_DELIVER_C3;
    public static int PRIORITY_MOUNT_CAP_C0_COMPETITIVE;
    public static int PRIORITY_DELIVER_C0_COMPETITIVE;

    @Value("${gameconfig.productionpriority.firstRing}")
    public void setPriorityFirstRing(int p) {
        PRIORITY_FIRST_RING = p;
    }

    @Value("${gameconfig.productionpriority.secondRing}")
    public void setPrioritySecondRing(int p) {
        PRIORITY_SECOND_RING = p;
    }

    @Value("${gameconfig.productionpriority.thirdRing}")
    public void setPriorityThirdRing(int p) {
        PRIORITY_THIRD_RING = p;
    }


    @Value("${gameconfig.productionpriority.mountCapC0}")
    public void setPriorityMountCapC0(int p) {
        PRIORITY_MOUNT_CAP_C0 = p;
    }

    @Value("${gameconfig.productionpriority.mountCapC1}")
    public void setPriorityMountCapC1(int p) {
        PRIORITY_MOUNT_CAP_C1 = p;
    }

    @Value("${gameconfig.productionpriority.mountCapC2}")
    public void setPriorityMountCapC2(int p) {
        PRIORITY_MOUNT_CAP_C2 = p;
    }

    @Value("${gameconfig.productionpriority.mountCapC3}")
    public void setPriorityMountCapC3(int p) {
        PRIORITY_MOUNT_CAP_C3 = p;
    }

    @Value("${gameconfig.productionpriority.deliverC0}")
    public void setPriorityDeliverC0(int p) {
        PRIORITY_DELIVER_C0 = p;
    }

    @Value("${gameconfig.productionpriority.deliverC1}")
    public void setPriorityDeliverC1(int p) {
        PRIORITY_DELIVER_C1 = p;
    }

    @Value("${gameconfig.productionpriority.deliverC2}")
    public void setPriorityDeliverC2(int p) {
        PRIORITY_DELIVER_C2 = p;
    }

    @Value("${gameconfig.productionpriority.deliverC3}")
    public void setPriorityDeliverC3(int p) {
        PRIORITY_DELIVER_C3 = p;
    }

    @Value("${gameconfig.productionpriority.mountCapC0competitive}")
    public void setPriorityMountCap0competitive(int p) {
        PRIORITY_MOUNT_CAP_C0_COMPETITIVE = p;
    }

    @Value("${gameconfig.productionpriority.deliverC0competitive}")
    public void setPriorityDeliverC0Competitive(int p) {
        PRIORITY_DELIVER_C0_COMPETITIVE = p;
    }

    @Bean()
    public RobotConnections create_RobotConnections() {
        return new RobotConnections((int) NUM_MILIS_ROBOT_LOST);
    }

    @Bean()
    public RefBoxConnectionManager create_RefboxConnectionManager(RefboxConfig config,
                                                                  PrivateRefBoxHandler teamHandler,
                                                                  PublicRefBoxHandler publicHandler) {
        RefBoxConnectionManager rbcm =
                new RefBoxConnectionManager(
                        new RefboxConnectionConfig(
                                config.getIp(),
                                new PeerConfig(config.getPublic_port_receive(), config.getPublic_port_send()),
                                new PeerConfig(config.getPrivate_port_receive_cyan(), config.getPrivate_port_send_cyan()),
                                new PeerConfig(config.getPrivate_port_receive_magenta(), config.getPrivate_port_send_magenta())),
                        new TeamConfig(config.getCrypto_key(), config.getTeamcolor()),
                        teamHandler,
                        publicHandler);
        try {
            rbcm.startServer();
            log.info("Started RefBoxConnectionManager Servers!");
        } catch (Exception e) {
            log.error("Error starting RefboxConnectionManager - is the refbox running?", e);
        }
        return rbcm;
    }

    @Bean()
    public MachineClient create_MachineClient(RefboxConfig config) {
        if (config.getTeamcolor() == null) {
            throw new RuntimeException("TeamColor is not configured!");
        }
        return new MachineClient(TeamColor.fromString(config.getTeamcolor()));
    }

    @Bean
    public RobotClient robotClient(ProtobufServer protobufServer, PrsTaskCreator prsTaskCreator, RobotConnections robotConnections) {
        return new RobotClient(protobufServer, prsTaskCreator, robotConnections);
    }

    @Bean()
    public PrsTaskCreator prsTaskCreator() {
        return new PrsTaskCreator();
    }

    @Bean
    @Scope("prototype")
    public RobotHandler robotHandler(RobotConnections robotConnections,
                                     HandleRobotMessageThreadFactory robotMessageThreadFactory) {
        return new RobotHandler(robotConnections, robotMessageThreadFactory);
    }

    @Bean
    @Qualifier("production-scheduler")
    public IScheduler createProductionScheduler(ClassesConfig classesConfig,
                                                RobotClient robotClient,
                                                ResourceDemandService resourceDemandService,
                                                SubProductionTaskDao subProductionTaskDao,
                                                ProductTaskDao productTaskDao,
                                                GameStateDao gameStateDao,
                                                MPSHandler mpsHandler,
                                                MachineInfoRefBoxDao machineInfoRefBoxDao,
                                                PartDemandDao partDemandDao,
                                                SubProductionUtils subProductionUtils,
                                                DefaultTaskFinder defaultTaskFinder,
                                                GameField gameField,
                                                MachineClient machineClient,
                                                PrsTaskCreator prsTaskCreator,
                                                ResourceLockService resourceLockService,
                                                RobotClientWrapper robotClientWrapper,
                                                RingDao ringDao,
                                                SubTaskGenerator subTaskGenerator,
                                                ProductOrderDao productOrderDao,
                                                AtomDao atomDao1,
                                                BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                                                PathEstimator pathEstimator) {
        if (ProductionScheduler.class.getSimpleName().equalsIgnoreCase(classesConfig.getProductionScheduler())) {
            return new ProductionScheduler(robotClient, resourceDemandService,
                    subProductionTaskDao, productTaskDao, gameStateDao,
                    machineInfoRefBoxDao, mpsHandler, partDemandDao,
                    defaultTaskFinder, subProductionUtils, gameField,
                    machineClient, resourceLockService, robotClientWrapper);
        } else if (AspScheduler.class.getSimpleName().equalsIgnoreCase(classesConfig.getProductionScheduler())) {
            return new AspScheduler(ringDao,subTaskGenerator, robotClient, machineClient, mpsHandler, productOrderDao, productTaskDao, gameStateDao, machineInfoRefBoxDao, subProductionTaskDao, atomDao1, beaconSignalFromRobotDao, resourceDemandService, resourceLockService, robotClientWrapper, pathEstimator);
        } else if (GraspingScheduler.class.getSimpleName().equalsIgnoreCase(classesConfig.getProductionScheduler())) {
            return new GraspingScheduler(prsTaskCreator, robotClient);
        } else {
            log.error("Invalid Scheduler class: " + Optional.ofNullable(classesConfig.getProductionScheduler()).orElse("NOT SET"));
            System.exit(1);
        }
        return null;
    }
}
