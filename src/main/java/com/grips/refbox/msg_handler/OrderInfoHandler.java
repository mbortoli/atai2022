package com.grips.refbox.msg_handler;

import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.dao.ProductOrderDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.scheduler.api.DbService;
import com.grips.team_server.SubTaskGenerator;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.OrderInfoProtos;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Optional;
import java.util.function.Consumer;

@Service
public class OrderInfoHandler implements Consumer<OrderInfoProtos.OrderInfo> {

    private final Mapper mapper;
    private final DbService dbService;
    private final ProductOrderDao productOrderDao;
    private final GameStateDao gameStateDao;
    private final SubTaskGenerator subTaskGenerator;

    public OrderInfoHandler(Mapper mapper,
                            DbService dbService, ProductOrderDao productOrderDao,
                            GameStateDao gameStateDao,
                            SubTaskGenerator subTaskGenerator) {
        this.mapper = mapper;
        this.dbService = dbService;
        this.productOrderDao = productOrderDao;
        this.gameStateDao = gameStateDao;
        this.subTaskGenerator = subTaskGenerator;
    }

    @Override
    public void accept(OrderInfoProtos.OrderInfo orderInfo) {
        for (OrderInfoProtos.Order tmp_order : orderInfo.getOrdersList()) {
            ProductOrder productOrder = mapper.map(tmp_order, ProductOrder.class);
            //productOrder.calculateProperties();
            if (dbService.checkProductionReady()) {
                if (!productOrderDao.existsById(productOrder.getId())) {
                    productOrder.setCreationGameTime(Optional.ofNullable(
                            gameStateDao.getLatestGameTimeProductionPhase())
                            .map(BigInteger::longValue).orElse((0L)));
                    productOrder.setDelivered(false);
                    productOrderDao.save(productOrder);
                    subTaskGenerator.generateSubTasks(productOrder);
                } else {
                    productOrder.setCreationGameTime(productOrderDao.findById(productOrder.getId()).get().getCreationGameTime());
                }
            }
        }
    }
}
