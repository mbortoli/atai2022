package com.grips.refbox.msg_handler;

import com.grips.model.teamserver.TeamColor;
import com.shared.domain.MachineName;
import com.grips.persistence.domain.ExplorationZone;
import com.grips.model.teamserver.MachineClient;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.refbox.PrivateRefBoxHandler;
import com.grips.refbox.PublicRefBoxHandler;
import com.grips.refbox.RefBoxConnectionManager;
import com.grips.scheduler.GameField;
import com.grips.scheduler.production.SubProductionTaskUpdater;
import com.grips.tools.PathEstimator;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@CommonsLog
@Service
public class MachineInfoHandler implements Consumer<MachineInfoProtos.MachineInfo> {

    private final MachineClient machineClient;
    private final SubProductionTaskUpdater subProductionTaskUpdater;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final Mapper mapper;
    private final GameField gameField;
    private final PathEstimator pathEstimator;
    private final RefBoxConnectionManager rbcm;

    public MachineInfoHandler(MachineClient machineClient,
                              SubProductionTaskUpdater subProductionTaskUpdater,
                              MachineInfoRefBoxDao machineInfoRefBoxDao,
                              Mapper mapper, GameField gameField,
                              PathEstimator pathEstimator,
                              RefBoxConnectionManager rbcm,
                              PrivateRefBoxHandler privateRefBoxHandler, PublicRefBoxHandler publicRefBoxHandler) {
        this.machineClient = machineClient;
        this.subProductionTaskUpdater = subProductionTaskUpdater;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.mapper = mapper;
        this.gameField = gameField;
        this.pathEstimator = pathEstimator;
        this.rbcm = rbcm;
        privateRefBoxHandler.setMachineInfoCallback(this); //todo check if public of private peer!!
        publicRefBoxHandler.setMachineInfoCallback(this); //todo check if public of private peer!!

    }

    @Override
    public void accept(MachineInfoProtos.MachineInfo machineInfo) {
        machineClient.update(machineInfo);
        subProductionTaskUpdater.updateTaskPerMachine(machineInfo);
        List<MachineInfoRefBox> machines = new ArrayList<>();
        for (MachineInfoProtos.Machine tmp_machine : machineInfo.getMachinesList()) {
            MachineInfoRefBox m_info = mapper.map(tmp_machine, MachineInfoRefBox.class);

            MachineInfoRefBox prevMachineState = machineInfoRefBoxDao.findByName(m_info.getName());
            String prevState = "";
            if (prevMachineState != null) {
                m_info.setPreviousState(prevMachineState.getPreviousState());
                prevState = prevMachineState.getState();
            }

            if (!prevState.equalsIgnoreCase(m_info.getState())) {
                m_info.setPreviousState(prevState);
                if ("BROKEN".equalsIgnoreCase(m_info.getState())) {
                    //todo what to do in that case?!?!?!
                    log.warn("Machine broken: " + m_info.getName());
                }
            }
            machineInfoRefBoxDao.save(m_info);
            // duplicate machine for robot updates
            MachineInfoRefBox m_info_mirrored = new MachineInfoRefBox();
            ExplorationZone zone = gameField.getZoneByName(m_info.getZone());

            if (zone == null) {
                log.warn("Could not find zone " + m_info.getZone() + " in gamefield!!!");
                return;
            }

            ExplorationZone mirrored_zone = zone.getMirroredZone();
            m_info_mirrored.setZone(mirrored_zone.getZoneName());
            String machineName = m_info.getName();
            if (machineName.startsWith("C-")) {
                machineName = machineName.replaceFirst("C-", "M-");
            } else {
                machineName = machineName.replaceFirst("M-", "C-");
            }
            m_info_mirrored.setName(machineName);

            long zoneNumber = gameField.getZoneByName(m_info.getZone()).getZoneNumber();
            int xPos = (int) (zoneNumber / 10L);
            int yPos = (int) (zoneNumber % 10);
            int rotation = gameField.rotateMachineIfNecessary(xPos, yPos, new MachineName(m_info.getName()), m_info.getRotation());
            m_info_mirrored.setRotation(rotation);
            m_info_mirrored.setRing1(m_info.getRing1());
            m_info_mirrored.setRing2(m_info.getRing2());
            m_info_mirrored.setCorrectlyReported(m_info.getCorrectlyReported());
            m_info_mirrored.setExplorationTypeState(m_info.getExplorationTypeState());
            m_info_mirrored.setExplorationZoneState(m_info.getExplorationTypeState());
            m_info_mirrored.setState(m_info.getState());
            m_info_mirrored.setTeamColor(machineName.startsWith("C-") ? TeamColor.CYAN : TeamColor.MAGENTA);
            m_info_mirrored.setType(m_info.getType());
            m_info_mirrored.setPreviousState(m_info.getPreviousState());

            machines.add(m_info);
            machines.add(m_info_mirrored);
            machineInfoRefBoxDao.save(m_info_mirrored);

            pathEstimator.addMachine(m_info.getName(), zone.getZoneCenter().getX(), zone.getZoneCenter().getY(), m_info.getRotation());
            pathEstimator.addMachine(m_info_mirrored.getName(), mirrored_zone.getZoneCenter().getX(), mirrored_zone.getZoneCenter().getY(), m_info_mirrored.getRotation());

            //System.out.println("\n\n\n");
            //double l1 = pathEstimator.pathLength("M-CS1", SubProductionTask.MachineSide.OUTPUT, "M-RS1", SubProductionTask.MachineSide.INPUT);
            //double l2 = pathEstimator.pathLength("M-CS1", SubProductionTask.MachineSide.OUTPUT, "M-RS2", SubProductionTask.MachineSide.INPUT);
            //System.out.println("Pathlenghts: RS1: " + l1 + " RS2: " + l2);

            if (m_info.getState().equalsIgnoreCase("BROKEN")) {
                // a machine is broken, we need to fail any associated task
                // productionScheduler.machineBroken(m_info.getName()); todo may add to IScheduler?
            }
        }

        if (machines.size() == 14 && machineInfo.hasTeamColor()) {
            String teamPrefix = (machineInfo.getTeamColor().equals(TeamProtos.Team.CYAN)) ? "C" : "M";
            gameField.updateHalfFieldOccupancy(machines, teamPrefix);
        }

        if (machineClient.fetchPrepareMessages().size() > 0) {
            log.info("Going to send " + machineClient.fetchPrepareMessages().size() + " prepare machine messages!");
        }
        if (machineClient.fetchResetMessages().size() > 0) {
            log.info("Going to send " + machineClient.fetchResetMessages().size() + " reset machine messages!");
        }
        machineClient.fetchPrepareMessages().forEach(rbcm::sendPrivateMsg);
        machineClient.fetchResetMessages().forEach(rbcm::sendPrivateMsg);
    }
}
