package com.grips.scheduler.production;

import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.dao.SubProductionTaskDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskAssigner {
    @Autowired
    private SubProductionTaskDao subProductionTaskDao;

    public SubProductionTask assignTask(SubProductionTask task, int robotId) {

        task.setRobotId(robotId);
        task.setState(SubProductionTask.TaskState.INWORK, "set to INWORK due to assigning task to robot");
        subProductionTaskDao.save(task);

        SubProductionTask sameRobotTask = subProductionTaskDao.findBySameRobotSubTask(task);
        while (sameRobotTask != null) {
            sameRobotTask.setRobotId(robotId);
            sameRobotTask.setState(SubProductionTask.TaskState.ASSIGNED, "set subsequent DELIVER task to ASSIGNED");
            subProductionTaskDao.save(sameRobotTask);

            sameRobotTask = subProductionTaskDao.findBySameRobotSubTask(sameRobotTask);
        }

        return task;
    }
}
