package com.grips.scheduler.production;

import static com.grips.persistence.domain.SubProductionTask.TaskState.TBD;

import com.grips.config.ProductionConfig;
import com.grips.persistence.domain.ProductTask;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.Ring;
import com.grips.persistence.dao.ProductTaskDao;
import com.grips.persistence.dao.RingDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.config.Config;
import com.grips.scheduler.production.resouce_managment.ResourceDemandService;
import com.grips.team_server.SubTaskGenerator;
import com.shared.domain.CapColor;
import com.shared.domain.Complexity;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log
@Service
public class ProductStarter {
    @Autowired
    private ProductTaskDao productTaskDao;

    @Autowired
    private GameStateDao gameStateDao;

    @Autowired
    private ResourceDemandService resourceDemandService;

    @Autowired
    private RingDao ringDao;

    @Autowired
    private SubTaskGenerator subTaskGenerator;

    // all times in minutes
    @Value("${gameconfig.productiontimes.estimate_C0}")
    private int estimateC0;
    @Value("${gameconfig.productiontimes.estimate_C1}")
    private int estimateC1;
    @Value("${gameconfig.productiontimes.estimate_C2}")
    private int estimateC2;
    @Value("${gameconfig.productiontimes.estimate_C3}")
    private int estimateC3;

    @Autowired
    private ProductionConfig productionConfig;

    public void startNewProductIfFeasible(int robotId) {
        log.info("Starting a new Product");
        int countBlack = 0;
        int countGrey = 0;

        List<ProductTask> inwork = productTaskDao.findByStateAndProductOrderComplexityIn(ProductTask.ProductState.INWORK, Arrays.asList(Complexity.C0, Complexity.C1, Complexity.C2, Complexity.C3));
        int count = 0;
        for (ProductTask pTask : inwork) {
            if (pTask.getSubProductionTasks().stream().anyMatch(s ->
                    s.getState() == TBD
                            && s.getType() == SubProductionTask.TaskType.DELIVER
                            && s.getMachine() != null && s.getMachine().contains("DS"))) {
                count++;

                if (CapColor.CAP_GREY.equals(pTask.getProductOrder().getCapColor())) {
                    countGrey++;
                } else {
                    countBlack++;
                }
            }
        }

        // do not start more than 3 products, but do not consider products that already are delivered
        // there we only do cleanup demand tasks
        if (count >= productionConfig.getMaxSimultaneousProducts()) {
            log.info(count + " orders are already running,  not starting a new!");
            return;
        }

        // if there is a competitive order, start it immediately
        List<ProductTask> competitiveOrders = productTaskDao.findByStateAndProductOrderCompetitive(ProductTask.ProductState.TBD, true);
        if (competitiveOrders != null && competitiveOrders.size() > 0) {
            log.info("Trying to start competitive Order");
            boolean value = tryStartProduct(competitiveOrders.get(0), robotId);
            log.info("Result of competitiveOrder: " + value);
            return;
        }

        List<ProductOrder> notStartedProductOrders = getRankedNotStartedProducts(countGrey, countBlack);
        List<ProductTask> notStartedProducts = productTaskDao.findByState(ProductTask.ProductState.TBD);

        for (ProductOrder pOrder : notStartedProductOrders) {
            long estimatedProductDuration = estimateProductDuration(pOrder.getComplexity());

            // first look for earliest product for which the deadline has not already passed
            long currentGameTime = gameStateDao.getLatestGameTimeProductionPhase().longValue();
            long deliver = currentGameTime + (estimatedProductDuration * 60L * 1000000000L);
            if (deliver < pOrder.getDeliveryPeriodEnd() * 1000000000L) {
                // we found product with earliest deadline that is not infeasible
                Optional<ProductTask> pTask = notStartedProducts.stream().filter(n -> n.getProductOrder().getId() == pOrder.getId()).findAny();
                if (pTask.isPresent()) {
                    log.info("Trying to start finishable" + pTask.get().getProductOrder().getId());
                    if (tryStartProduct(pTask.get(), robotId)) {
                        log.info("Started " + pTask.get().getProductOrder().getId());
                        return;
                    }
                }
            }
        }

        // if no product with a not-passed deadline was found, do any product, but only when there are no others
        if (count > 1) {
            return;
        }

        Collections.shuffle(notStartedProducts);
        for (ProductOrder pOrder : notStartedProductOrders) {

            Optional<ProductTask> pTask = notStartedProducts.stream().filter(n -> n.getProductOrder().getId() == pOrder.getId()).findAny();
            if (pTask.isPresent()) {
                log.info("Trying to start not finishable" + pTask.get().getProductOrder().getId());
                if (tryStartProduct(pTask.get(), robotId)) {
                    log.info("Started " + pTask.get().getProductOrder().getId());
                    return;
                }
            }
        }
        log.warning("Not able to start a new product!");
    }

    private long estimateProductDuration(Complexity complexity) {
        switch (complexity) {
            case C0:
                return estimateC0;
            case C1:
                return estimateC1;
            case C2:
                return estimateC2;
            case C3:
                return estimateC3;
        }
        throw new IllegalArgumentException("Complexity not estimated: " + complexity);
    }

    public boolean tryStartProduct(ProductTask pTask, int robotId) {
        pTask.setState(ProductTask.ProductState.INWORK, "product started by " + robotId);
        productTaskDao.save(pTask);

        String colorprefix;
        try {
            colorprefix = gameStateDao.getTeamColor().equals(Config.TEAM_CYAN_COLOR) ? "C-" : "M-";
        } catch (Exception e) {
            System.out.println("Team color undefined! Setting default to CYAN!");
            colorprefix = "C-";
        }

        String cs = pTask.getProductOrder().getCapColor() == CapColor.CAP_GREY ? productionConfig.getGrey_cap_machine() : productionConfig.getBlack_cap_machine();
        resourceDemandService.addNewDemand(colorprefix + cs, 1, pTask);

        if (pTask.getProductOrder().getRing1() != null) {
            Ring r1 = ringDao.findByRingColor(pTask.getProductOrder().getRing1());
            if (r1.getRawMaterial() > 0) {
                resourceDemandService.addNewDemand(subTaskGenerator.getRingStationForColor(r1.getRingColor()), r1.getRawMaterial(), pTask);
            }
        }

        if (pTask.getProductOrder().getRing2() != null) {
            Ring r2 = ringDao.findByRingColor(pTask.getProductOrder().getRing2());
            if (r2.getRawMaterial() > 0) {
                resourceDemandService.addNewDemand(subTaskGenerator.getRingStationForColor(r2.getRingColor()), r2.getRawMaterial(), pTask);
            }
        }

        if (pTask.getProductOrder().getRing3() != null) {
            Ring r3 = ringDao.findByRingColor(pTask.getProductOrder().getRing3());
            if (r3.getRawMaterial() > 0) {
                resourceDemandService.addNewDemand(subTaskGenerator.getRingStationForColor(r3.getRingColor()), r3.getRawMaterial(), pTask);
            }
        }

        return true;
    }

    private List<ProductOrder> getRankedNotStartedProducts(int countGrey, int countBlack) {

        // the currently inwork products do not have parallel tasks that can be done
        // or no product currently in work, start a new one
        // we prefer products with simpler complexity
        List<ProductTask> notStartedProducts = productTaskDao.findByState(ProductTask.ProductState.TBD);

        Comparator<ProductOrder> capColorComparator;
        if (countGrey > countBlack) {
            capColorComparator = Comparator.comparing(ProductOrder::getCapColor).thenComparing(ProductOrder::getDeliveryPeriodEnd);
        } else {
            capColorComparator = Comparator.comparing(ProductOrder::getCapColor).reversed().thenComparing(ProductOrder::getDeliveryPeriodEnd);
        }

        // earliest deadline first, that is not passed
        List<ProductOrder> notStartedProductOrders = notStartedProducts.stream()
                .map(ProductTask::getProductOrder)
                .filter(pOrder -> pOrder.getComplexity() == Complexity.C0 && productionConfig.isDoC0()
                        || pOrder.getComplexity() == Complexity.C1 && productionConfig.isDoC1()
                        || pOrder.getComplexity() == Complexity.C2 && productionConfig.isDoC2()
                        || pOrder.getComplexity() == Complexity.C3 && productionConfig.isDoC3())
                .sorted(capColorComparator).collect(Collectors.toList());

        return notStartedProductOrders;
    }
}
