package com.grips.scheduler.production;

import com.grips.model.teamserver.TeamColor;
import com.grips.persistence.domain.ProductTask;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.dao.*;
import com.grips.config.Config;
import com.grips.scheduler.production.resouce_managment.ResourceDemandService;
import com.grips.scheduler.production.resouce_managment.ResourceLockService;
import com.grips.team_server.SubTaskGenerator;
import com.shared.domain.BaseColor;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.grips.persistence.domain.SubProductionTask.TaskState.*;
import static com.grips.scheduler.production.ProductionScheduler.READY_AT_OUTPUT;

@Service
@CommonsLog
public class DefaultTaskFinder {

    private final Object lock = new Object();
    @Autowired
    private ResourceDemandService resourceDemandService;
    @Autowired
    private ProductStarter productStarter;
    @Autowired
    private TaskAssigner taskAssigner;
    @Autowired
    private SubProductionTaskDao subProductionTaskDao;
    @Autowired
    private GameStateDao gameStateDao;
    @Autowired
    private ProductTaskDao productTaskDao;
    @Autowired
    private SubTaskGenerator subTaskGenerator;
    @Autowired
    private MachineInfoRefBoxDao machineInfoRefBoxDao;
    @Autowired
    private ProductOrderDao productOrderDao;
    @Autowired
    private SubProductionUtils subProductionUtils;
    //Step1: Find linked tasks for same robot
    //Step2: See if we need to do some cleanup
    //Step3: Schedule task
    //Step4: Demand tasks
    //Step5: New Product
    //Step6+7: In new product: either new task or demand task
    public SubProductionTask findSuitableTask(int robotId) {
        synchronized (lock) {
            // Step1: Find linked tasks for same robot
            SubProductionTask linkedTask = findSameRobotTask(robotId);
            if (linkedTask != null) {
                // if machine is in use by other robot, do circle around task in the meantime
                if (linkedTask.getType() == SubProductionTask.TaskType.DELIVER
                        && resourceDemandService.isMachineLockedByOtherRobot(linkedTask.getMachine(), linkedTask.getType(), robotId)) {
                    log.info("Robot " + robotId + " already has the product in gripper, so do a circle around task meanwhile");
                    return null;
                }

                // in this case, we need to check for early delivery and not look for further tasks if this is the case
                if (!needToWaitForDelivery(linkedTask)) {
                    return taskAssigner.assignTask(linkedTask, robotId);
                } else {
                    log.warn("will get assigned a dummy task, has product in gripper and gets deliver task assigned later");
                    return null; //
                }
            }

            // Step2: See if we need to do some cleanup
            SubProductionTask cTask = findCleanupTasks();
            if (cTask != null) {
                log.info("Assigned cleanup task to: " + robotId);
                return taskAssigner.assignTask(cTask, robotId);
            }

            // Step3: Schedule task (in already active products)
            SubProductionTask task = findNextTaskInActiveProduct(robotId);
            if (task != null) {
                log.info("Assigned task " + task.getId() + " in active product, robot: " + robotId);
                return taskAssigner.assignTask(task, robotId);
            }

            // Step4: See if resources demanded
            SubProductionTask resourceDemand = findDemandedResourceTask();
            if (resourceDemand != null) {
                log.info("Assigned demaned task " + resourceDemand.getId() + " to robot: " + robotId);
                return taskAssigner.assignTask(resourceDemand, robotId);
            }

            // sleep and check again if tasks can be done
            try {
                Thread.sleep(3000);
                log.info("SLEEPING 3 seconds!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            // Step3a: Schedule task (in already active products)
            task = findNextTaskInActiveProduct(robotId);
            if (task != null) {
                log.info("Assigned task in active product 3A, robot: " + robotId);
                return taskAssigner.assignTask(task, robotId);
            }

            // Step4a: See if resources demanded
            resourceDemand = findDemandedResourceTask();
            if (resourceDemand != null) {
                log.info("Assigned demaned task 4A to robot: " + robotId);
                return taskAssigner.assignTask(resourceDemand, robotId);
            }

            // Step5: Start a new product (if possible)
            productStarter.startNewProductIfFeasible(robotId);

            // Step6: Schedule task (again)
            task = findNextTaskInActiveProduct(robotId);
            if (task != null) {
                log.info("Assigned task in active product 6, robot: " + robotId);
                return taskAssigner.assignTask(task, robotId);
            }

            // Step7: Demand task (again)
            resourceDemand = findDemandedResourceTask();
            if (resourceDemand != null) {
                log.info("Assigned demaned task 7 to robot: " + robotId);
                return taskAssigner.assignTask(resourceDemand, robotId);
            }

            log.warn("No task was found for robot: " + robotId);
            return null;
        }
    }

    private SubProductionTask findSameRobotTask(int robotId) {
        Collection<SubProductionTask> tasks = subProductionTaskDao.findByStateAndRobotId(SubProductionTask.TaskState.ASSIGNED, robotId);
        tasks = tasks.stream().filter(t -> ProductTask.ProductState.INWORK.equals(t.getProductTask().getState())).collect(Collectors.toList());
        if (tasks != null) {
            // find task with all pre-tasks done
            SubProductionTask readyToDo = null;
            for (SubProductionTask task : tasks) {
                boolean allDone = true;
                for (SubProductionTask pre : task.getPreConditionTasks()) {
                    if (pre.getState() != SubProductionTask.TaskState.SUCCESS) {
                        allDone = false;
                    }
                }

                if (allDone) {
                    readyToDo = task;
                }
            }

            if (readyToDo != null) {

                if (readyToDo.isBindDisposeRsOnAssignment()) {
                    // first find from which machine we got the product
                    Optional<SubProductionTask> oPickupFromCsTask = readyToDo.getPreConditionTasks().stream().filter(p -> p.getMachine().contains("CS") && p.getType() == SubProductionTask.TaskType.GET).findFirst();

                    if (oPickupFromCsTask.isPresent()) {
                        String colorprefix;
                        try {
                            colorprefix = gameStateDao.getTeamColor().equals(Config.TEAM_CYAN_COLOR) ? "C-" : "M-";
                        } catch (Exception e) {
                            log.info("Team color undefined! Setting default to CYAN!");
                            colorprefix = "C-";
                        }
                        String ringStationForCapDispose = subTaskGenerator.getRSForCapDispose(oPickupFromCsTask.get().getMachine());
                        if (ringStationForCapDispose == null) {
                            if (Math.random() >= 0.5) {
                                ringStationForCapDispose = colorprefix + "RS2";
                            } else {
                                ringStationForCapDispose = colorprefix + "RS1";
                            }
                        }

                        readyToDo.setMachine(ringStationForCapDispose);
                        readyToDo.setLockMachine(ringStationForCapDispose);
                        readyToDo.setUnlockMachine(ringStationForCapDispose);
                        readyToDo.setIncrementMachine(ringStationForCapDispose);
                        readyToDo.setSide(MachineSide.SLIDE);
                        subProductionTaskDao.save(readyToDo);
                    }
                }

                return readyToDo;
            }
        }

        return null;
    }

    private SubProductionTask findDemandedResourceTask() {

        List<SubProductionTask> subProductionTasks = resourceDemandService.generateDemandTasksIfRequired();

        if (subProductionTasks != null && subProductionTasks.size() > 0) {
            subProductionTaskDao.saveAll(subProductionTasks);
            return subProductionTasks.get(0);
        } else {
            return null;
        }
    }

    private SubProductionTask findCleanupTasks() {
        String teamColor = gameStateDao.getTeamColor();
        TeamColor tColor = TeamColor.CYAN;
        if (teamColor.startsWith("M")) {
            tColor = TeamColor.MAGENTA;
        }

        List<MachineInfoRefBox> machines = machineInfoRefBoxDao.findByTeamColor(tColor);
        List<MachineInfoRefBox> readyAtOutputMachines = machines.stream().filter(m -> READY_AT_OUTPUT.equals(m.getState())).collect(Collectors.toList());

        for (MachineInfoRefBox m : readyAtOutputMachines) {
            // method does not work for BS since we do not know if input or output
            if (m.getName().contains("BS")) {
                continue;
            }

            // see if no task can be done for the ready-at-output state machine
            Collection<SubProductionTask> tasksForMachine = subProductionTaskDao.findByMachineAndSideInAndStateIn(m.getName(), Arrays.asList(MachineSide.OUTPUT), Arrays.asList(INWORK, ASSIGNED, TBD));

            boolean readyTasksExist = false;
            for (SubProductionTask t : tasksForMachine) {
                if (ProductTask.ProductState.INWORK.equals(t.getProductTask().getState())
                        && t.getPreConditionTasks().stream().allMatch(p -> (p.getState() == SUCCESS || p.getState() == SUCCESS_PENDING))) {

                    readyTasksExist = true;
                }
            }

            if (!readyTasksExist) {
                ProductOrder pOrder = new ProductOrder();
                pOrder.setId(System.currentTimeMillis());
                pOrder.setBaseColor(BaseColor.BASE_BLACK);
                ProductTask pTask = new ProductTask();
                pTask.setState(ProductTask.ProductState.INWORK, "started cleanup task");
                pTask.setProductOrder(pOrder);
                List<SubProductionTask> subProductionTasks = subTaskGenerator.prepareRing(pOrder, m.getName(), subTaskGenerator.getRSForCapDispose(m.getName()), MachineSide.OUTPUT, false);
                subProductionTasks.stream().forEach(s -> s.setProductTask(pTask));

                productOrderDao.save(pOrder);
                productTaskDao.save(pTask);
                subProductionTaskDao.saveAll(subProductionTasks);

                return subProductionTasks.get(0);
            }
        }

        return null;
    }

    private SubProductionTask findNextTaskInActiveProduct(int robotId) {
        // check first if there is a task left for an unfinished product
        List<ProductTask> productsInWork = productTaskDao.findByState(ProductTask.ProductState.INWORK);
        return findNextTaskInActiveProductTask(productsInWork, robotId);
    }

    private boolean needToWaitForDelivery(SubProductionTask readyToDo) {
        // check if we need to wait for delivery
        if (readyToDo.getMachine() != null && readyToDo.getMachine().contains("DS")) {
            // check if we already can deliver, delivery period begin is specified in s, whereas gametime is specified in ns
            if (gameStateDao.getLatestGameTimeProductionPhase().longValue() < readyToDo.getProductTask().getProductOrder().getDeliveryPeriodBegin() * 1000L * 1000L * 1000L) {
                // we need to wait for the deliveryperiod to begin
                log.warn("Wait for deliveryperiod to begin for delivering product");
                return true;
            } else {
                log.info("Deliveryperiod already started, so we can deliver the product");
            }
        }

        return false;
    }

    private SubProductionTask findNextTaskInActiveProductTask(List<ProductTask> productsToCheck, int robotId) {
        if (productsToCheck.size() > 0) {
            // we have a product to finish
            List<SubProductionTask> tasksToDo = subProductionTaskDao.findByProductTaskInAndState(productsToCheck, TBD);
            tasksToDo = tasksToDo.stream().filter(t -> !resourceDemandService.isMachineLockedByOtherRobot(t.getLockMachine(), t.getType(), robotId)).collect(Collectors.toList());


            // if a task is still in SUCCESS_PENDING, it is done for the robot and we are waiting for the machine state
            // to determine the actual result. the GET task can already be given to the robot if there is no other product at the output
            // if there is more than one predecessor, this will not work!
            List<SubProductionTask> includeSuccessPending = tasksToDo.stream().filter(t -> t.getPreConditionTasks() != null
                    && t.getPreConditionTasks().size() == 1
                    && t.getPreConditionTasks().stream().allMatch(p -> SUCCESS_PENDING.equals(p.getState()))).collect(Collectors.toList());
            if (includeSuccessPending != null && includeSuccessPending.size() > 0) {
                ArrayList<SubProductionTask> maybeToDo = new ArrayList<>();
                for (SubProductionTask t : includeSuccessPending) {
                    Collection<SubProductionTask> otherGetTasks = subProductionTaskDao.findByMachineAndSideInAndStateIn(t.getMachine(), Arrays.asList(t.getSide()), Arrays.asList(TBD, INWORK));
                    long otherReadyCount = otherGetTasks.stream().filter(o -> o.getPreConditionTasks().stream().allMatch(p -> SUCCESS.equals(p.getState()))).count();
                    if (otherReadyCount == 0) {
                        maybeToDo.add(t);
                    }
                }

                SubProductionTask foundTask = findHighestPriorityTaskAllResourcesAvailableAndPredecessorsDone(maybeToDo);
                if (foundTask != null) {
                    return foundTask;
                }
            }


            // see if there is a task without any precondition still not done
            List<SubProductionTask> parentsToDo = tasksToDo.stream().filter(t -> (t.getPreConditionTasks() == null || t.getPreConditionTasks().size() == 0)
                    && (TBD.equals(t.getState()))).collect(Collectors.toList());

            SubProductionTask foundTask = findHighestPriorityTaskAllResourcesAvailableAndPredecessorsDone(parentsToDo);
            if (foundTask != null) {
                return foundTask;
            }

            // all parents appear to be done, select the next best task with all preconditions done
            // find task with all preconditions met, that is not done yet
            List<SubProductionTask> childsToDo = tasksToDo.stream().filter(t -> (TBD.equals(t.getState()) &&
                    (t.getPreConditionTasks().stream().filter(p -> (p.getState() != SubProductionTask.TaskState.SUCCESS)).count() == 0)))
                    .collect(Collectors.toList());

            foundTask = findHighestPriorityTaskAllResourcesAvailableAndPredecessorsDone(childsToDo);

            if (foundTask != null) {
                return foundTask;
            }
        }

        return null;
    }

    private SubProductionTask findHighestPriorityTaskAllResourcesAvailableAndPredecessorsDone(List<SubProductionTask> tasks) {
        // sort in decreasing order of priority
        Comparator<SubProductionTask> taskPriorityComp = Comparator.comparingInt(SubProductionTask::getPriority);
        Collections.sort(tasks, taskPriorityComp);
        Collections.reverse(tasks);

        for (SubProductionTask t : tasks) {
            SubProductionTask successorTask = subProductionUtils.getSuccessorDeliverTask(t, TBD);
            if (successorTask == null) continue;
            if (successorTask.getDecrementMachine() == null || resourceDemandService.tryLockParts(successorTask.getDecrementMachine(), successorTask.getDecrementCost(), successorTask.getProductTask())) {
                // there are enough resources for this task to be fulfilled
                log.info("Task: " + successorTask.getId() + "has all resourcesDemands fullfilled, assigning it!");
                return t;
            } else {
                if (ProductTask.ProductState.INWORK.equals(t.getProductTask().getState())) {
                    log.info("No resources for Task: " + successorTask.getName() + ", required: " + successorTask.getDecrementCost());
                    // check if there are enough demands
                    resourceDemandService.ensureEnoughDemands(successorTask.getDecrementMachine(), successorTask.getDecrementCost(), successorTask.getProductTask());
                }
            }
        }
        return null;
    }
}
