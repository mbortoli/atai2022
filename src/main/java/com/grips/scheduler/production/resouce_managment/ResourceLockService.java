package com.grips.scheduler.production.resouce_managment;

import com.grips.persistence.dao.LockPartDao;
import com.grips.persistence.domain.LockPart;
import com.grips.persistence.domain.ProductTask;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@CommonsLog
public class ResourceLockService {

    private final LockPartDao lockPartDao;

    public ResourceLockService(LockPartDao lockPartDao) {
        this.lockPartDao = lockPartDao;
    }

    @Transactional
    public void clearAllParts(String machine) {
        log.info("clearAllParts machine is: " + machine);
        lockPartDao.deleteByMachine(machine);
    }

    @Transactional
    public void addNewParts(String machine) {
        log.info("Increment machine is: " + machine);
        LockPart lockPart = new LockPart();
        lockPart.setMachine(machine);
        lockPartDao.save(lockPart);
    }

    @Transactional
    public void addNewPartAndLock(String machine, Long productId) {
        LockPart lockPart = new LockPart();
        lockPart.lock(productId);
        lockPart.setMachine(machine);
        lockPartDao.save(lockPart);
    }

    @Transactional
    public boolean tryLockPartsNoDemandClear(String machine, int count, ProductTask productTask) {
        List<LockPart> potentialParts = lockPartDao.findByMachine(machine);
        if (potentialParts == null || potentialParts.size() == 0) {
            return false;
        }

        if (potentialParts.stream().filter(lp -> !lp.isLocked()).count() < count) {
            /*
            //System.err.println("Not enough unlocked material for machine " + machine + " exists");
            int required = count - new Long(potentialParts.stream().filter(lp -> !lp.isLocked()).count()).intValue();

            // we may need to add demands if there are no more
            int existingDemands = partDemandDao.countByProductTask(productTask);

            int requiredDemands = required - existingDemands;

            if (requiredDemands > 0) {
                addNewDemand(machine, requiredDemands, productTask);
            }
            */
            return false;
        } else {
            for (int i = 0; i < count; ++i) {
                Optional<LockPart> firstUnlocked = potentialParts.stream().filter(lp -> !lp.isLocked()).findFirst();
                if (!firstUnlocked.isPresent()) {
                    //System.err.println("No unlocked material for machine " + machine + " exists!");
                    return false;
                }

                firstUnlocked.get().lock(productTask.getProductOrder().getId());
                lockPartDao.save(firstUnlocked.get());
            }
        }
        return true;
    }

    @Transactional
    public boolean tryDecreaseParts(String machine, int count, Long productId) {
        log.info("Decrement machine is: " + machine);
        List<LockPart> machineParts = lockPartDao.findByMachine(machine);
        List<LockPart> potentialParts = machineParts.stream().filter(lp -> lp.isLocked() && lp.getProductId().equals(productId)).collect(Collectors.toList());
        if (potentialParts.size() < count) {
            log.error("Not enough locked material for Machine: " + machine + " exists");
            return false;
        } else {
            for (int i = 0; i < count; ++i) {
                lockPartDao.delete(potentialParts.get(i));
            }
        }

        return true;
    }


    @Transactional
    public void releasePartsForProduct(ProductTask pTask) {
        log.info("releasePartsForProduct ProductTaskId: " + pTask.getId() + " OrderId: " + pTask.getProductOrder().getId());
        List<LockPart> byProductId = lockPartDao.findByProductId(pTask.getProductOrder().getId());
        byProductId.stream().filter(lp -> lp.getProductId() != null && lp.getProductId().equals(pTask.getProductOrder().getId())).forEach(lp -> lp.unlock());
        lockPartDao.saveAll(byProductId);
    }

}
