package com.grips.scheduler.production.resouce_managment;

import com.grips.config.Config;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.dao.PartDemandDao;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.persistence.domain.PartDemand;
import com.grips.persistence.domain.ProductTask;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.scheduler.api.DbService;
import com.grips.team_server.SubTaskGenerator;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@CommonsLog
public class ResourceDemandService {

    private final PartDemandDao partDemandDao;
    private final SubProductionTaskDao subProductionTaskDao;
    private final DbService dbService;
    private final SubTaskGenerator subTaskGenerator;
    private final MachineInfoRefBoxDao machineInfoRefboxDao;
    private final ResourceLockService resourceLockService;

    public ResourceDemandService(PartDemandDao partDemandDao,
                                 SubProductionTaskDao subProductionTaskDao,
                                 DbService dbService,
                                 SubTaskGenerator subTaskGenerator,
                                 MachineInfoRefBoxDao machineInfoRefBoxDao,
                                 ResourceLockService resourceLockService) {
        this.partDemandDao = partDemandDao;
        this.subProductionTaskDao = subProductionTaskDao;
        this.dbService = dbService;
        this.subTaskGenerator = subTaskGenerator;
        this.machineInfoRefboxDao = machineInfoRefBoxDao;
        this.resourceLockService = resourceLockService;
    }

    @Transactional
    public void clearDemands(ProductTask productTask) {
        log.info("clearDemands ProductTaskId: " + productTask.getId() + " OrderId: " + productTask.getProductOrder().getId());
        partDemandDao.deleteByProductTask(productTask);
    }

    @Transactional
    public void clearDemands(String machine, ProductTask pTask) {
        log.info("clearDemands for machine: " + machine + " ProductTaskId: " + pTask.getId() + " OrderId: " + pTask.getProductOrder().getId());
        partDemandDao.deleteByMachineAndProductTask(machine, pTask);
    }

    public boolean isDemandTaskAlreadyInWork(String machine, Collection<MachineSide> machineSide) {
        Collection<SubProductionTask> t = subProductionTaskDao.findByMachineAndSideInAndStateIn(machine, machineSide,
                Arrays.asList(SubProductionTask.TaskState.INWORK, SubProductionTask.TaskState.SUCCESS_PENDING, SubProductionTask.TaskState.ASSIGNED));

        long tCount = t.stream().filter(p -> ProductTask.ProductState.INWORK.equals(p.getProductTask().getState())).count();

        if (tCount == 0) {
            return false;
        } else {
            return true;
        }
    }

    public List<SubProductionTask> generateDemandTasksIfRequired() {
        Iterable<PartDemand> csDemands = partDemandDao.findByTaskIsNullAndMachineContainsOrderByIdAsc("CS");
        Iterable<PartDemand> rsDemands = partDemandDao.findByTaskIsNullAndMachineContainsOrderByIdAsc("RS");

        for (PartDemand pDemand : csDemands) {

            if (isDemandTaskAlreadyInWork(pDemand.getMachine(), Arrays.asList(MachineSide.SHELF, MachineSide.INPUT))) {
                continue;
            }

            int removeWastePriority = 0;
            switch (pDemand.getProductTask().getProductOrder().getComplexity()) {
                case C0:
                    removeWastePriority = Config.PRIORITY_MOUNT_CAP_C0;
                    break;
                case C1:
                    removeWastePriority = Config.PRIORITY_MOUNT_CAP_C1;
                    break;
                case C2:
                    removeWastePriority = Config.PRIORITY_MOUNT_CAP_C2;
                    break;
                case C3:
                    removeWastePriority = Config.PRIORITY_MOUNT_CAP_C3;
                    break;
                default:
                    log.info("Could not determine product complexity");
            }

            if (dbService.getMaterialCount(pDemand.getMachine()) == 0) {
                // see if there is also a demand for a ringstation, if so, we already can assign this
                if (rsDemands != null && rsDemands.iterator().hasNext()) {
                    List<SubProductionTask> subProductionTasks;
                    PartDemand rsDemand = rsDemands.iterator().next();
                    subProductionTasks = subTaskGenerator.prepareCapAssignDispose(pDemand.getProductTask().getProductOrder(), pDemand.getMachine(), rsDemand.getMachine(), false, MachineSide.SLIDE);
                    subProductionTasks.forEach(s -> s.setProductTask(pDemand.getProductTask()));

                    subProductionTasks.get(2).setPriority(removeWastePriority);
                    subProductionTaskDao.saveAll(subProductionTasks);

                    pDemand.setTask(subProductionTasks.get(1));
                    rsDemand.setTask(subProductionTasks.get(3));

                    partDemandDao.save(pDemand);
                    partDemandDao.save(rsDemand);

                    return subProductionTasks;
                } else {
                    // assign later
                    List<SubProductionTask> subProductionTasks;
                    subProductionTasks = subTaskGenerator.prepareCapAssignDisposeLater(pDemand.getProductTask().getProductOrder(), pDemand.getMachine());
                    subProductionTasks.forEach(s -> s.setProductTask(pDemand.getProductTask()));

                    subProductionTasks.get(2).setPriority(removeWastePriority);
                    subProductionTaskDao.saveAll(subProductionTasks);

                    pDemand.setTask(subProductionTasks.get(1));

                    partDemandDao.save(pDemand);

                    return subProductionTasks;
                }
            }
        }

        for (PartDemand pDemand : rsDemands) {

            if (isDemandTaskAlreadyInWork(pDemand.getMachine(), Arrays.asList(MachineSide.SLIDE, MachineSide.INPUT))) {
                continue;
            }

            if (dbService.getMaterialCount(pDemand.getMachine()) < 3) {
                List<SubProductionTask> subProductionTasks;
                String colorPrefix = pDemand.getMachine().substring(0, 1);
                subProductionTasks = subTaskGenerator.prepareRing(pDemand.getProductTask().getProductOrder(), colorPrefix + "-" + "BS", pDemand.getMachine(), MachineSide.SLIDE, true);
                subProductionTasks.forEach(s -> s.setProductTask(pDemand.getProductTask()));
                subProductionTaskDao.saveAll(subProductionTasks);

                pDemand.setTask(subProductionTasks.get(1));

                partDemandDao.save(pDemand);

                return subProductionTasks;
            }
        }

        return null;
    }

    public void addNewDemand(String machine, int amount, ProductTask pTask) {
        log.info("Add new demand of " + amount + " parts for Machine: " + machine);

        for (int i = 0; i < amount; ++i) {
            PartDemand demand = new PartDemand();
            demand.setProductTask(pTask);
            demand.setMachine(machine);

            partDemandDao.save(demand);
        }
    }

    public void ensureEnoughDemands(String machine, int amount, ProductTask pTask) {
        List<PartDemand> byProductTask = partDemandDao.findByTaskIsNullAndProductTask(pTask);

        int toAdd = amount - byProductTask.size();
        if (toAdd > 0) {
            addNewDemand(machine, toAdd, pTask);
        }
    }

    @Transactional
    public void fulfillDemand(SubProductionTask task) {
        partDemandDao.deleteByTask(task);
    }

    @Transactional
    public void fulfillDemandByExplorationLoad(String machine) {
        List<PartDemand> byTaskIsNullAndMachine = partDemandDao.findByTaskIsNullAndMachine(machine);
        if (byTaskIsNullAndMachine != null && byTaskIsNullAndMachine.size() > 0) {
            partDemandDao.delete(byTaskIsNullAndMachine.get(0));
        }
    }


    public boolean isMachineLockedByOtherRobot(String machine, SubProductionTask.TaskType type, long robotId) {
        Collection<SubProductionTask> t = subProductionTaskDao.findByMachineAndTypeAndStateIn(machine, type,
                Arrays.asList(SubProductionTask.TaskState.INWORK, SubProductionTask.TaskState.SUCCESS_PENDING));

        if (t == null || t.size() == 0 || (t.stream().filter(s -> s.getRobotId() == robotId).count() > 0)) {
            return false;
        } else {
            List<Long> tasks = t.stream()
                    .filter(x -> x.getRobotId() != robotId)
                    .map(SubProductionTask::getId)
                    .collect(Collectors.toList());
            StringBuilder ids =  new StringBuilder();
            tasks.forEach(id -> ids.append(" ").append(id));

            log.warn("Machine: " + machine + " / " + machineInfoRefboxDao.findByName(machine).getState() +  " is locked by other Robot, Robot: " + robotId + " cant drive to it! Blocking tasks:" + ids);
            return true;
        }
    }

    public boolean tryLockParts(String machine, int count, ProductTask productTask) {
        if (count == 0) {
            return true;
        }
        if (resourceLockService.tryLockPartsNoDemandClear(machine, count, productTask)) {
            clearDemands(machine, productTask);
            return true;
        }
        return false;
    }

}
