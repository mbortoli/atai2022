package com.grips.scheduler.production;

import com.google.common.collect.Lists;
import com.grips.model.teamserver.MachineClient;
import com.grips.model.teamserver.MachineClientUtils;
import com.grips.model.teamserver.Peer;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.robot_communication.services.RobotClient;
import com.grips.robot.RobotClientWrapper;
import com.grips.scheduler.GameField;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.production.resouce_managment.ResourceDemandService;
import com.grips.scheduler.production.resouce_managment.ResourceLockService;
import com.grips.team_server.MPSHandler;
import com.shared.domain.Complexity;
import com.shared.domain.MachineSide;
import lombok.NonNull;
import lombok.Synchronized;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsBeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import static com.grips.persistence.domain.SubProductionTask.TaskState.*;

//import org.jetbrains.annotations.NotNull;

@CommonsLog
public class ProductionScheduler implements IScheduler {

    public static final String READY_AT_OUTPUT = "READY-AT-OUTPUT";
    public static final String IDLE = "IDLE";
    public static final String DOWN = "DOWN";
    public static final String BROKEN = "BROKEN";
    public static final String PREPARED = "PREPARED";
    public static final String PROCESSING = "PROCESSING";
    public static final String PROCESSED = "PROCESSED";
    @Value("${gameconfig.productiontimes.max_wait}")
    private int maxWait;

    private final RobotClient robotClient;
    private final ResourceDemandService resourceDemandService;
    private final SubProductionTaskDao subProductionTaskDao;
    private final ProductTaskDao productTaskDao;
    private final GameStateDao gameStateDao;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final MPSHandler mpsHandler;
    private final PartDemandDao partDemandDao;
    private final DefaultTaskFinder defaultTaskFinder;
    private final SubProductionUtils subProductionUtils;
    private final GameField gameField;
    private final MachineClient client;
    private final ResourceLockService resourceLockService;
    private final RobotClientWrapper robotClientWrapper;
    Map<Long, BigInteger> robotToAssignTimeNs;

    //private Semaphore _mutex = new Semaphore(1);
    private MachineSide bsSide = MachineSide.INPUT;

    public ProductionScheduler(RobotClient robotClient,
                               ResourceDemandService resourceDemandService,
                               SubProductionTaskDao subProductionTaskDao,
                               ProductTaskDao productTaskDao,
                               GameStateDao gameStateDao,
                               MachineInfoRefBoxDao machineInfoRefBoxDao,
                               MPSHandler mpsHandler,
                               PartDemandDao partDemandDao,
                               DefaultTaskFinder defaultTaskFinder,
                               SubProductionUtils subProductionUtils,
                               GameField gameField,
                               MachineClient client,
                               ResourceLockService resourceLockService,
                               RobotClientWrapper robotClientWrapper) {
        this.robotClient = robotClient;
        this.resourceDemandService = resourceDemandService;
        this.subProductionTaskDao = subProductionTaskDao;
        this.productTaskDao = productTaskDao;
        this.gameStateDao = gameStateDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.mpsHandler = mpsHandler;
        this.partDemandDao = partDemandDao;
        this.defaultTaskFinder = defaultTaskFinder;
        this.subProductionUtils = subProductionUtils;
        this.gameField = gameField;
        this.client = client;
        this.resourceLockService = resourceLockService;
        this.robotClientWrapper = robotClientWrapper;
        robotToAssignTimeNs = new HashMap<>();
    }

    public void failProduct(SubProductionTask subTask, String logging) {
        if (subTask.isDemandTask() || SubProductionTask.TaskType.DUMMY.equals(subTask.getType())) {
            return;
        }

        subTask.getProductTask().setState(ProductTask.ProductState.FAILED, logging);
        productTaskDao.save(subTask.getProductTask());

        resourceDemandService.clearDemands(subTask.getProductTask());
        resourceLockService.releasePartsForProduct(subTask.getProductTask());

        List<SubProductionTask> byProductTaskAndState = subProductionTaskDao.findByProductTaskAndState(subTask.getProductTask(), INWORK);
        for (SubProductionTask t : byProductTaskAndState) {
            robotClient.cancelTask(t.getRobotId());
        }
    }

    private List<SubProductionTask> getOtherReadyGetTasks(SubProductionTask subTask) {
        List<SubProductionTask> possibleOtherGets = subProductionTaskDao.findByTypeAndMachineAndStateIn(SubProductionTask.TaskType.GET, subTask.getMachine(), Arrays.asList(INWORK, ASSIGNED, TBD));
        possibleOtherGets = possibleOtherGets.stream().filter(p -> p.getId() != subTask.getId()).collect(Collectors.toList());

        List<SubProductionTask> otherGets = new ArrayList<>();
        boolean otherGetReady = false;
        // for this tasks, check if the predecessor was done
        if (possibleOtherGets != null && possibleOtherGets.size() > 0) {
            for (SubProductionTask get : possibleOtherGets) {
                boolean allPreDone = get.getPreConditionTasks().stream().allMatch(p -> SUCCESS.equals(p.getState()));
                if (allPreDone) {
                    otherGets.add(get);
                }
            }
        }

        return otherGets;
    }

    //@NotNull
    private String getCurrentMachineState(SubProductionTask subTask) {
        MachineInfoRefBox mInfoRb = machineInfoRefBoxDao.findByName(subTask.getMachine());
        String machineState = IDLE;
        if (mInfoRb != null) {
            // if we are still in exploration, so the state is IDLE for sure
            machineState = mInfoRb.getState();
        }

        // wait with decision while unconclusive machine state
        int breakCounter = 0;
        while (!machineState.equalsIgnoreCase(IDLE)
                && !machineState.equalsIgnoreCase(READY_AT_OUTPUT)
                && !machineState.equalsIgnoreCase("BROKEN")
                && breakCounter < 100) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                log.error("Sleep was interrupted in failTaskHandler");
            }

            mInfoRb = machineInfoRefBoxDao.findByName(subTask.getMachine());

            if (mInfoRb != null) {
                // if we are still in exploration, so the state is IDLE for sure
                machineState = mInfoRb.getState();
            }

            if (mInfoRb != null && !"DOWN".equalsIgnoreCase(machineState)) {
                breakCounter++;
            }

            log.info("Wait for machine " + subTask.getMachine() + " to become IDLE/READY-AT-OUTPUT/BROKEN");
        }
        return machineState;
    }

    private boolean handleDummyTask(SubProductionTask subTask) {
        if (subTask != null && subTask.getType() == SubProductionTask.TaskType.DUMMY) {
            subTask.setState(FAILED, "dummy task FAILED");
            subProductionTaskDao.save(subTask);
            failProduct(subTask, "dummy task FAILED");
            productTaskDao.save(subTask.getProductTask());
            return true;
        }
        return false;
    }

    public SubProductionTask failPreviousRobotActivityAndAssignTask(int robotId) {

        // we got no game phase info from refbox yet
        if (gameStateDao.getLatestGameTimeProductionPhase() == null) {
            log.warn("No GamePhase yet, not assigning any task to robot: " + robotId);
            return null;
        }

        /*
        Collection<SubProductionTask> prevActiveTasks = subProductionTaskDao.findByStateAndRobotId(SubProductionTask.TaskState.INWORK, robotId);
	    if (prevActiveTasks != null && prevActiveTasks.size() > 0) {
	        for (SubProductionTask prevTask : prevActiveTasks) {
	            if (SubProductionTask.TaskType.GET.equals(prevTask.getType())) {
	                handleGetResult(prevTask.getId(), robotId, PrsTaskProtos.PrsTask.ExecutionResult.FAIL);
                } else if (SubProductionTask.TaskType.DELIVER.equals(prevTask.getType())) {
                    handleDeliverResult(prevTask.getId(), robotId, PrsTaskProtos.PrsTask.ExecutionResult.FAIL, false);
                }
            }
        }
        */

        SubProductionTask task = defaultTaskFinder.findSuitableTask(robotId);

        if (task != null && task.getMachine().contains("BS")) {
            bsSide = (bsSide == MachineSide.INPUT) ? MachineSide.OUTPUT : MachineSide.INPUT;
            task.setSide(bsSide);
            subProductionTaskDao.save(task);
        }

        return task;
    }

    public void handleGetResult(long taskId, int robotId, boolean wasSuccess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            log.error("Error, task that should fail was not found!");
            return;
        }

        if (wasSuccess) {
            subTask.setState(SUCCESS, "robot said SUCCESS");
            subProductionTaskDao.save(subTask);
            log.info("GET Task: " + taskId + " was finished by robot: " + robotId);
        } else {
            log.error("GET Task: " + taskId + " was failed by robot: " + robotId);
            if (subTask.getMachine().contains("BS")) {
                handleBsGetFail(subTask);
            } else if (subTask.getMachine().contains("CS") || subTask.getMachine().contains("RS")) {
                handleCsRsGetFail(subTask);
            } else {
                log.error("GET task at unexpected machine " + subTask.getMachine() + " taskId: " + taskId);
            }
        }
    }

    private void handleCsRsGetFail(SubProductionTask subTask) {
        if (MachineSide.SHELF.equals(subTask.getSide())) {
            clearTasksInDemand(subTask);
            failTaskAndSuccessor(subTask, "get at SHELF failed");
        } else if (MachineSide.OUTPUT.equals(subTask.getSide())) {
            String currentMachineState = getCurrentMachineState(subTask);

            if (READY_AT_OUTPUT.equalsIgnoreCase(currentMachineState)) {
                updateTaskAndSuccessorStates(subTask, TBD, "task failed but machine still ready-at-output");
            } else if (IDLE.equalsIgnoreCase(currentMachineState)) {

                failTaskAndSuccessor(subTask, "GET failed and machine is IDLE we probably pushed product in machine");
                Collection<SubProductionTask> successPendingAtInput = subProductionTaskDao.findByMachineAndSideInAndStateIn(subTask.getMachine(), Arrays.asList(MachineSide.INPUT), Arrays.asList(SUCCESS_PENDING));
                if (successPendingAtInput == null || successPendingAtInput.size() == 0) {
                    // there is no other product at input but we failed, so to be sure: reset mps (we do not impact other product at input)
                    client.sendResetMachine(MachineClientUtils.parseMachineWithColor(subTask.getMachine()));
                    mpsHandler.stopPreparingMachine(subTask);

                    //todo is this a smart IDEA to fail and reset count?
                    // empty machine parts
                    resourceLockService.clearAllParts(subTask.getMachine());
                }

                // clear demand or fail product in any case
                if (subTask.isDemandTask()) {
                    clearTasksInDemand(subTask);
                } else {
                    failProduct(subTask, "get product failed and product not at machine");
                }
            } else {
                log.error("Unexpected machinestate " + currentMachineState + " at machine " + subTask.getMachine());
            }
        } else {
            log.error("GET task FAILED for unexpected side " + subTask.getSide() + " at machine " + subTask.getMachine());
        }
    }

    private void handleBsGetFail(SubProductionTask subTask) {
        client.sendResetMachine(MachineClientUtils.parseMachineWithColor(subTask.getMachine()));
        mpsHandler.stopPreparingMachine(subTask);
        if (!subTask.isDemandTask()) {
            failTaskAndSuccessor(subTask, "get at BS failed");
            failProduct(subTask, "get at BS failed");
        } else {
            clearTasksInDemand(subTask);
            failTaskAndSuccessor(subTask, "get at BS failed");
        }
    }

    private void clearTasksInDemand(SubProductionTask subTask) {
        SubProductionTask taskToClear = subTask;
        if (SubProductionTask.TaskType.GET.equals(subTask.getType())) {
            taskToClear = subProductionUtils.getSuccessorDeliverTask(subTask);
        }

        if (taskToClear == null) {
            log.error("Task to clear was null!!! SubTask: " + subTask.getId());
        }

        PartDemand byTask = partDemandDao.findByTask(taskToClear);
        if (byTask != null) {
            byTask.setTask(null);
            partDemandDao.save(byTask);
        }

        // if a CS task fails, also the RS task needs to be reset
        Collection<SubProductionTask> tasks = subProductionTaskDao.findByStateAndPreConditionTasksIn(TBD, Arrays.asList(taskToClear));
        if (tasks != null && tasks.size() > 0) {
            SubProductionTask rsGet = tasks.iterator().next();
            if (rsGet != null) {
                rsGet.setState(FAILED, "demand task is FAILED");
                subProductionTaskDao.save(rsGet);
            }

            SubProductionTask rsDeliver = subProductionUtils.getSuccessorDeliverTask(rsGet, TBD);
            if (rsDeliver != null) {
                rsDeliver.setState(FAILED, "demand task is FAILED");
                subProductionTaskDao.save(rsDeliver);
            }

            PartDemand rsDemand = partDemandDao.findByTask(rsDeliver);

            if (rsDemand != null) {
                rsDemand.setTask(null);
                partDemandDao.save(rsDemand);
            }
        }
    }

    private void updateTaskAndSuccessorStates(SubProductionTask subTask, SubProductionTask.TaskState taskState, String logging) {
        subTask.setState(taskState, logging);
        subProductionTaskDao.save(subTask);
        SubProductionTask successor = subProductionUtils.getSuccessorDeliverTask(subTask, ASSIGNED);
        if (successor != null) {
            successor.setState(taskState, logging);
            subProductionTaskDao.save(successor);
        }

        SubProductionTask successorGet = getSuccessorGetTask(subTask, INWORK);
        if (successorGet != null && taskState == FAILED) {
            robotClient.cancelTask(successorGet.getRobotId());
            failTaskAndSuccessor(successorGet, logging);
        }
    }

    private SubProductionTask getSuccessorGetTask(SubProductionTask subTask, SubProductionTask.TaskState state) {
        Collection<SubProductionTask> successors = subProductionTaskDao.findByStateAndPreConditionTasksIn(state, Lists.newArrayList(subTask));
        Optional<SubProductionTask> foundSuccesor = successors.stream().filter(s -> s.getType() == SubProductionTask.TaskType.GET).findAny();
        if (foundSuccesor.isPresent()) {
            return foundSuccesor.get();
        }

        return null;
    }

    private void failTaskAndSuccessor(SubProductionTask subTask, String logging) {
        updateTaskAndSuccessorStates(subTask, FAILED, logging);
    }

    public void handleDeliverResult(long taskId, int robotId, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return;
        }

        if (wasSucess) {
            subTask.setState(SUCCESS_PENDING, "robot said task was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.error("Deliver Task: " + taskId + " was supposedly finished by robot: " + robotId);
            if (subTask.getMachine().contains("RS") && Complexity.E0.equals(subTask.getProductTask().getProductOrder().getComplexity())) {
                successProductTask(subTask);
            }

            // no prepare required
            if (subTask.getMachine().contains("RS") && MachineSide.SLIDE.equals(subTask.getSide())) {
                subTask.setState(SUCCESS, "deliver was done to slide");
                subProductionTaskDao.save(subTask);
                handleResourcesOnSuccess(subTask);
            }
        } else {
            log.error("Task Failed result: " + wasSucess + " for task: " + taskId);
        }
    }

    private void successProductTask(SubProductionTask subTask) {
        resourceDemandService.clearDemands(subTask.getProductTask());
        subTask.getProductTask().setState(ProductTask.ProductState.FINISHED, "deliver was done");
        productTaskDao.save(subTask.getProductTask());
    }

    public void handleResourcesOnSuccess(SubProductionTask subTask) {
        if (!StringUtils.isEmpty(subTask.getIncrementMachine())) {
            resourceLockService.addNewParts(subTask.getIncrementMachine());
        }

        if (subTask.getDecrementCost() > 0) {
            resourceLockService.tryDecreaseParts(subTask.getDecrementMachine(), subTask.getDecrementCost(), subTask.getProductTask().getProductOrder().getId());
        }

        /*
        if (subTask.isDemandTaskWithOutDemand()) {
            resourceDemandService.fulfillDemandByExplorationLoad(subTask.getMachine());
        } else {
            resourceDemandService.fulfillDemand(subTask);
        }
        */
    }

    public void failOtherInWorkTasks(int robotId, long taskId) {
        List<SubProductionTask> robotTaks = subProductionTaskDao.findByRobotIdAndState(robotId, INWORK);
        for (SubProductionTask t : robotTaks) {
            if (t.getId() != taskId) {
                log.error("Robot " + robotId + " is already working on Task: " + taskId + ", thus we fail Task: " + t.getId());
                failTaskAndSuccessor(t, "Robot already is working on another task with ID: " + taskId);
            }
        }
    }

    @Synchronized
    @Override
    public void handleBeaconSignal(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, @NonNull Peer robot) {
        SubProductionTask currentTask = null;
        if (beacon_signal.getTaskId() != -1) {
            currentTask = subProductionTaskDao.findById((long) beacon_signal.getTaskId()).orElse(null);
        }

        if (robotToAssignTimeNs.containsKey((long) robotId)) {
            BigInteger diff = gameStateDao.getLatestGameTimeProductionPhase().subtract(robotToAssignTimeNs.get((long) robotId));
            //todo use check where we wait for 1 beacon signal with last task id!
            if (diff.compareTo(new BigInteger("4000000000")) < 0) {
                log.warn("Recently assigned task to Robot: " + robotId + " not assigning a new one!");
                return;
            }
        }

        if (beacon_signal.getTaskId() == -1 || (currentTask != null && currentTask.getProductTask().getProductOrder().getComplexity() == Complexity.D0)) {
            // robot has no current task

            //log.info("checking for robot " + robot.getId());

            //TODO: check if robot had a previous task, if so, cancel this task
            SubProductionTask task = this.failPreviousRobotActivityAndAssignTask(robotId);


            if (task != null) {
                if (SubProductionTask.TaskType.GET.equals(task.getType())) {
                    log.error("Giving robot " + robotId + " a GET task " + task.getName() + " although robot still has something in gripper");
                }
                if (SubProductionTask.TaskType.DELIVER.equals(task.getType())) {
                    log.error("Giving robot " + robotId + " a DELIVER task " + task.getName() + " although robot has nothing in its gripper");
                }

                robot.setWaiting(false);
                robot.setWaitingTime(0);
                robotToAssignTimeNs.put(robot.getId(), gameStateDao.getLatestGameTimeProductionPhase());
                robotClientWrapper.sendProductionTaskToRobot(task);
            } else {

                // we did not find a task for this robot, move it away if we cannot find one for 3 beacons
                if (!robot.isWaiting()) {
                    robot.setWaitingTime(robot.getWaitingTime() + 1);

                    if (robot.getWaitingTime() >= 3 && !robot.isWaiting()) {
                        robot.setWaiting(true);
                        ExplorationZone waitingZone = gameField.getWaitingZone(robotId);
                        if (waitingZone != null) {
                            log.warn("Sending robot " + robotId + " to waitingzone " + waitingZone.getZoneName());
                            robotClient.sendWaitingTaskToRobot(robot.getId(), waitingZone.getZoneName());
                        } else {
                            log.error("Failed to find a waiting zone!");
                        }
                    }
                }
            }
        } else {
            // robot has current task, do not assign a new one
            if (beacon_signal.getTaskId() != -1) {
                this.failOtherInWorkTasks(robotId, beacon_signal.getTaskId());
            }
        }

    }

    @Synchronized
    @Override
    public void handleRobotTaskResult(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask) {
        if (prsTask.hasGetFromStation()) {
            this.handleGetResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful());
        } else if (prsTask.hasDeliverToStation()) {
            this.handleDeliverResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful()); //todo check!
        /*} else if (prsTask.hasSendPrepareMachine()) {
            log.info("Received preparemachine Task from Robot " + prsTask.getRobotId());
            SubProductionTask productionTask = subProductionTaskDao.findByIdAndRobotId(prsTask.getTaskId(), prsTask.getRobotId());
            if (productionTask == null) {
                log.error("No task found for prepareMachine, DB inconsistency!");
            } else {
                mpsHandler.prepareMachine(productionTask);
            }
        }*/
        } else if (prsTask.hasMoveToWaypoint()) {
            this.handleMoveToWaypointResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getMoveToWaypoint(), prsTask.getSuccessful());
        } else if (prsTask.hasBufferCapStation()) {
            this.handleBufferCapStationResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getBufferCapStation(), prsTask.getSuccessful());
        } else {
            log.error("Unsupportet midlevelTaskResult: " + prsTask.toString());
        }
    }

    private void handleBufferCapStationResult(int taskId, int robotId, GripsMidlevelTasksProtos.BufferCapStation bufferCapStation, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return;
        }

        if (wasSucess) {
            subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.error("MoveTo Task: " + taskId + " was finished by robot: " + robotId);
        } else {
            log.error("MoveTo Task Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);
        }
    }

    private void handleMoveToWaypointResult(Integer taskId, Integer robotId, GripsMidlevelTasksProtos.MoveToWaypoint moveToWaypoint, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return;
        }

        if (wasSucess) {
            subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.error("MoveTo Task: " + taskId + " was finished by robot: " + robotId);
        } else {
            log.error("MoveTo Task Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);
        }
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        List<SubProductionTask> task = subProductionTaskDao.findByMachineAndState(machine.getName(), SubProductionTask.TaskState.SUCCESS_PENDING);
        for (SubProductionTask subProductionTask : task) {
            MachineClientUtils.MachineState state = client.getStateForMachine(MachineClientUtils.parseMachineWithColor(subProductionTask.getMachine())).get();
            if (state.equals(MachineClientUtils.MachineState.READY_AT_OUTPUT)) {
                subProductionTask.setState(SUCCESS);
                this.handleResourcesOnSuccess(subProductionTask);
                subProductionTaskDao.save(subProductionTask);
            }
        }
    }
}
