package com.grips.scheduler.challanges;

import com.grips.model.teamserver.Peer;
import com.grips.scheduler.api.IScheduler;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsBeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

@CommonsLog
public class ExplorationScheduler implements IScheduler {

    @Override
    public void handleBeaconSignal(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, Peer robot) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }

    @Override
    public void handleRobotTaskResult(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }
}
