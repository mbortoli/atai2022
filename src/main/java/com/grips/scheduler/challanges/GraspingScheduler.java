package com.grips.scheduler.challanges;

import com.grips.model.teamserver.Peer;
import com.robot_communication.services.RobotClient;
import com.grips.scheduler.api.IScheduler;
import com.robot_communication.services.PrsTaskCreator;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsBeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

import java.util.ArrayList;
import java.util.List;

@CommonsLog
public class GraspingScheduler implements IScheduler {

    private final PrsTaskCreator prsTaskCreator;
    private final RobotClient robotClient;

    private final String robot1Machine = "M-CS1";

    private List<GripsMidlevelTasksProtos.GripsMidlevelTasks> robot1_task;
    private int robot1_current_index;

    public GraspingScheduler(PrsTaskCreator prsTaskCreator, RobotClient robotClient) {
        this.prsTaskCreator = prsTaskCreator;
        this.robotClient = robotClient;
        //genreate fake product!
        robot1_task = new ArrayList<>();
        prsTaskCreator.createGetWorkPieceTaskNew(1L, 1L, robot1Machine, "output");
        prsTaskCreator.createDeliverWorkPieceTaskNew(1L, 2L, robot1Machine, "input");
        prsTaskCreator.createGetWorkPieceTaskNew(1L, 3L, robot1Machine, "output");
        prsTaskCreator.createDeliverWorkPieceTaskNew(1L, 4L, robot1Machine, "input");
        prsTaskCreator.createGetWorkPieceTaskNew(1L, 5L, robot1Machine, "output");
        prsTaskCreator.createDeliverWorkPieceTaskNew(1L, 6L, robot1Machine, "input");
        log.info("Created GraspingScheduler");
    }

    @Override
    public void handleBeaconSignal(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, Peer robot) {
        if (robotId == 1) {
            if (beacon_signal.getTaskId() != -1 == false) {
                robotClient.sendPrsTaskToRobot(robot1_task.get(robot1_current_index));
                robot1_current_index++;
            }
        }
    }

    @Override
    public void handleRobotTaskResult(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask) {

    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {

    }
}
