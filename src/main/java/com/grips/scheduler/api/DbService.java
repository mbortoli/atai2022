package com.grips.scheduler.api;

import com.google.common.collect.Lists;
import com.grips.persistence.dao.LockPartDao;
import com.grips.persistence.domain.Ring;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.dao.ProductTaskDao;
import com.grips.persistence.dao.RingDao;
import com.grips.config.Config;
import com.shared.domain.Complexity;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@CommonsLog
public class DbService {

    private final RingDao ringDao;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final ProductTaskDao productTaskDao;
    private final LockPartDao lockPartDao;

    public DbService(RingDao ringDao,
                     MachineInfoRefBoxDao machineInfoRefBoxDao,
                     ProductTaskDao productTaskDao,
                     LockPartDao lockPartDao) {
        this.ringDao = ringDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.productTaskDao = productTaskDao;
        this.lockPartDao = lockPartDao;
    }

    public boolean checkProductionReady() {

        Collection<Ring> allRings = Lists.newArrayList(ringDao.findAll());
        if (allRings.size() != Config.NUM_RING_COLORS) {
            return false;
        }

        for (Ring r : allRings) {
            if (machineInfoRefBoxDao.countByRing1OrRing2(r.getRingColor(), r.getRingColor()) == 0) {
                return false;
            }
        }

        return true;
    }

    public boolean hasOrders() {
        if (productTaskDao.countByProductOrderComplexity(Complexity.C0) > 0
                && productTaskDao.countByProductOrderComplexity(Complexity.C1) > 0
                && (productTaskDao.countByProductOrderComplexity(Complexity.C2) > 0
                || productTaskDao.countByProductOrderComplexity(Complexity.C3) > 0)) {
            return true;
        }

        return false;
    }

    public int getMaterialCount(String machine) {
        int returner = lockPartDao.countByMachine(machine);
        log.info("getMaterialCount for Machine: : " + machine + " is:" + returner);
        return returner;
    }
}
