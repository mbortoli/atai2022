package com.grips.scheduler.asp.util;

import com.grips.config.ProductionConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.Atom;
import com.grips.persistence.domain.ProductOrder;
import com.grips.scheduler.asp.encoding.CodedProblem;
import com.grips.scheduler.asp.encoding.IntOp;
import com.shared.domain.RingColor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class KnowledgeBase {

    private final AtomDao atomDao;
    private final ProductOrderDao productOrderDao;
    private final ProductTaskDao productTaskDao;
    private final GameStateDao gameStateDao;
    private final RingDao ringDao;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final SubProductionTaskDao subProductionTaskDao;
    private CodedProblem cp;
    private String [] oldRobotPosition;
    private Map<Integer, TempAtom> wastedProductStage;

    private Map<Integer, String> runningProductStage;

    @Autowired
    private ProductionConfig productionConfig;


    public  KnowledgeBase(AtomDao atomDao, ProductOrderDao productOrderDao, ProductTaskDao productTaskDao, GameStateDao gameStateDao, RingDao ringDao, MachineInfoRefBoxDao machineInfoRefBoxDao, SubProductionTaskDao subProductionTaskDao, ProductionConfig productionConfig) {
        this.atomDao = atomDao;
        this.productOrderDao = productOrderDao;
        this.productTaskDao = productTaskDao;
        this.gameStateDao = gameStateDao;
        this.ringDao = ringDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.subProductionTaskDao = subProductionTaskDao;
        this.productionConfig = productionConfig;
        oldRobotPosition = new String[3];
        oldRobotPosition[0] = "";
        oldRobotPosition[1] = "";
        oldRobotPosition[2] = "";

        wastedProductStage = new HashMap<Integer, TempAtom>();
        runningProductStage = new HashMap<Integer, String>();
    }


    public void setWastedProductStage(HashMap<Integer, TempAtom> wastedProductStage) {
        this.wastedProductStage.putAll(wastedProductStage);
    }

    public TempAtom getTempAtomFromWastedProductStage (int id) {
        return wastedProductStage.get(id);
    }

    public void generateInitialKB () {
        //generation of standard atom (robots, stations,  ecc...):
        atomDao.save(new Atom("at", "r1;start"));
        atomDao.save(new Atom("at", "r2;start"));
        atomDao.save(new Atom("at", "r3;start"));
        atomDao.save(new Atom("free", "r1"));
        atomDao.save(new Atom("free", "r2"));
        atomDao.save(new Atom("free", "r3"));
        atomDao.save(new Atom("empty", "bs"));
        atomDao.save(new Atom("empty", "cs1_input"));
        atomDao.save(new Atom("empty", "cs1_output"));
        atomDao.save(new Atom("empty", "cs1_shelf"));
        atomDao.save(new Atom("empty", "cs2_input"));
        atomDao.save(new Atom("empty", "cs2_output"));
        atomDao.save(new Atom("empty", "cs2_shelf"));
        atomDao.save(new Atom("empty", "ds"));
        atomDao.save(new Atom("empty", "rs1_input"));
        atomDao.save(new Atom("empty", "rs2_input"));
        atomDao.save(new Atom("empty", "rs1_output"));
        atomDao.save(new Atom("empty", "rs2_output"));
        atomDao.save(new Atom("n_holding", "r1"));
        atomDao.save(new Atom("n_holding", "r2"));
        atomDao.save(new Atom("n_holding", "r3"));

        //machine config
        atomDao.save(new Atom("stationcapcolor" , productionConfig.getBlack_cap_machine().toLowerCase(Locale.ROOT) + ";cap_black"));
        atomDao.save(new Atom("stationcapcolor" , productionConfig.getGrey_cap_machine().toLowerCase(Locale.ROOT) + ";cap_grey"));
        atomDao.save(new Atom("stationringcolor" , "rs1;" + machineInfoRefBoxDao.findByName("C-RS1").getRing1().toString().toLowerCase(Locale.ROOT)));
        atomDao.save(new Atom("stationringcolor" , "rs1;" + machineInfoRefBoxDao.findByName("C-RS1").getRing2().toString().toLowerCase(Locale.ROOT)));
        atomDao.save(new Atom("stationringcolor" , "rs2;" + machineInfoRefBoxDao.findByName("C-RS2").getRing1().toString().toLowerCase(Locale.ROOT)));
        atomDao.save(new Atom("stationringcolor" , "rs2;" + machineInfoRefBoxDao.findByName("C-RS2").getRing2().toString().toLowerCase(Locale.ROOT)));
        atomDao.save(new Atom("requiredbases" , "ring_yellow", ringDao.findByRingColor(RingColor.RING_YELLOW).getRawMaterial()));
        atomDao.save(new Atom("requiredbases" , "ring_orange", ringDao.findByRingColor(RingColor.RING_ORANGE).getRawMaterial()));
        atomDao.save(new Atom("requiredbases" , "ring_blue", ringDao.findByRingColor(RingColor.RING_BLUE).getRawMaterial()));
        atomDao.save(new Atom("requiredbases" , "ring_green", ringDao.findByRingColor(RingColor.RING_GREEN).getRawMaterial()));



    }

    public void generateOrderKb (List<ProductOrder> goals) {
        for (ProductOrder p : goals) {
            long id = p.getId();
            //manage the case in which the order was already present (replanning) (by doing nothing)
            if (atomDao.findByNameAndAttributes("complexity", "p" + id).isEmpty()) {
                atomDao.save(new Atom("stage_c0_0", "p" + id));
            }
        }
    }


    //THIS METHOD IS A SHOW-CASE AND CAN BE REPLACED/MODIFIED
    public void printKB () {
        for( Atom at : atomDao.findAll())
            System.out.println(at.toString());
    }


    public void prettyPrintKBtoProblemFile (FileWriter fr) throws IOException {
        for (Atom at : atomDao.findAll())
            if(!at.getName().equals("holdafter") )
                fr.write(at.prettyPrinter());
    }


    public void setCp (CodedProblem cp) {
        this.cp = cp;
    }


    public void purgeKB () {
        atomDao.deleteAll();
    }



    /**
     * check if end precondition of action are satisfied
     * @param action
     * @return
     */



}
