package com.grips.scheduler.asp.dispatcher;


import com.grips.config.Config;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.grips.scheduler.asp.encoding.CodedProblem;
import com.grips.scheduler.asp.encoding.IntOp;
import com.shared.domain.BaseColor;
import com.shared.domain.Complexity;
import com.shared.domain.MachineSide;
import com.shared.domain.RingColor;
import org.robocup_logistics.llsf_msgs.MachineDescriptionProtos;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class ActionMapping {

    CodedProblem cp;
    ProductOrderDao productOrderDao;
    RingDao ringDao;

    private static String TASK_STATE_BROKEN = "BROKEN";
    private static String TASK_STATE_IDLE = "IDLE";
    private static String TASK_STATE_READY_AT_OUTPUT = "READY-AT-OUTPUT";

    public ActionMapping(CodedProblem cp, ProductOrderDao productOrderDao, RingDao ringDao) {
        this.cp = cp;
        this.productOrderDao = productOrderDao;
        this.ringDao = ringDao;
    }

    public SubProductionTask actionToTask (IntOp action) {
        String name = action.getName();
        SubProductionTask task;
        int orderIdIndex;
        int orderId;
        int robotIndex =  action.getInstantiations()[0];
        int robotId = Character.getNumericValue(cp.getConstants().get(robotIndex).charAt(1));
        switch (name) {
            case "getbasefrombscriticaltask":
                orderIdIndex =  action.getInstantiations()[2];
                orderId = Integer.parseInt(cp.getConstants().get(orderIdIndex).substring(1));
                Optional<ProductOrder> optorder = productOrderDao.findById((long) orderId);
                ProductOrder order = optorder.get();
                BaseColor baseColor = order.getBaseColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setPreConditionTasks(Collections.emptySet())
                        .setName("GetBaseFromBS")
                        .setMachine("C-BS")
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)
                        .setSide(MachineSide.OUTPUT)
                        .setOrderInfoId((long) orderId)
                        .setLockMachine("C-BS")
                        .setUnlockMachine("C-BS")
                        .setIncrementMachine(null)
                        .setDecrementMachine(null)
                        .setSameRobotSubTask(null)
                        .setRequiredColor(baseColor.toString())
                        .setOptCode(null)
                        .setFatal(true)
                        .setRequiresReset(true)
                        .setPriority(Config.PRIORITY_FIRST_RING)
                        .build();
                break;



            default:
                System.out.println("case not managed");
                task = null;
                break;
        }
        task.setRobotId(robotId);
        return task;
    }
}
