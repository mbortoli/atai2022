package com.grips.scheduler.asp.dispatcher;


import com.grips.scheduler.asp.encoding.IntOp;
import com.grips.scheduler.asp.planparser.TemporalEdge;
import com.grips.scheduler.asp.planparser.TemporalGraph;
import com.grips.scheduler.asp.planparser.TemporalNode;
import com.grips.scheduler.asp.util.KnowledgeBase;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class Dispatcher {

    private BigInteger currentTime;
    static long timerStarting;
    private Timer timer;
    private TemporalGraph tgraph;
    private KnowledgeBase kb;


    public Dispatcher(BigInteger currentTime, TemporalGraph tgraph, KnowledgeBase kb) {
        this.currentTime = currentTime;
        this.tgraph = tgraph;
        this.kb = kb;

    }

    public void dispatch () {
        timer = new Timer();
        timerStarting = System.currentTimeMillis();

        //get the plan starting node
        TemporalNode planStart = tgraph.getNode(0);
        tryDispatchNode(planStart);

    }

    public void tryDispatchNode (TemporalNode node) {
        //checking input edges are all enabled
        boolean ready = true;
        Set<TemporalEdge> inEdges = tgraph.getInputEdges(node);
        Iterator<TemporalEdge> it = inEdges.iterator();
        while(it.hasNext() && ready){
            TemporalEdge e = it.next();
            if(!e.getEnabled()) {
                ready = false;
            }
        }
        if (ready && node.getType() != 2) {
            //dispatching the node
            dispatchNode(node);
            //enabling outpud edges
            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
            it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getLb() == Double.MIN_VALUE) {              //simplest case, without lower bound on the edge
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    tryDispatchNode(tgraph.getTarget(e));
                } else {                                        //action duration is the only case for the moment
                    //timer.schedule(new ActionEnd(e), (System.currentTimeMillis() - timerStarting) + Math.round(e.getLb()*1000));
                    timer.schedule(new ActionEnd(e),  Math.round(e.getLb()*1000));
                }

            }
        }

    }

    public void dispatchEndNode (TemporalNode node) {
        //todo check if lower and upper bounds of inconimng edges are respected
        Iterator<TemporalEdge> it;
        dispatchNode(node);
        //enabling outpud edges
        Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
        it = outEdges.iterator();
        while (it.hasNext()) {
            TemporalEdge e = it.next();
            if (e.getLb() == Double.MIN_VALUE) {              //simplest case, without lower bound on the edge
                e.enable();
                tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                tryDispatchNode(tgraph.getTarget(e));
            } else {                                        //action duration is the only case for the moment
                //timer.schedule(new ActionEnd(e), (System.currentTimeMillis() - timerStarting) + Math.round(e.getLb()*1000));
                timer.schedule(new ActionEnd(e),  Math.round(e.getLb()*1000));
            }

        }
    }

    public void dispatchNode (TemporalNode node) {
        int type = node.getType();
        double time = node.getTime();

        if (type == 1) {
            IntOp action = tgraph.nodeToAction(time, node.getId());
            //KnowledgeBase.normalizeAction(action, type);
            System.out.println("Time: " + time + " Timer: " + (System.currentTimeMillis() - timerStarting) +  " - Dispacting action " + action);


        } else if (type == 2) { //end action
            IntOp action = tgraph.nodeToAction(time, node.getId());
            //KnowledgeBase.normalizeAction(action, type);
            System.out.println("Time: " + time + " Timer: " + (System.currentTimeMillis() - timerStarting) + " - Finishing action " + action);

        }
    }

    public class ActionEnd extends TimerTask {

        TemporalEdge e;

        public ActionEnd(TemporalEdge e) {
            this.e = e;
        }

        @Override
        public void run() {
            e.enable();
            tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
            //tryDispatchNode(tgraph.getTarget(e));
        }


    }



}
