package com.grips.scheduler.exploration;

import com.google.common.collect.Lists;
import com.shared.domain.MachineName;
import com.grips.persistence.domain.ProductTask;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.dao.ProductOrderDao;
import com.grips.persistence.dao.ProductTaskDao;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.protobuf_lib.RobotConnections;
import com.robot_communication.services.RobotClient;
import com.grips.team_server.SubTaskGenerator;
import com.robot_communication.services.PrsTaskCreator;
import com.shared.domain.Complexity;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.grips.persistence.domain.SubProductionTask.TaskState.ASSIGNED;
import static com.grips.persistence.domain.SubProductionTask.TaskState.TBD;

@Service
@CommonsLog
public class ProductionTaskInExplorationService {

    private final Set<MachineName> csWithTask;
    private final SubProductionTaskDao subProductionTaskDao;
    private final RobotClient robotClient;
    private final SubTaskGenerator subTaskGenerator;
    private final ProductTaskDao productTaskDao;
    private final ProductOrderDao productOrderDao;
    private final RobotConnections robotConnections;
    private final PrsTaskCreator prsTaskCreator;

    private SubProductionTask bsTask; //todo refactor that!

    public ProductionTaskInExplorationService(SubProductionTaskDao subProductionTaskDao,
                                              RobotClient robotClient,
                                              SubTaskGenerator subTaskGenerator,
                                              ProductTaskDao productTaskDao,
                                              ProductOrderDao productOrderDao,
                                              RobotConnections robotConnections,
                                              PrsTaskCreator prsTaskCreator) {
        this.subProductionTaskDao = subProductionTaskDao;
        this.robotClient = robotClient;
        this.subTaskGenerator = subTaskGenerator;
        this.productTaskDao = productTaskDao;
        this.productOrderDao = productOrderDao;
        this.robotConnections = robotConnections;
        this.prsTaskCreator = prsTaskCreator;
        this.csWithTask = new HashSet<>();
    }

    public boolean assignPrepareCapTask(Long robotId) {
        List<SubProductionTask> prepareCapTasks = subProductionTaskDao.findByRobotIdAndState(-1, SubProductionTask.TaskState.ASSIGNED_EXPLORATION);
        if (prepareCapTasks != null && prepareCapTasks.size() > 0) {
            // there is a task that can be done by this robot
            // only the cap loading should be assigned, discarding
            // the trash cannot be done since prepare machine doesnt work

            //TODO: set start time and stuff that is done in production scheduler when task is actually assigned
            Optional<SubProductionTask> bufferCap = prepareCapTasks.stream()
                    .filter(t -> t.getPreConditionTasks() == null || t.getPreConditionTasks().size() == 0)
                    .findAny();
            if (bufferCap.isPresent()) {
                SubProductionTask bufferCapTask = bufferCap.get();
                bufferCapTask.setState(SubProductionTask.TaskState.INWORK, "inwork started in exploration");
                bufferCapTask.setRobotId(robotId.intValue());
                List<SubProductionTask> sameRobotTasks = prepareCapTasks.stream()
                        .filter(p -> bufferCapTask.equals(p.getSameRobotSubTask()))
                        .collect(Collectors.toList());
                sameRobotTasks.forEach(s -> s.setRobotId(robotId.intValue()));
                sameRobotTasks.forEach(s -> s.setState(SubProductionTask.TaskState.ASSIGNED, "assigned started in exploration"));

                Optional<SubProductionTask> next = prepareCapTasks.stream()
                        .filter(p -> p.getPreConditionTasks().contains(bufferCapTask))
                        .findAny();
                while (next.isPresent()) {
                    if (next.get().getState() != ASSIGNED)
                        next.get().setState(TBD, "dispose waste set to TBD in exploration");
                    subProductionTaskDao.save(next.get());
                    final SubProductionTask fNext = next.get();
                    next = prepareCapTasks.stream().filter(p -> p.getPreConditionTasks().contains(fNext)).findAny();
                }

                subProductionTaskDao.save(bufferCapTask);
                subProductionTaskDao.saveAll(sameRobotTasks);
                robotClient.sendGetTaskToRobot(bufferCapTask.getRobotId().longValue(),
                        bufferCapTask.getId(), bufferCapTask.getMachine(), bufferCapTask.getSide(), 1); //todo check if 1 or 0!

                return true;
            }
        }
        return false;
    }

    public void createBufferCapTask(MachineName machine) {
        if (csWithTask.contains(machine)) {
            log.warn(machine + " already got task!");
            return;
        }
        csWithTask.add(machine);
        log.info(machine + " reported, creating task!");

        ProductOrder pOrder = new ProductOrder();
        pOrder.setId(System.currentTimeMillis());
        pOrder.setComplexity(Complexity.E0);
        productOrderDao.save(pOrder);

        ProductTask pTask = new ProductTask();
        pTask.setState(ProductTask.ProductState.INWORK, "cap buffer productTask inwork");
        pTask.setProductOrder(pOrder);
        productTaskDao.save(pTask);

        // we do not know the dispose station yet, so it should be set on task assignment
        List<SubProductionTask> prepareCapTasks = subTaskGenerator.prepareCapAssignDisposeLater(pOrder,
                machine.getRawMachineName());

        prepareCapTasks.forEach(t -> t.setFatal(true)); // do not reassign
        prepareCapTasks.forEach(t -> t.setRobotId(new Long(-1).intValue()));
        prepareCapTasks.forEach(t -> t.setState(SubProductionTask.TaskState.ASSIGNED_EXPLORATION, "set to ASSIGNED_EXPLORATION when machine was found"));
        prepareCapTasks.forEach(t -> t.setProductTask(pTask));

        prepareCapTasks.get(1).setDemandTaskWithOutDemand(true);

        pTask.setSubProductionTasks(prepareCapTasks);
        subProductionTaskDao.saveAll(prepareCapTasks);
    }

    public void createBsTask(MachineName machine) {
        ProductOrder pOrder = new ProductOrder();
        pOrder.setId(System.currentTimeMillis());

        ProductTask pTask = new ProductTask();
        pTask.setState(ProductTask.ProductState.INWORK);
        pTask.setProductOrder(pOrder);

        pOrder.setId(System.currentTimeMillis());
        pOrder.setComplexity(Complexity.D0);

        productOrderDao.save(pOrder);
        productTaskDao.save(pTask);

        bsTask = SubProductionTaskBuilder.newBuilder()
                .setMachine(machine.getRawMachineName())
                .setName("[D0] VisitRandomPlace")
                .setState(SubProductionTask.TaskState.INWORK)
                .setSide(MachineSide.OUTPUT)
                .setType(SubProductionTask.TaskType.DUMMY)
                .setFatal(true) // should not be reasigned
                .build();
        bsTask.setRobotId(-1);

        bsTask.setProductTask(pTask);
        pTask.setSubProductionTasks(Lists.newArrayList(bsTask));
        subProductionTaskDao.save(bsTask);
    }

    public boolean assignBsTask(Long robotId) {
        if (bsTask == null) {
            return false;
        }
        GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask = prsTaskCreator.createMoveToMachineTask(robotId,
                new Random().nextInt(),
                new MachineName(bsTask.getMachine()),
                bsTask.getSide());

        robotClient.sendPrsTaskToRobot(prsTask);
        return true;
    }
}
