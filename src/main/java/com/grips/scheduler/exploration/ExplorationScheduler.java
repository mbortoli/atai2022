package com.grips.scheduler.exploration;

import com.grips.config.ExplorationConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.robot_communication.services.RobotClient;
import com.grips.robot.SendSuccessfullMachineReports;
import com.grips.scheduler.api.IScheduler;
import com.shared.domain.MachineName;
import com.grips.protobuf_lib.RobotConnections;
import com.google.common.collect.Lists;
import com.grips.model.teamserver.*;
import com.grips.scheduler.GameField;
import com.robot_communication.services.PrsTaskCreator;
import com.shared.domain.Point2d;
import lombok.NonNull;
import lombok.Synchronized;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsBeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

@CommonsLog
@Service
public class ExplorationScheduler implements IScheduler {
    private final int NUMBER_OF_MACHINES = 4; //16
    private final GameField _gameField;
    private final GameStateDao gameStateDao;
    private final RobotConnections _robotConnections;
    private final RobotObservationDao _robotObservationDao;
    private final ExplorationTaskDao _explorationTaskDao;
    private final MachineReportDao _machineReportDao;
    private final PrsTaskCreator prsTaskCreator;
    private final ProductionTaskInExplorationService productionTaskInExplorationService;
    private final ExplorationConfig explorationConfig;
    private final SendSuccessfullMachineReports sendSuccessfullMachineReports;
    private final RobotClient robotClient;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;

    private List<RobotObservation> _notVerifiedObservations = new ArrayList<>();
    private List<MachineName> _alreadyReportedMachines = new ArrayList<>();

    private HashMap<Peer, GripsMidlevelTasksProtos.GripsMidlevelTasks> _activeExplorationTasks = new HashMap<>();
    private HashMap<Peer, GripsMidlevelTasksProtos.GripsMidlevelTasks> _previousExplorationTask = new HashMap<>();

    private int _sentToBs = -1;

    public ExplorationScheduler(GameField gameField,
                                GameStateDao gameStateDao,
                                RobotConnections robotConnections,
                                RobotObservationDao robotObservationDao,
                                ExplorationTaskDao explorationTaskDao,
                                MachineReportDao machineReportDao,
                                PrsTaskCreator prsTaskCreator,
                                ProductionTaskInExplorationService productionTaskInExplorationService,
                                ExplorationConfig explorationConfig,
                                SendSuccessfullMachineReports sendSuccessfullMachineReports,
                                RobotClient robotClient,
                                BeaconSignalFromRobotDao beaconSignalFromRobotDao) {
        _gameField = gameField;
        this.gameStateDao = gameStateDao;
        _robotConnections = robotConnections;
        _robotObservationDao = robotObservationDao;
        _explorationTaskDao = explorationTaskDao;
        _machineReportDao = machineReportDao;
        this.prsTaskCreator = prsTaskCreator;
        this.productionTaskInExplorationService = productionTaskInExplorationService;
        this.explorationConfig = explorationConfig;
        this.sendSuccessfullMachineReports = sendSuccessfullMachineReports;
        this.robotClient = robotClient;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        _notVerifiedObservations.addAll(Lists.newArrayList(_robotObservationDao.findAll()));
        Iterable<MachineReport> machineReports = _machineReportDao.findAll();
        for (MachineReport mReport : machineReports) {
            _alreadyReportedMachines.add(mReport.getName());
        }
        log.info("Using exploration config: " + explorationConfig.toString());
    }

    @Synchronized
    public void addRobotObservation(RobotObservation observation) {
        log.info("Adding Observation: " + observation);
        if (_alreadyReportedMachines.contains(observation.getMachineName())) {
            return;
        }
        // check if machine in zone is already reported by this robot
        if (_notVerifiedObservations.stream()
                .anyMatch(observation::isLogicalSame)) {
            return;
        }
        // DS may only be approached from input side
        if (observation.getMachineName().isDeliveryStation()) {
            observation.setSide("input");
        }
        _notVerifiedObservations.add(observation);
        _robotObservationDao.save(observation);
        log.info("Added Observation successfully: " + observation);
    }

    // if a robot is active, assign a new task, otherwise return 0
    @Synchronized
    public GripsMidlevelTasksProtos.GripsMidlevelTasks getNextExplorationTask(@NonNull Integer robotId) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks newTask = null;
        Peer robot = _robotConnections.getRobot(robotId);

        // new in 2019: before letting this robot do further exploration tasks, see if there is a preparecap for him
        // (but only do this in the last minute of exploration)
        BigInteger latestGameTimeExplorationPhase = gameStateDao.getLatestGameTimeExplorationPhase();
        if (explorationConfig.isEarly_cap_buffering()
                && latestGameTimeExplorationPhase.compareTo(explorationConfig.getStart_early_cap_buffering()) >= 0) {
            if (productionTaskInExplorationService.assignPrepareCapTask(robotId.longValue())) {
                return null;
            }
        }

        if (_notVerifiedObservations.size() > 0) {
            // if we do have unchecked observations, we send
            // the robot to the exploration that makes the most sense
            // for him to verify
            newTask = getBestExplorationOption(robot);
            if (newTask == null) {
                newTask = getRandomExplorationZone(robot);
            }
        } else {
            // if we do not have any observation to check
            // we send to robot to any random zone that is
            // still unexplored
            if (_alreadyReportedMachines.size() < NUMBER_OF_MACHINES) {
                newTask = getRandomExplorationZone(robot);
            } else if (explorationConfig.isEarly_cap_buffering()
                    && _sentToBs == -1
                    && latestGameTimeExplorationPhase.compareTo(explorationConfig.getStart_early_cap_buffering()) >= 0) {
                _sentToBs = robotId;

            } else {
                log.warn("No Task for Robot : " + robotId + " all machines reported!");
                // do not do anything here
                //newTask = getRandomExplorationZone(robot);
            }
        }

        if (newTask != null) {
            _activeExplorationTasks.put(robot, newTask);

            ExplorationTask eTask = new ExplorationTask();
            eTask.setRobotId(robotId);
            eTask.setMachine(newTask.getExploreMachine().getMachineId());
            eTask.setZone(newTask.getExploreMachine().getWaypoint());
            eTask.setTimeStamp(System.currentTimeMillis());
            _explorationTaskDao.save(eTask);
        }

        return newTask;
    }

    @Synchronized
    public void explorationTaskResult(long robotId, @NonNull final MachineName machineName, String zoneName) {

        log.info("explorationTaskResult: robotId: " + robotId + ", machineName: " + machineName + "zoneName: " + zoneName);
        if (machineName != null) {
            _alreadyReportedMachines.add(machineName);
            _alreadyReportedMachines.add(machineName.mirror());
            List<RobotObservation> toDelete = _notVerifiedObservations.stream()
                    .filter(o -> o.getMachineName().equals(machineName) || o.getMachineName().equals(machineName.mirror()))
                    .collect(Collectors.toList());
            _notVerifiedObservations.removeAll(toDelete);
            _robotObservationDao.deleteAll(toDelete);
        }

        _gameField.getZoneByName(zoneName).setExplorationState(ExplorationZone.ZoneExplorationState.EXPLORED);

        //todo because of challanges _gameField.getZoneByName(zoneName).getMirroredZone().setExplorationState(ExplorationZone.ZoneExplorationState.EXPLORED);

        Peer robot = _robotConnections.getRobot(robotId);
        GripsMidlevelTasksProtos.GripsMidlevelTasks activeTask = _activeExplorationTasks.get(robot);
        _previousExplorationTask.put(robot, activeTask);
        _activeExplorationTasks.remove(robot);

        // new 2019: if a CS is reported, start to buffer the cap (but only in the last minute of exploration phase,
        // this is checked when assigning the task)
        if (explorationConfig.isEarly_cap_buffering() && machineName.isCapStation()) {
            if (gameStateDao.isCyan() && machineName.isMagenta()
                    || gameStateDao.isMagenta() && machineName.isCyan()) {
                productionTaskInExplorationService.createBufferCapTask(machineName.mirror());
            } else if (gameStateDao.isMagenta() && machineName.isCyan()) {
                productionTaskInExplorationService.createBufferCapTask(machineName);
            }
        }
    }

    @Synchronized
    public void explorationTaskFailed(long robotId, MachineName machineName, String zoneName) {
        // remove either own observation or other observation
        if (!machineName.isDummyMachine()) {
            // we where observing a machine
            Optional<RobotObservation> ownObs = _notVerifiedObservations.stream()
                    .filter(o -> o.getRobotId() == robotId
                            && o.getMachineName().equals(machineName)
                            && o.getMachineZone().equalsIgnoreCase(zoneName))
                    .findAny();
            if (ownObs.isPresent()) {
                log.info("Removing robot's own observation for machine " + machineName + " at zone " + zoneName);
                _notVerifiedObservations.remove(ownObs.get());
            }

            Optional<RobotObservation> anyObs = _notVerifiedObservations.stream()
                    .filter(o -> o.getMachineName().equals(machineName) && o.getMachineZone().equalsIgnoreCase(zoneName))
                    .findAny();
            if (anyObs.isPresent()) {
                log.info("Removing random own observation for machine " + machineName + " at zone " + zoneName);
                _notVerifiedObservations.remove(anyObs.get());
            }
        } else {
            _gameField.getZoneByName(zoneName).setExplorationState(ExplorationZone.ZoneExplorationState.EXPLORED);
        }
    }

    private GripsMidlevelTasksProtos.GripsMidlevelTasks getBestExplorationOption(Peer robot) {
        // first see, if there are observations by this robot
        List<RobotObservation> usedObservations = new LinkedList<>();
        List<RobotObservation> ownObservations = _notVerifiedObservations.stream().filter(o -> o.getRobotId() == robot.getId()).collect(Collectors.toList());
        if (ownObservations.size() > 0) {
            usedObservations.addAll(ownObservations);
        } else {
            // this case should never happen todo ask thomas/jakob why!
            usedObservations.addAll(_notVerifiedObservations);
        }

        // now we try to find the nearest observation
        // first we check, if the robot has a previous task
        Point2d prevZoneCenter = null;
        GripsMidlevelTasksProtos.GripsMidlevelTasks prevTask = _previousExplorationTask.get(robot);
        if (prevTask != null) {
            //System.out.println("Previous task found for position estimation");
            String zoneId = prevTask.getExploreMachine().getWaypoint();
            ExplorationZone prevZone = _gameField.getZoneByName(zoneId);
            prevZoneCenter = prevZone.getZoneCenter();

            if (prevZoneCenter == null) {
                log.warn("Error that should not happen, check!!!");
                prevZoneCenter = new Point2d(0, 0);
            }
        } else {
            prevZoneCenter = new Point2d(0, 0);
        }

        RobotObservation obs = findNearestObservationNotScheduled(usedObservations, prevZoneCenter);

        GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask = null;
        if (obs != null) {
            prsTask = prsTaskCreator.createExplorationTask(robot.getId(), obs.getMachineName().getRawMachineName(), obs.getMachineZone(), obs.getSide());
            log.info("Sending Robot " + robot.getId() + " to explore Machine " + prsTask.getExploreMachine().getMachineId()
                    + " at Zone " + prsTask.getExploreMachine().getWaypoint() + " side: " + prsTask.getExploreMachine().getMachinePoint());
        }

        return prsTask;
    }

    private RobotObservation findNearestObservationNotScheduled(List<RobotObservation> usedObservations, Point2d prevZoneCenter) {
        double minDistance = Double.MAX_VALUE;
        RobotObservation nearestObservation = null;

        for (RobotObservation obs : usedObservations) {
            ExplorationZone zone = _gameField.getZoneByName(obs.getMachineZone());
            if (zone == null) {
                continue;
            }
            double distance = zone.getZoneCenter().distance(prevZoneCenter);
            boolean alreadyScheduled = isAlreadyScheduled(obs);
            if (distance < minDistance && !alreadyScheduled) {
                minDistance = distance;
                nearestObservation = obs;
            }
        }

        return nearestObservation;
    }

    private boolean isAlreadyScheduled(RobotObservation obs) {
        boolean alreadyScheduled = _activeExplorationTasks.values().stream().anyMatch(t -> t.getExploreMachine().getWaypoint().equalsIgnoreCase(obs.getMachineZone()));
        //todo because of challanges  String mirroredZone = _gameField.getZoneByName(obs.getMachineZone()).getMirroredZone().getZoneName();
        // alreadyScheduled |= _activeExplorationTasks.values().stream()
        //         .anyMatch(t -> t.getExploreMachine().getWaypoint().equalsIgnoreCase(mirroredZone));
        alreadyScheduled |= _activeExplorationTasks.values().stream()
                .anyMatch(t -> new MachineName(t.getExploreMachine().getMachineId()).equals(obs.getMachineName()));
        alreadyScheduled |= _activeExplorationTasks.values().stream()
                .anyMatch(t -> new MachineName(t.getExploreMachine().getMachineId()).equals(obs.getMachineName().mirror()));

        return alreadyScheduled;
    }


    private GripsMidlevelTasksProtos.GripsMidlevelTasks getRandomExplorationZone(Peer robot) {
        Optional<ExplorationZone> unexploredZone = _gameField.getRandomUnexploredZoneForTeam();

        //todo smart version to select a machine!
        MachineName name = new MachineName("C-CS1");

        if (unexploredZone.isPresent()) {
            unexploredZone.get().setExplorationState(ExplorationZone.ZoneExplorationState.SCHEDULED);
            GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask = prsTaskCreator.createExplorationTask(robot.getId(),
                    name.getRawMachineName(), unexploredZone.get().getZoneName(), "input");
            log.info("Sending Robot to random Zone " + prsTask.getExploreMachine().getWaypoint());
            return prsTask;
        }

        return null;
    }

    @Override
    public void handleBeaconSignal(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, Peer robot) {
        BigInteger latestGameTimeExplorationPhase = gameStateDao.getLatestGameTimeExplorationPhase();

        // immediately assign tasks to robot1, wait for robot2 and robot3
        if (beacon_signal.getTaskId() == -1 && checkIfShouldStart(robotId, latestGameTimeExplorationPhase)) {
            //robot currently has no task assigned

            GripsMidlevelTasksProtos.GripsMidlevelTasks task = getNextExplorationTask(robotId);

            if (task != null) {
                robotClient.sendPrsTaskToRobot(task);
            } else {
                log.warn("No task for Robot " + robotId + " found in exploration!");
            }
        } else {
            log.debug("robot already has a task assigned, do not assign a new one");
        }

        // we also broadcast all already successfully reported machines to all robots
        sendSuccessfullMachineReports.sendSuccessfullMachineReports(robotId);
    }

    private boolean checkIfShouldStart(int robotId, BigInteger latestGameTimeExplorationPhase) {
        BigInteger inMs = latestGameTimeExplorationPhase.divide(new BigInteger("1000000"));
        log.info("LatestTime in ExplorationPhase is: " + inMs.toString());
        return (robotId == 1 && inMs.longValue() > explorationConfig.getTime_offset().getRobot1() * 1000L  /* s -> ms */)
                || (robotId == 2 && inMs.longValue() > explorationConfig.getTime_offset().getRobot2() * 1000L /* s -> ms */)
                || (robotId == 3 && inMs.longValue() > explorationConfig.getTime_offset().getRobot3() * 1000L /* s -> ms */);
    }

    private boolean checkIfShouldStart2(int robotId, BigInteger latestGameTimeExplorationPhase) {
        if (robotId == 1) {
            return true;
        } else {
            if (robotId == 2) {
                BeaconSignalFromRobot from1 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("1");
                return checkPose(from1.getPoseX(), from1.getPoseY());
            } else { //robotId = 3
                BeaconSignalFromRobot from2 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("2");
                return checkPose(from2.getPoseX(), from2.getPoseY());
            }
        }
    }

    private boolean checkPose(double poseX, double poseY) {
        if (poseX < 4.6 && poseX > 4.0) {
            if (poseY < 1.7) {
                return false;
            }
        }
        return true;
    }


    @Override
    public void handleRobotTaskResult(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask) {

    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {

    }
}
