export GRIPS_HW_TYPE=intel
runtime=$(docker info | grep "Default Runtime" | awk '{print $3}')
echo "Runtime is: $runtime"
if [[ $runtime == "nvidia" ]]
then
    export GRIPS_HW_TYPE=nvidia
fi

export GRIPS_GAZEBO_GUI=true;
export GRIPS_SKIP_LOCAL_BUILD=false;
export GRIPS_LOG_ROSBAG=false;
export GRIPS_LOG_GAZEBO=false;

teamserver_scripts_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export TEAM_SERVER_DIR=$teamserver_scripts_dir/../..
function start_simulation() { cd $teamserver_scripts_dir && ./start.sh $1; }
alias stop_simulation="$teamserver_scripts_dir/stop.sh"
alias llsf-refbox-shell="docker exec -it refbox bash -c 'sleep 0.5 && /usr/local/bin/llsf-refbox-shell'"
alias rviz="docker exec -it grips_gazebo bash -c 'source /root/logistics/.devcontainer/setup.bash && rviz'"
alias copy_gazebo_log="docker cp grips_gazebo:/root/.ros/gazebo_log/state.log state.log"
alias copy_ros_log="docker cp grips_simulation:/ros.bag.active ros.bag"
alias copy_teamserver_log="docker cp grips_team_server:/recordings/. ."
alias copy_logs="copy_gazebo_log && copy_teamserver_log && copy_ros_log"
alias sx="screen -x"
alias sb="source ~/.bashrc"
alias vs="nano ~/team_server/scripts/setup.sh"

export TS_TEAM_SERVER_IMAGE=registry.gitlab.com/grips_robocup/team_server:master
export TS_REFBOX_IMAGE=registry.gitlab.com/pkohout/rc-eval-simulation/refbox
export TS_LOGISTICS_IMAGE=registry.gitlab.com/grips_robocup/logistics:master
export TS_GAZEBO_IMAGE=registry.gitlab.com/grips_robocup/logistics/tools:${GRIPS_HW_TYPE}

function play_gazebo_log() { docker rm -f grips_gazebo ; true && docker run --restart no -e DISPLAY=$DISPLAY -e QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v /dev/dri:/dev/dri -v ~/team_server/.gazebo:/root/.gazebo -v /usr/lib/x86_64-linux-gnu/libXv.so.1:/usr/lib/x86_64-linux-gnu/libXv.so.1:rw -v $(pwd)/$1:/state.log --name grips_gazebo -it registry.gitlab.com/grips_robocup/logistics/tools:${GRIPS_HW_TYPE} bash -c "source /setup_env.sh && export GAZEBO_MASTER_URI=http://localhost:11345 &&sleep 2 && gazebo --verbose -u -p /state.log"; }

function filter_gazebo_log() { docker rm -f grips_gazebo ; true && docker run --restart no -e DISPLAY=$DISPLAY -e QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v /dev/dri:/dev/dri -v ~/team_server/.gazebo:/root/.gazebo -v /usr/lib/x86_64-linux-gnu/libXv.so.1:/usr/lib/x86_64-linux-gnu/libXv.so.1:rw -v $(pwd):/out -v $(pwd)/$1:/state.log --name grips_gazebo -it registry.gitlab.com/grips_robocup/logistics/tools:${GRIPS_HW_TYPE} bash -c "echo \"Filtering log....this may take several minutes...\" && gz log -f /state.log -o /out/$2 -z 25 && echo \" finished filtering log!\""; }

function reindex_ros_log() { docker rm -f grips_gazebo ; true && docker run --restart no -e DISPLAY=$DISPLAY -e QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v /dev/dri:/dev/dri -v ~/team_server/.gazebo:/root/.gazebo -v /usr/lib/x86_64-linux-gnu/libXv.so.1:/usr/lib/x86_64-linux-gnu/libXv.so.1:rw -v $(pwd)/$1:/ros.bag --name grips_gazebo -it registry.gitlab.com/grips_robocup/logistics/tools:${GRIPS_HW_TYPE} bash -c "source /setup_env.sh && export ROS_MASTER_URI=http://localhost:11311 && roscore & sleep 1 && rosbag reindex /ros.bag"; }

function play_ros_log() { docker rm -f grips_gazebo ; true && docker run --restart no -e DISPLAY=$DISPLAY -e QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v /dev/dri:/dev/dri -v ~/team_server/.gazebo:/root/.gazebo -v /usr/lib/x86_64-linux-gnu/libXv.so.1:/usr/lib/x86_64-linux-gnu/libXv.so.1:rw -v $(pwd)/$1:/ros.bag --name grips_gazebo -it registry.gitlab.com/grips_robocup/logistics/tools:${GRIPS_HW_TYPE} bash -c "source /setup_env.sh && export ROS_MASTER_URI=http://localhost:11311 && roscore & rviz & sleep 1 && rosbag play /ros.bag"; }


function install_rcll_sim_deps() {
sudo apt install python3-pip mongo-tools
sudo pip3 install pymongo
sudo pip3 install matplotlib
}
