#!/bin/bash
source ./setup.sh
for COUNT in {1..5}
do
    ./stop.sh
    echo "Starting Simulation number $COUNT"
    screen -dmS game_controller bash -c ./game_control.sh
    ./start.sh 2
    echo "Finished simulation number $COUNT"
    echo "Stopping screen with game_controller!"
    screen -X -S game_controller quit
done
