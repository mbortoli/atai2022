import { Component, OnInit } from '@angular/core';
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.sass']
})
export class StatisticsComponent implements OnInit {

  public data$;

  constructor(private dataService: DataService) {
    this.data$ = dataService.getPointsForStatistics();
  }

  ngOnInit() {
  }

}
