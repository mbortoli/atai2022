import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamePointsComponent } from './game-points.component';

describe('GamePointsComponent', () => {
  let component: GamePointsComponent;
  let fixture: ComponentFixture<GamePointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamePointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamePointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
