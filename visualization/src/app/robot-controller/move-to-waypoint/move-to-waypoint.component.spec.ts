import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveToWaypointComponent } from './move-to-waypoint.component';

describe('MoveToWaypointComponent', () => {
  let component: MoveToWaypointComponent;
  let fixture: ComponentFixture<MoveToWaypointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoveToWaypointComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveToWaypointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
