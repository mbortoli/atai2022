import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreMachineComponent } from './explore-machine.component';

describe('ExploreMachineComponent', () => {
  let component: ExploreMachineComponent;
  let fixture: ComponentFixture<ExploreMachineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExploreMachineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreMachineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
