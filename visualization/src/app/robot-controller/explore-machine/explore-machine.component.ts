import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {formly} from "ngx-formly-helpers";
import {RobotOptions} from "../robots";
import {MachineOptions} from "../machines";
import {MachinePointOptions} from "../machine-points";
import {EndpointService} from "../../services/endpoint.service";

@Component({
  selector: 'app-explore-machine',
  templateUrl: './explore-machine.component.html',
  styleUrls: ['./explore-machine.component.sass']
})
export class ExploreMachineComponent implements OnInit {

  form = new FormGroup({});
  model: any = {};
  fields: any[] = [
    formly.requiredSelect("robotId", "Robot ID", RobotOptions),
    formly.requiredSelect("machine", "Machine", MachineOptions),
    formly.requiredText("zone", "Zone"),
    formly.requiredSelect("side", "Side", MachinePointOptions),
  ];

  constructor(private endpoint: EndpointService) { }

  ngOnInit(): void {
  }

  public submit() {
    console.log("Model is: ", this.model);
    this.endpoint.post("robot-controller/explore-machine", this.model)
      .subscribe(res => console.log("Explore Machine send!"));
  }

}
