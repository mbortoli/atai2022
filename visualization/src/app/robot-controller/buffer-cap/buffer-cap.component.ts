import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {formly} from "ngx-formly-helpers";
import {RobotOptions} from "../robots";
import {EndpointService} from "../../services/endpoint.service";

@Component({
  selector: 'app-buffer-cap',
  templateUrl: './buffer-cap.component.html',
  styleUrls: ['./buffer-cap.component.sass']
})
export class BufferCapComponent implements OnInit {

  form = new FormGroup({});
  model: any = {};
  fields: any[] = [
    formly.requiredSelect("robotId", "Robot ID", RobotOptions),
    formly.requiredText("machine", "Machine"),
    formly.requiredNumber("shelf", "Shelf")
  ];

  constructor(private endpoint: EndpointService) {
  }

  ngOnInit(): void {
  }

  submit(): void {
    console.log("Model is: ", this.model);
    this.endpoint.post("robot-controller/buffer-cap", this.model)
      .subscribe(res => console.log("Buffer Cap send!"));
  }
}
