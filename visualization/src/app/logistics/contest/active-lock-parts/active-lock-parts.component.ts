import {Component, Input, OnInit} from '@angular/core';
import {NestedTreeControl} from "@angular/cdk/tree";
import {of, timer} from "rxjs";
import {mergeMap} from "rxjs/operators";
import {MatTreeNestedDataSource} from "@angular/material/tree";

@Component({
  selector: 'app-active-lock-parts',
  templateUrl: './active-lock-parts.component.html',
  styleUrls: ['./active-lock-parts.component.sass']
})
export class ActiveLockPartsComponent implements OnInit {


  @Input() data;

  treeControl = new NestedTreeControl<any>(node => node.children);
  dataSource = new MatTreeNestedDataSource<any>();

  constructor() { }

  ngOnInit() {
  }

  updateData() {
    if (this.data == undefined) {
      return;
    }
    let tmp = this.data.reduce((obj, ele) => {
      if (obj.filter(x => x.machine == ele.machine)[0] == undefined) {
        obj.push({
          machine: ele.machine,
          children: []
        });
      }
      obj.filter(x => x.machine == ele.machine)[0].children.push(ele);
      return obj;
    }, []);
    this.dataSource.data = tmp;
    this.treeControl.dataNodes = tmp;
    this.treeControl.dataNodes.forEach(node => this.treeControl.expand(node));
  }

  startUpdates() {
    timer(0, 3000).pipe(
      mergeMap(_ => of(this.updateData()))).subscribe();
  }

  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;

}
