import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {of, timer} from "rxjs";
import {mergeMap} from "rxjs/operators";
import {ActivePartDemandsComponent} from "./active-part-demands/active-part-demands.component";
import {ActiveLockPartsComponent} from "./active-lock-parts/active-lock-parts.component";
import {DataService} from "../../services/data.service";
import {MapComponent} from "./map/map.component";

@Component({
  selector: 'app-contest',
  templateUrl: './contest.component.html',
  styleUrls: ['./contest.component.sass']
})
export class ContestComponent implements OnInit {

  @ViewChild(ActivePartDemandsComponent) partDemands;
  @ViewChild(ActiveLockPartsComponent) lockParts;
  @ViewChild(MapComponent) map;


  tabIndex: number;


  constructor(public dataService: DataService) {
  }

  ngOnInit() {
    /*
    this.tabIndex = 0;
    //this.loadStatic();
    timer(0, 3000).pipe(
      mergeMap(_ => {
          if (this.partDemands != undefined) {
            return of(this.partDemands.updateData());
          } else {
            //console.log('partsDemands null!');
            return of({});
          }
        }
      )).subscribe();

    timer(0, 3000).pipe(
      mergeMap(_ => {
          if (this.partDemands != undefined) {
            return of(this.lockParts.updateData());
          } else {
            //console.log('lockParts null!');
            return of({});
          }
        }
      )).subscribe();

    timer(0, 1000).pipe(
      mergeMap(_ => {
          if (this.map != undefined) {
            return of(this.map.update());
          } else {
            //console.log('map is null!');
            return of({});
          }
        }
      )).subscribe();

     */
  }

  test() {
    console.log("In Test!!");
  }

  onTabChange($event) {
    console.log("Here: $event");
    console.log($event);
    this.tabIndex = $event.index;
  }
}
