import {Component, Input, OnInit} from '@angular/core';
import {of, timer} from "rxjs";
import {mergeMap} from "rxjs/operators";

@Component({
  selector: 'app-robot-beacon-signal',
  templateUrl: './robot-beacon-signal.component.html',
  styleUrls: ['./robot-beacon-signal.component.sass']
})
export class RobotBeaconSignalComponent implements OnInit {

  @Input() data;
  seconds: number;

  constructor() {
  }

  ngOnInit() {
    timer(0, 1000).pipe(
      mergeMap(_ => of((this.seconds = Date.now() - this.data.localTimestamp) / 1000))
    ).subscribe();

  }

}
