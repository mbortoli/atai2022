import { Injectable } from '@angular/core';
import {Observable, of, timer} from "rxjs/index";
import {mergeMap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public $data;

  constructor(private httpClient: HttpClient) {
  }

  public startNetwork() {
    this.$data = timer(0, 3000).pipe(
      mergeMap(_ => this.httpClient.get("http://localhost:8090/visualization")));
  }

  public getFilesNames() {
    return this.httpClient.get("http://localhost:8090/recordings_list");
  }

  public getFileData(fileName: string) {
    return this.httpClient.get("http://localhost:8090/recording/" + fileName);
  }

  public getPointsForStatistics() {
    return this.httpClient.get("http://localhost:8090/gamepoints");
  }

  public updateData(newData: any) {
    this.$data = of(newData);
  }
}
