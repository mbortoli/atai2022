import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContestComponent } from './logistics/contest/contest.component';
import { ActiveProductTaskComponent } from './logistics/contest/active-product-task/active-product-task.component';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {ActivePartDemandsComponent} from "./logistics/contest/active-part-demands/active-part-demands.component";
import { ActiveLockPartsComponent } from './logistics/contest/active-lock-parts/active-lock-parts.component';
import { RobotBeaconSignalComponent } from './logistics/contest/robot-beacon-signal/robot-beacon-signal.component';
import { MapComponent } from './logistics/contest/map/map.component';
import { CellComponent } from './logistics/contest/map/cell/cell.component';
import { OrdersComponent } from './logistics/contest/orders/orders.component';
import { MinuteSecondsPipe } from './logistics/contest/pipes/minute-seconds.pipe';
import { HeaderComponent } from './header/header.component';
import { FileStepperComponent } from './file-stepper/file-stepper.component';
import { RobotsComponent } from './logistics/contest/robots/robots.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { GamePointsComponent } from './statistics/game-points/game-points.component';
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {MatIconModule} from "@angular/material/icon";
import {MatSelectModule} from "@angular/material/select";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatTreeModule} from "@angular/material/tree";
import {MatTabsModule} from "@angular/material/tabs";
import {MatCardModule} from "@angular/material/card";
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import { RobotControllerComponent } from './robot-controller/robot-controller.component';
import { MoveToWaypointComponent } from './robot-controller/move-to-waypoint/move-to-waypoint.component';
import {ReactiveFormsModule} from "@angular/forms";
import {FormlyModule} from "@ngx-formly/core";
import {FormlyMaterialModule} from "@ngx-formly/material";
import { GetProductComponent } from './robot-controller/get-product/get-product.component';
import { DeliverProductComponent } from './robot-controller/deliver-product/deliver-product.component';
import { ExploreMachineComponent } from './robot-controller/explore-machine/explore-machine.component';
import { BufferCapComponent } from './robot-controller/buffer-cap/buffer-cap.component';

@NgModule({
  declarations: [
    AppComponent,
    ContestComponent,
    ActiveProductTaskComponent,
    ActivePartDemandsComponent,
    ActiveLockPartsComponent,
    RobotBeaconSignalComponent,
    MapComponent,
    CellComponent,
    OrdersComponent,
    MinuteSecondsPipe,
    HeaderComponent,
    FileStepperComponent,
    RobotsComponent,
    StatisticsComponent,
    GamePointsComponent,
    RobotControllerComponent,
    MoveToWaypointComponent,
    GetProductComponent,
    DeliverProductComponent,
    ExploreMachineComponent,
    BufferCapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatMenuModule,
    MatInputModule,
    MatTreeModule,
    MatTabsModule,
    NoopAnimationsModule,
    MatTableModule,
    HttpClientModule,
    MatCardModule,
    NgxChartsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    FormlyMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
