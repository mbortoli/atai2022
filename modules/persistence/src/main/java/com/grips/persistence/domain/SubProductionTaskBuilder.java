package com.grips.persistence.domain;

import com.shared.domain.MachineSide;

import java.util.Set;

public class SubProductionTaskBuilder {

    private SubProductionTaskBuilder() {}

    public static SubProductionTaskBuilder newBuilder() {
        return new SubProductionTaskBuilder();
    }

    public SubProductionTask build() {
        return new SubProductionTask(preConditionTasks,
                name,
                machine,
                state,
                type,
                side,
                orderInfoId,
                lockMachine,
                unlockMachine,
                incrementMachine,
                decrementMachine,
                decrementCost,
                sameRobotSubTask,
                requiredColor,
                optCode,
                fatal,
                requiresReset,
                bindRsOnAssignment,
                machineStateTaskOk,
                machineStateRequiresReset,
                machineStateTaskReasign,
                demandTaskWithOutDemand,
                priority,
                isDemandTask);
    }



    private Set<SubProductionTask> preConditionTasks = null;
    private String name = null;
    private String machine = null;
    private SubProductionTask.TaskState state = null;
    private MachineSide side = null;
    private Long orderInfoId = null;
    private String lockMachine = null;
    private String unlockMachine = null;
    private String incrementMachine = null;
    private String decrementMachine = null;
    private int decrementCost = 0;
    private SubProductionTask sameRobotSubTask = null;
    private SubProductionTask.TaskType type = null;
    private String requiredColor = null;
    private String optCode = null;
    private boolean fatal = false;
    private boolean requiresReset = false;
    private String machineStateTaskOk = null;
    private String machineStateRequiresReset = null;
    private String machineStateTaskReasign = null;
    private boolean bindRsOnAssignment = false;
    private boolean demandTaskWithOutDemand = false;
    private int priority = 0;
    private boolean isDemandTask = false;



    public SubProductionTaskBuilder setPreConditionTasks(Set<SubProductionTask> preConditionTasks) {
        this.preConditionTasks = preConditionTasks;
        return this;
    }

    public SubProductionTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SubProductionTaskBuilder setMachine(String machine) {
        this.machine = machine;
        return this;
    }

    public SubProductionTaskBuilder setState(SubProductionTask.TaskState state) {
        this.state = state;
        return this;
    }

    public SubProductionTaskBuilder setOrderInfoId(Long orderInfoId) {
        this.orderInfoId = orderInfoId;
        return this;
    }

    public SubProductionTaskBuilder setLockMachine(String lockMachine) {
        this.lockMachine = lockMachine;
        return this;
    }

    public SubProductionTaskBuilder setUnlockMachine(String unlockMachine) {
        this.unlockMachine = unlockMachine;
        return this;
    }

    public SubProductionTaskBuilder setIncrementMachine(String incrementMachine) {
        this.incrementMachine = incrementMachine;
        return this;
    }

    public SubProductionTaskBuilder setDecrementMachine(String decrementMachine) {
        this.decrementMachine = decrementMachine;
        return this;
    }

    public SubProductionTaskBuilder setSameRobotSubTask(SubProductionTask sameRobotSubTask) {
        this.sameRobotSubTask = sameRobotSubTask;
        return this;
    }

    public SubProductionTaskBuilder setType(SubProductionTask.TaskType type) {
        this.type = type;
        return this;
    }

    public SubProductionTaskBuilder setSide(MachineSide side) {
        this.side = side;
        return this;
    }

    public SubProductionTaskBuilder setRequiredColor(String requiredColor) {
        this.requiredColor = requiredColor;
        return this;
    }

    public SubProductionTaskBuilder setOptCode(String optCode) {
        this.optCode = optCode;
        return this;
    }

    public SubProductionTaskBuilder setFatal(boolean fatal) {
        this.fatal = fatal;
        return this;
    }

    public SubProductionTaskBuilder setRequiresReset(boolean requiresReset) {
        this.requiresReset = requiresReset;
        return this;
    }

    public SubProductionTaskBuilder setMachineStateTaskOk(String machineStateTaskOk) {
        this.machineStateTaskOk = machineStateTaskOk;
        return this;
    }

    public SubProductionTaskBuilder setMachineStateRequiresReset(String machineStateRequiresReset) {
        this.machineStateRequiresReset = machineStateRequiresReset;
        return this;
    }

    public SubProductionTaskBuilder setMachineStateTaskReasign(String machineStateTaskReasign) {
        this.machineStateTaskReasign = machineStateTaskReasign;
        return this;
    }

    public SubProductionTaskBuilder setDecrementCost(int decrementCost) {
        this.decrementCost = decrementCost;
        return this;
    }

    public SubProductionTaskBuilder setBindRsOnAssignment(boolean bindRsOnAssignment) {
        this.bindRsOnAssignment = bindRsOnAssignment;
        return this;
    }

    public SubProductionTaskBuilder setDemandTaskWithOutDemand(boolean demandTaskWithOutDemand) {
        this.demandTaskWithOutDemand = demandTaskWithOutDemand;
        return this;
    }

    public SubProductionTaskBuilder setPriority(int priority) {
        this.priority = priority;
        return this;
    }

    public SubProductionTaskBuilder setIsDemandTask(boolean isDemandTask) {
        this.isDemandTask = isDemandTask;
        return this;
    }
}
