package com.grips.persistence.converter;

import com.shared.domain.MachineName;

import javax.persistence.AttributeConverter;

public class MachineNameConverter implements AttributeConverter<MachineName, String> {
    @Override
    public String convertToDatabaseColumn(MachineName attribute) {
        return attribute.getRawMachineName();
    }

    @Override
    public MachineName convertToEntityAttribute(String dbData) {
        return new MachineName(dbData);
    }
}
