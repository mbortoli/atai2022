package com.grips.persistence.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shared.domain.MachineSide;
import com.visualization.domain.VisualizationSubTask;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class SubProductionTask implements VisualizationSubTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
  
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "preconditions", joinColumns = {
			@JoinColumn(name = "sub_production_task_id", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "pre_sub_production_task_id", nullable = true) })
	@JsonIgnore
    private Set<SubProductionTask> preConditionTasks;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="preConditionTasks")
	@JsonIgnore
    private Set<SubProductionTask> subConditionTasks;

	private String name;

    private String machine;

    @Enumerated(EnumType.STRING)
    private TaskState state;

    private Long orderInfoId;
    
    private String lockMachine;
    
    private String unlockMachine;
    
    private String incrementMachine;
    
    private String decrementMachine;

    private String requiredColor;

    private String optCode;

    private boolean requiresReset;

    private int decrementCost;

    private boolean isDemandTask;
    
    @OneToOne
	@JsonIgnore
    private SubProductionTask sameRobotSubTask;
    
    @OneToOne(mappedBy="sameRobotSubTask")
	@JsonIgnore
    private SubProductionTask postsameRobotSubTask;

    @ManyToOne
    @JoinColumn(name="product_task_id")
	@JsonIgnore
    private ProductTask productTask;

    private Integer robotId;
    
    private Long startTime;

    private Long endTime;

    @Enumerated(EnumType.STRING)
    private TaskType type;

    @Enumerated(EnumType.STRING)
    private MachineSide side;

    private boolean fatal;

    private String machineStateTaskOk;

    private String machineStateRequiresReset;

    private String machineStateTaskReasign;

    private boolean bindDisposeRsOnAssignment;

    private boolean demandTaskWithOutDemand;

    private int priority;

	@Lob
    private String stateLogging;

    public SubProductionTask() {
    }

    public SubProductionTask(Set<SubProductionTask> preConditionTasks, String name, String machine,
                             TaskState state, TaskType type, MachineSide side, Long orderInfoId, String lock, String unlock, String incrementMachine,
                             String decrementMachine, int decrementCost, SubProductionTask sameRobotSubTask, String requiredColor, String optCode,
                             boolean fatal, boolean requiresReset, boolean bindRsOnAssignment, String machineStateTaskOk, String machineStateRequiresReset,
                             String machineStateTaskReasign, boolean demandTaskWithOutDemand, int priority, boolean isDemandTask) {
		super();
		this.preConditionTasks = preConditionTasks;
		this.name = name;
		this.machine = machine;
		this.state = state;
		this.orderInfoId = orderInfoId;
		this.lockMachine = lock;
		this.unlockMachine = unlock;
		this.incrementMachine = incrementMachine;
		this.decrementMachine = decrementMachine;
		this.sameRobotSubTask = sameRobotSubTask;
		this.type = type;
		this.side = side;
		this.requiredColor = requiredColor;
		this.optCode = optCode;
		this.fatal = fatal;
		this.requiresReset = requiresReset;
		this.machineStateTaskOk = machineStateTaskOk;
		this.machineStateRequiresReset = machineStateRequiresReset;
		this.machineStateTaskReasign = machineStateTaskReasign;
		this.decrementCost = decrementCost;
		this.bindDisposeRsOnAssignment = bindRsOnAssignment;
		this.demandTaskWithOutDemand = demandTaskWithOutDemand;
		this.priority = priority;
		this.isDemandTask = isDemandTask;
	}

	public void setState(TaskState state, String stateLogging) {
		this.state = state;
		this.stateLogging += ("; " + stateLogging);
	}

	public enum TaskState {SUCCESS, FAILED, TBD, INWORK, ASSIGNED, SUCCESS_PENDING, ASSIGNED_EXPLORATION }

    public enum TaskType {GET, DELIVER, DUMMY, MOVE, BUFFER_CAP}
}
