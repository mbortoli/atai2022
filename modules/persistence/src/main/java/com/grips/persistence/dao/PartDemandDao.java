package com.grips.persistence.dao;

import com.grips.persistence.domain.PartDemand;
import com.grips.persistence.domain.ProductTask;
import com.grips.persistence.domain.SubProductionTask;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartDemandDao extends CrudRepository<PartDemand, Long> {

	Iterable<PartDemand> findByTaskIsNullAndMachineContainsOrderByIdAsc(String machine);
	PartDemand findByTask(SubProductionTask task);
	int countByMachine(String machine);
	void deleteByTask(SubProductionTask task);
	List<PartDemand> findByTaskIsNullAndMachine(String machine);
	void deleteByProductTask(ProductTask pTask);
	int countByProductTask(ProductTask pTask);
	List<PartDemand> findByTaskIsNullAndProductTask(ProductTask pTask);
	void deleteByMachineAndProductTask(String machine, ProductTask pTask);

}
