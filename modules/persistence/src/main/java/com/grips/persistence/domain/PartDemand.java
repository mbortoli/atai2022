package com.grips.persistence.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.visualization.domain.VisualizationLock;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class PartDemand implements VisualizationLock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name="product_task_id")
    @JsonIgnore
    private ProductTask productTask;

    @ManyToOne
    @JoinColumn(name="sub_production_task_id")
    @JsonIgnore
    private SubProductionTask task;

    private String machine;

}
