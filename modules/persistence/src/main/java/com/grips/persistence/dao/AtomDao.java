package com.grips.persistence.dao;

import com.grips.persistence.domain.Atom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AtomDao extends CrudRepository<Atom, Long> {

    List<Atom> findByNameAndAttributes (String name, String attributes);

    List<Atom> findByName (String name);

}
