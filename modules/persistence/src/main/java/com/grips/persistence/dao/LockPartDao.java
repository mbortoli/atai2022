package com.grips.persistence.dao;

import com.grips.persistence.domain.LockPart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LockPartDao extends CrudRepository<LockPart, Long> {

	void deleteByMachine(String machine);
	List<LockPart> findByMachine(String machine);
	int countByMachine(String machine);
	List<LockPart> findByProductId(long productId);

}
