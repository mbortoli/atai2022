/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.persistence.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shared.domain.Point2d;
import com.visualization.domain.VisualizationExplorationZone;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;

import java.util.*;

import static com.grips.persistence.domain.ExplorationZone.ZoneExplorationState.UNEXPLORED;

@Getter
@Setter
@CommonsLog
public class ExplorationZone extends Zone implements VisualizationExplorationZone {

    public static final int NUM_ZONES_WIDTH = 14;
    public static final int NUM_ZONES_HEIGHT = 8;
    public static final List<Integer> INSERTION_ZONE_NUMBERS = Collections.unmodifiableList(Arrays.asList(41, 31, 21));
    public static final List<Integer> FREE_ZONE_NUMBERS = Collections.unmodifiableList(Arrays.asList());


    protected Point2d zoneCenter;

    protected String zonePrefix;

    protected int heightIDX;

    protected int widthIDX;

    private long zoneNumber;

    protected ArrayList<ExplorationObservation> observations;

    protected ZoneExplorationState explorationState = UNEXPLORED;

    @JsonIgnore
    protected ExplorationZone mirroredZone;

    protected ZoneState zoneState;

    private String machine;

    private ZoneOccupiedState zoneOccupiedState;

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public ZoneOccupiedState getZoneOccupiedState() {
        return zoneOccupiedState;
    }

    public void setZoneOccupiedState(ZoneOccupiedState zoneOccupiedState) {
        this.zoneOccupiedState = zoneOccupiedState;
    }

    public enum ZoneOccupiedState {MACHINE, BLOCKED, FREE}

    public ExplorationZone(String zonePrefix, int zoneHeightIdx, int zoneWidthIdx) {
        if (zoneHeightIdx > NUM_ZONES_HEIGHT) {
            log.error("Zone height out of bounds!");
        }
        if (zoneWidthIdx > (NUM_ZONES_WIDTH)) {
            log.error("Zone width out of bounds!");
        }

        this.zonePrefix = zonePrefix;
        this.zoneNumber = zoneHeightIdx + 10 * zoneWidthIdx;
        this.heightIDX = zoneHeightIdx;
        this.widthIDX = zoneWidthIdx;
        this.zoneName = zonePrefix + "_Z" + String.valueOf(this.zoneNumber);
        this.observations = new ArrayList<>();
        if (FREE_ZONE_NUMBERS.contains(new Long(zoneNumber).intValue())) {
            this.setZoneState(ExplorationZone.ZoneState.FREE);
        } else if (INSERTION_ZONE_NUMBERS.contains(new Long(zoneNumber).intValue())) {
            this.setZoneState(ExplorationZone.ZoneState.INSERTION);
        } else {
            this.setZoneState(ExplorationZone.ZoneState.VALID);
        }
        double yPos = heightIDX - 0.5;
        double xPos = widthIDX - 0.5;
        if (0 == zonePrefix.compareToIgnoreCase("M")) {
            xPos *= -1;
        }
        this.zoneCenter = new Point2d(xPos, yPos);
    }

    public enum ZoneExplorationState {UNEXPLORED, EXPLORED, SCHEDULED, ORIENTATION_FOUND, REPORTED}

    public enum ZoneState {INSERTION, FREE, VALID}

}
