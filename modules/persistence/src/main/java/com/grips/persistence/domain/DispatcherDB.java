package com.grips.persistence.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@Builder
public class DispatcherDB  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int first;

}
