package com.robot_communication.services;

import com.google.protobuf.GeneratedMessageV3;
import com.grips.protobuf_lib.ProtobufServer;
import com.grips.protobuf_lib.RobotConnections;
import com.grips.protobuf_lib.RobotMessageRegister;
import com.shared.domain.MachineName;
import com.shared.domain.MachineSide;
import lombok.NonNull;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.*;

@CommonsLog
public class RobotClient {

    private boolean robotsStopped;

    private final ProtobufServer protobufServer;
    private final PrsTaskCreator prsTaskCreator;
    private final RobotConnections robotConnections;

    public RobotClient(ProtobufServer protobufServer,
                       PrsTaskCreator prsTaskCreator,
                       RobotConnections robotConnections) {
        this.protobufServer = protobufServer;
        this.prsTaskCreator = prsTaskCreator;
        this.robotConnections = robotConnections;
        RobotMessageRegister.getInstance().add_message(GripsBeaconSignalProtos.GripsBeaconSignal.class);
        RobotMessageRegister.getInstance().add_message(GripsMachineHandlingReportAllSeenMachinesProtos.GripsMachineHandlingReportAllSeenMachines.class);
        RobotMessageRegister.getInstance().add_message(GripsMidlevelTasksProtos.GripsMidlevelTasks.class);
        RobotMessageRegister.getInstance().add_message(GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine.class);
        RobotMessageRegister.getInstance().add_message(GripsPrepareMachineProtos.GripsPrepareMachine.class);
        new Thread(protobufServer).start();
        this.robotsStopped = false;
    }

    public boolean isRobotsStopped() {
        return this.robotsStopped;
    }

    public void cancelTask(int robotId) {
        log.info("Canceling current task of robot: " + robotId);
        GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask = prsTaskCreator.createCancelTask((long) robotId);
        sendPrsTaskToRobot(prsTask);
    }

    public void sendDummyTaskToRobot(@NonNull Long robotId,
                                     @NonNull String machine,
                                     MachineSide side) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask = prsTaskCreator.createDummyTask(robotId,
                machine,
                side);
        sendPrsTaskToRobot(prsTask);
    }

    public void sendDeliverTaskToRobot(@NonNull Long robotId,
                                       @NonNull Long taskId,
                                       @NonNull String machine,
                                       @NonNull MachineSide side,
                                       Integer machineCount) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks deliverPrsTask = prsTaskCreator.createDeliverWorkPieceTask(
                robotId,
                taskId,
                machine,
                side,
                convertSideToShelfSlide(side, machineCount));
        sendPrsTaskToRobot(deliverPrsTask);
    }

    public void sendWaitingTaskToRobot(@NonNull Long robotId,
                                       @NonNull String zone) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask = prsTaskCreator.createWaitingTask(robotId, zone);
        sendPrsTaskToRobot(prsTask);
    }

    public void sendGetTaskToRobot(@NonNull Long robotId,
                                   @NonNull Long taskId,
                                   @NonNull String machine,
                                   MachineSide side,
                                   Integer machineCount) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks getPrsTask = prsTaskCreator.createGetWorkPieceTask(
                robotId,
                taskId,
                machine,
                side,
                convertSideToShelfSlide(side, machineCount));
        sendPrsTaskToRobot(getPrsTask);
    }

    private String convertSideToShelfSlide(MachineSide side, Integer materialCount) {
        switch (side) {
            case SHELF:
                return "shelf" + (materialCount);
            case SLIDE:
                return "slide";
            default:
                return null;
        }
    }

    public void stopAllRobots() {
        this.robotsStopped = true;
        robotConnections.getclientId().forEach(this::stopRobot);
    }

    public void startAllRobots() {
        this.robotsStopped = false;
        robotConnections.getclientId().forEach(this::startRobot);
    }

    private void startRobot(Long robotId) {
        sendPrsTaskToRobot(prsTaskCreator.createStopTask(robotId, false));
    }

    private void stopRobot(Long robotId) {
        sendPrsTaskToRobot(prsTaskCreator.createStopTask(robotId, true));
    }

    public <T extends GeneratedMessageV3> void sendToRobot(long robot_id, @NonNull T msg) {
        protobufServer.send_to_robot(robot_id, msg);
    }

    public void sendPrsTaskToRobot(GripsMidlevelTasksProtos.GripsMidlevelTasks task) {
        log.info("Sending Task: " + task.getTaskId() + " to robot: " + task.getRobotId() + " - " + task.toString());
        robotConnections.getRobot(task.getRobotId()).setTimeLastTaskAssignment(System.currentTimeMillis());
        this.sendToRobot(task.getRobotId(), task);
    }

    public void sendBufferCap(Long robotId, Long taskId, MachineName machine, Integer shelf) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks getPrsTask = prsTaskCreator.createBufferCapTask(
                robotId,
                taskId.intValue(),
                machine,
                shelf);
        sendPrsTaskToRobot(getPrsTask);
    }

    public void sendMoveTask(Long robotId, Long taskId, MachineName machineName, MachineSide side) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks getPrsTask = prsTaskCreator.createMoveToMachineTask(robotId, taskId.intValue(),
                machineName, side);
        sendPrsTaskToRobot(getPrsTask);
    }

    public void sendMoveToZoneTask(Long robotId, Long taskId, String zone) {
        GripsMidlevelTasksProtos.GripsMidlevelTasks getPrsTask = prsTaskCreator.createMoveToWaypointTask(robotId, taskId.intValue(), zone);
        sendPrsTaskToRobot(getPrsTask);
    }
}

