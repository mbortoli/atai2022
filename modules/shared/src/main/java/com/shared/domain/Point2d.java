package com.shared.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Point2d {
    double x;
    double y;


    //from java.awt.geom.Point2D
    public static double distance(double var0, double var2, double var4, double var6) {
        var0 -= var4;
        var2 -= var6;
        return Math.sqrt(var0 * var0 + var2 * var2);
    }

    public double distance(Point2d var1) {
        double var2 = var1.getX() - this.getX();
        double var4 = var1.getY() - this.getY();
        return Math.sqrt(var2 * var2 + var4 * var4);
    }

}
