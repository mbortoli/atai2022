package com.shared.domain;

public enum BaseColor {
    BASE_RED,
    BASE_BLACK,
    BASE_SILVER
}
