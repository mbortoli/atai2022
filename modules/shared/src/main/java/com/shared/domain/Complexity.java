package com.shared.domain;

public enum Complexity {
    C0,
    C1,
    C2,
    C3,
    D0,
    D1,
    D2,
    E0
}
