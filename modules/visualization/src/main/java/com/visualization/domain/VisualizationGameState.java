package com.visualization.domain;

import com.grips.model.teamserver.TeamColor;
import com.shared.domain.GamePhase;
import com.shared.domain.GameState;

public interface VisualizationGameState {
    String getTeamCyan();
    String getTeamMagenta();
    long getPointsCyan();
    long getPointsMagenta();
    long getGameTimeNanoSeconds();
    GameState getState();
    GamePhase getPhase();
    TeamColor getGripsColor();
}
