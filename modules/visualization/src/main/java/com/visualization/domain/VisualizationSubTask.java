package com.visualization.domain;

public interface VisualizationSubTask {
    long getId();
    String getName();
    String getMachine();
    Integer getRobotId();
}
