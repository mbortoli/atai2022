package com.visualization.domain;

import com.shared.domain.RingColor;

public interface VisualizationRing {
    RingColor getRingColor();
    int getRawMaterial();
}
