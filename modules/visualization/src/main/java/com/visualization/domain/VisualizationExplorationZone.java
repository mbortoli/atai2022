package com.visualization.domain;

import com.shared.domain.Point2d;

public interface VisualizationExplorationZone {
    Point2d getZoneCenter();
    //todo use MachineName object!
    String getMachine();
}
