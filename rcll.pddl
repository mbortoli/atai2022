(define (domain rcll)
 (:requirements :strips :typing :durative-actions :fluents :preferences)
 (:types base_station cap_station_input cap_station_output cap_station_shelf delivery_station starting ring_station_input ring_station_output - location
   location holding_station - station
   cap_station ring_station - holding_station
   robot product ccolor rcolor)

  (:predicates 
     (at ?r - robot ?l - location)
     (free ?r - robot)
     (empty ?l - location)
     (holding ?r - robot ?p - product)
     (n_holding ?r - robot)
	 (stage_c0_0 ?p - product)
     (stage_c0_1 ?p - product)
	 (stationcapcolor ?cs - cap_station ?c - ccolor)
	 (stationringcolor ?rs - ring_station ?c - rcolor)
 )

 (:functions
    (distance ?l1 ?l2 - location)
	(requiredbases ?c - rcolor)
 )




(:durative-action getBaseFromBScriticalTask 
	:parameters (?r - robot ?bs - base_station ?p - product ?l - location)
	:duration (= ?duration (+ 31 (distance ?l ?bs) ) )
	:condition (and 
		(at start (at ?r ?l))
		(at start (empty ?bs))
		(at start (free ?r))
		(at start (stage_c0_0 ?p))
		(at start (n_holding ?r))
	)
	:effect (and
		(at start (at ?r ?bs))
		(at start (not (empty ?bs)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (stage_c0_1 ?p ))
		(at end (not (stage_c0_0 ?p)))
		(at end (holding ?r ?p))
		(at start (not (n_holding ?r)))

 	)
)





)
